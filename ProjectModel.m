//
//  ProjectModel.m
//  appDemo
//
//  Created by 刘雨辰 on 15/9/13.
//  Copyright (c) 2015年 lyc. All rights reserved.
//

#import "ProjectModel.h"

@implementation ProjectModel





-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    
    if(self){
        
        self.projectID = [aDecoder decodeObjectForKey:@"ProejctID"];
        self.projectTitle = [aDecoder decodeObjectForKey:@"ProjectTitle"];
        self.joinType = [aDecoder decodeObjectForKey:@"joinType"];
        
        
    }
    return self;
    
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:self.projectID forKey:@"ProejctID"];
    [aCoder encodeObject:self.projectTitle forKey:@"ProjectTitle"];
    [aCoder encodeObject:self.joinType forKey:@"joinType"];
    
}



@end
