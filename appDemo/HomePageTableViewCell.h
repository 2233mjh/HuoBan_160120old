//
//  MainTableViewCell.h
//  appDemo
//
//  Created by 刘雨辰 on 15/9/6.
//  Copyright (c) 2015年 lyc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCFireworksButton.h"
#import "SDCycleScrollView.h"
#import "DataModelHomeCard.h"
#import "TYMProgressBarView.h"


@interface HomePageTableViewCell : UITableViewCell
//项目大图


@property (weak, nonatomic) IBOutlet UIImageView *imageViewContent;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewContentHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewContentWidth;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewLove;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewJoined;
@property (weak, nonatomic) IBOutlet UILabel *labelBy;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelContents;
@property (weak, nonatomic) IBOutlet UIView *viewCell;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewRight;

@property (weak, nonatomic) IBOutlet UIView *viewType;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTypeHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTypeWidth;

@property (weak, nonatomic) IBOutlet UILabel *labelType;
@property (weak, nonatomic) IBOutlet UIView *viewCountry;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewCountryWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewCountryHeight;
@property (weak, nonatomic) IBOutlet UILabel *labelCountry;

@property (weak, nonatomic) IBOutlet UIView *viewDate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewDateWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewDateHeight;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UIView *viewPreson;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewPresonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewPresonWidth;
@property (weak, nonatomic) IBOutlet UILabel *labelPreson;
@property(nonatomic,strong)UILabel *labelProgress;

@property(nonatomic,strong) TYMProgressBarView *HomePageProgressBarView;

@end
