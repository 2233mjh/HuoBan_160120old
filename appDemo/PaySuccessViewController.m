//
//  PaySuccessViewController.m
//  huoban
//
//  Created by 刘雨辰 on 15/11/3.
//  Copyright © 2015年 lyc. All rights reserved.
//

#import "PaySuccessViewController.h"
#import "DataModel.h"
#import "MainTabBarViewController.h"

@interface PaySuccessViewController ()

@end

@implementation PaySuccessViewController

UIWindow *window;

DataModel *_dataModelPaySuccess;
NSString *_iOSPaySuccess;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    _dataModelPaySuccess = [[DataModel alloc] init];
    
    _iOSPaySuccess = _dataModelPaySuccess.userInfomation.iOSDeviceSize;
    
     CGSize _iOSDevice = [UIScreen mainScreen].bounds.size;
    
    self.viewBlueHeight.constant = _iOSDevice.height/2;
    self.viewGreenHeight.constant = _iOSDevice.height/2;
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)buttonCancel:(id)sender {
    
//    MainTabBarViewController *loginTabBarvc = [window.rootViewController.storyboard
//                                               instantiateViewControllerWithIdentifier:@"mainTabBarView"];
//    window.rootViewController = loginTabBarvc;
//    
//    loginTabBarvc.selectedIndex = 1;
    
    [self.delegate paySuccessViewControllerDone:self];
}
@end
