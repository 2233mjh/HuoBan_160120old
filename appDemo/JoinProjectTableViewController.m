//
//  JoinProjectTableViewController.m
//  huoban
//
//  Created by 刘雨辰 on 15/9/22.
//  Copyright (c) 2015年 lyc. All rights reserved.
//

#import "JoinProjectTableViewController.h"

#import "PayForProjectTableViewController.h"
#import "ToolClass.h"
#import "DataModel.h"

@interface JoinProjectTableViewController ()

@end

@implementation JoinProjectTableViewController
@synthesize aryDataChou,projectID,imageClose,countDownJoinDate,dataDate,buttonClose;

CGFloat _heightLabelContent;
NSDateFormatter *_dateFormatterJoinIn;
NSDate  *_dateJoinInNow;
CGSize  _iOSBoundsJoin;
CGRect _viewLineSize;
NSDate *_createDate;

NSMutableParagraphStyle * _paragraphSummarizeStyleJoin;
NSMutableAttributedString  *_strContentsJoin;

NSString *_iOSJoinProject;
DataModel *_dataModelJoinProject;

- (void)viewDidLoad {
    [super viewDidLoad];

    NSLog(@"众筹众筹%@",self.aryDataChou);
    self.navigationController.navigationBar.translucent = NO;
    //上放导航栏
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1]];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName : [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1]}];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    //去除导航栏阴影
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];

    
    _iOSBoundsJoin = [UIScreen mainScreen].bounds.size;
    _dataModelJoinProject = [[DataModel alloc] init];

    _iOSJoinProject = _dataModelJoinProject.userInfomation.iOSDeviceSize;
    
    if([_iOSJoinProject isEqualToString:@"iPhone6Plus"]){
        
        imageClose.image = [UIImage imageNamed:@"back_white_3x"];
        
    }else{
        imageClose.image = [UIImage imageNamed:@"back_white_2x"];
    }
    
    self.tableView.contentInset = UIEdgeInsetsMake(6, 0,6,0);
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];

    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self.tableView reloadData];

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    NSLog(@"打印数组数量%lu",(unsigned long)[self.aryDataChou count]);
    return [self.aryDataChou count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *joinProjectCellIdentifier = @"JoinProjectCell";
    
    joinProjectTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:joinProjectCellIdentifier];
    
    if (cell == nil)
    {
        cell = [[joinProjectTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"JoinProjectCell"];
    }

    cell.viewLine.backgroundColor = [UIColor whiteColor];
    cell.textMoney.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
    cell.textTitle.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
    cell.textContents.textColor =[UIColor colorWithRed:170.0/255 green:170.0/255 blue:170.0/255 alpha:1];
    cell.textPreson.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
    cell.textTime.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];

//    cell.textContents.layer.borderWidth = 1;
    
    NSMutableDictionary *dic = self.aryDataChou[indexPath.row];
    
    cell.textTitle.text = dic[@"title"];
    cell.textMoney.text = [NSString stringWithFormat:@"支持%ld元", [dic[@"money"]longValue]];
    
 _strContentsJoin =[[NSMutableAttributedString alloc]initWithString:dic[@"desc"]];
    
   _paragraphSummarizeStyleJoin =[[NSMutableParagraphStyle alloc]init];
    
    [_paragraphSummarizeStyleJoin setMinimumLineHeight:24];
    
    [_strContentsJoin addAttribute:NSParagraphStyleAttributeName value:_paragraphSummarizeStyleJoin range:NSMakeRange(0, [dic[@"desc"] length])];
    
    cell.textContents.attributedText = _strContentsJoin;
    [cell.textContents sizeToFit];
    _heightLabelContent = cell.textContents.frame.size.height;
    cell.textTime.text = [NSString stringWithFormat:@"众筹结束后%lu天内发送",[dic[@"send_number"] longValue]];
    
    if([dic[@"want"] longValue] >= 10000){
     
        cell.textPreson.text = [NSString stringWithFormat:@"%ld人支持",[dic[@"fact"] longValue]];

    }else{
           cell.textPreson.text = [NSString stringWithFormat:@"%ld人支持/%ld个名额",[dic[@"fact"] longValue],[dic[@"want"] longValue]];
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  _heightLabelContent + 87;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    joinProjectTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if( self.countDownJoinDate <= 0){
        [[ToolClass sharedInstance] showAlert:@"此项目众筹期已结束，你仍可以「关注」来加入项目社群喔"];
        return;
    }
//
    NSMutableDictionary *dic = self.aryDataChou[indexPath.row];
//
//    
    if([dic[@"fact"] longValue] - [dic[@"want"] longValue] == 0 ){
        
        [[ToolClass sharedInstance] showAlert:@"此档位已满，请选择其他档位支持"];
        return;
    }
    
    cell.viewLine.backgroundColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
    cell.textMoney.textColor = [UIColor whiteColor];
    cell.textTitle.textColor = [UIColor whiteColor];
    cell.textContents.textColor = [UIColor whiteColor];
    cell.textPreson.textColor = [UIColor whiteColor];
    cell.textTime.textColor = [UIColor whiteColor];
    
    NSLog(@"%@",dic);
    
    [self performSegueWithIdentifier:@"PayProjectSegue" sender:dic];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"PayProjectSegue"]){
        
        UINavigationController *naVC = segue.destinationViewController;
        
        PayForProjectTableViewController *controller = (PayForProjectTableViewController*)naVC.topViewController;
        
        NSLog(@"%@",sender);
        controller.dicPayDataModel = sender;
        
        controller.projectEndDate = self.dataDate;
        
        
        NSLog(@"带你传递Model%@",self.projectID);
        
        controller.projectID = self.projectID;
        
        
    }
}

-(void)buttonJoinClick:(UIButton*)sender{
    
    NSLog(@"出发了我要支持");
}


- (IBAction)buttonCancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
