//
//  TalkMessageTableViewCell.h
//  huoban
//
//  Created by Lyc on 15/12/23.
//  Copyright © 2015年 lyc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TalkMessageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPreson;
@property (weak, nonatomic) IBOutlet UILabel *labelBy;
@property (weak, nonatomic) IBOutlet UILabel *labelProjectName;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;
@property (weak, nonatomic) IBOutlet UILabel *labelCreator;
@property (weak, nonatomic) IBOutlet UIButton *buttonUpPresonList;
@property (weak, nonatomic) IBOutlet UILabel *labelContents;
@property (weak, nonatomic) IBOutlet UIButton *buttonDelete;
@property (weak, nonatomic) IBOutlet UIButton *buttonLove;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewLove;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewDelete;
@property (weak, nonatomic) IBOutlet UILabel *labelUpPresonList;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewContents;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewContentsWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewContentsHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonLoveRight;
@property (weak, nonatomic) IBOutlet UIButton *buttonPreson;
@end
