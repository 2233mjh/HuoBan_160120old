//
//  PaySuccessViewController.h
//  huoban
//
//  Created by 刘雨辰 on 15/11/3.
//  Copyright © 2015年 lyc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PaySuccessViewController;

@protocol paySuccessControllerDelegate <NSObject>

-(void)paySuccessViewControllerDone:(PaySuccessViewController*)sender;

@end


@interface PaySuccessViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imagePagSuccess;


- (IBAction)buttonCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imageClose;
@property (weak, nonatomic) IBOutlet UIView *viewGreen;
@property (weak, nonatomic) IBOutlet UIView *viewBlue;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewGreenHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewBlueHeight;
@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;

@property(nonatomic,strong)id<paySuccessControllerDelegate>delegate;


@end
