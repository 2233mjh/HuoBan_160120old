//
//  huobanLoginViewController.m
//  huoban登录注册部分
//
//  Created by 马锦航 on 15/12/17.
//  Copyright © 2015年 马锦航. All rights reserved.
//

#import "huobanLoginViewController.h"
#import "CDPMonitorKeyboard.h"
#import "ToolClass.h"
#import "DataModel.h"
#import "HttpClassSelf.h"
#import "UserSelfMessageViewController.h"
#import "huobanAuthViewController.h"

//#import "HttpTool.h"
#import "huobanRegisterViewController.h"

#import "CDPMonitorKeyboard.h"
#import "RegisterAccountViewController.h"
#import "ToolClass.h"
#import "HomePageTableViewController.h"
#import "UserSelfMessageViewController.h"
#import "CommonCrypto/CommonDigest.h"
#import "JSONKit.h"
#import "CommonBase64.h"
#import "ToolClass.h"
#import "CDPMonitorKeyboard.h"
#import "HomePageTableViewController.h"
#import "MainTabBarViewController.h"
#import "HttpClassSelf.h"
#import "UserInfomation.h"
#import "DataModel.h"
#import "ProjectModel.h"
#import "ProjectDynamicTableViewController.h"
#import "ImageSameWidth.h"
#import "MainTabBarViewController.h"
#import "ForgotPWDViewController.h"

#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]

//字体宏
#define PingFangSC(s)     [UIFont fontWithName:@"PingFangSC-Light" size:(s)/1.0f]


// #B1AC5B  177,172,91
#define PlaceHolderColor  [UIColor colorWithRed:177/255.f green:172/255.f blue:91/255.f alpha:1.0]


#define PlaceHolderFont     [UIFont fontWithName:@"PingFangSC-Light" size:18.0f]

//#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]


@interface huobanLoginViewController ()<UITextFieldDelegate>

{
    NSMutableDictionary *_resLoginDic;
    HttpClassSelf *_httpClassLogin;
    
    DataModel *_dataModelLogin;
    NSString *_iOSSizeLogin;
}

@property (nonatomic,strong) UITextField * huobanPhoneNum;          //手机号

@property (nonatomic,strong) UITextField * huobanPassWorld;         //密码

@property (nonatomic,strong) UIButton * passWorldCancel;            //密码取消按钮

@property (nonatomic,strong) UIView * loginView;                    //登录视图

@property (nonatomic,strong) UIButton * loginButton;                //登录按钮

@property (nonatomic,strong) UIView * huobanFootView;               //底部视图

@property (nonatomic,strong) UIButton * huobanFoundPassWorld;       //找回密码

@property (nonatomic,strong) UIButton * huobanRegister;             //注册密码

@end

@implementation huobanLoginViewController

#pragma mark - 懒加载

- (UITextField *)huobanPhoneNum {
    if (!_huobanPhoneNum) {
        _huobanPhoneNum = [[UITextField alloc]initWithFrame:CGRectMake(12, 0, SCREEN_WIDTH - 12*2, 60)];
        //        [_huobanPhoneNum setPlaceholder:@"手机号"];
        //        [_huobanPhoneNum setFont:[UIFont fontWithName:@"PingFangSC-Light" size:14.7]];
        _huobanPhoneNum.delegate = self;
#warning 测试用 输入的电话号
        //        _huobanPhoneNum.text = @"18810032434";
        //        _huobanPassWorld.text = @"ggblcs";
        
        //        _huobanPhoneNum.text = @"13520495251";
        //        _huobanPassWorld.text = @"123456";
        
        _huobanPhoneNum.attributedPlaceholder = [self attributedPlaceholderWithString:@"手机号"];
        _huobanPhoneNum.returnKeyType = UIReturnKeyDone;

    }
    return _huobanPhoneNum;
}

- (UIButton *) passWorldCancel {
    if (!_passWorldCancel) {
        //        _passWorldCancel = [[UIButton alloc] initWithFrame:CGRectMake(self.huobanPassWorld.frame.size.width - 32, 20, 32, 32)];
        NSInteger cancelButtonWidth = 25;
        _passWorldCancel = [[UIButton alloc] initWithFrame:CGRectMake(self.huobanPassWorld.frame.origin.x + self.huobanPassWorld.frame.size.width - cancelButtonWidth, self.huobanPassWorld.frame.origin.y + self.huobanPassWorld.frame.size.height - cancelButtonWidth - 10, cancelButtonWidth, cancelButtonWidth)];
        [_passWorldCancel setBackgroundImage:[UIImage imageNamed:@"登录_x@2x"] forState:UIControlStateNormal];
        _passWorldCancel.hidden = YES;
        [_passWorldCancel addTarget:self action:@selector(cancelPassWorld:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _passWorldCancel;
}

- (UITextField *)huobanPassWorld {
    if (!_huobanPassWorld) {
        _huobanPassWorld = [[UITextField alloc]initWithFrame:CGRectMake(12,self.huobanPhoneNum.frame.origin.y+self.huobanPhoneNum.frame.size.height, SCREEN_WIDTH - 12*2, 60)];
        //        [_huobanPassWorld setPlaceholder:@"密码"];
        //开启默认的掩码
        _huobanPassWorld.secureTextEntry = YES;
        [_huobanPassWorld setFont:[UIFont fontWithName:@"PingFangSC-Light" size:14.7]];
        _huobanPassWorld.delegate = self;
        
        _huobanPassWorld.attributedPlaceholder = [self attributedPlaceholderWithString:@"密码"];
        _huobanPassWorld.returnKeyType = UIReturnKeyDone;
        //        [_huobanPassWorld addSubview:_passWorldCancel];
    }
    return _huobanPassWorld;
}


#pragma mark 中部登录视图

//320*568   navi 64 + 60 *3 + 36 +12 +
- (UIButton *)loginButton {
    if (!_loginButton) {
        NSInteger loginButtonHigth = 50;
        _loginButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2 - loginButtonHigth/2 , 20, loginButtonHigth, loginButtonHigth)];
        //        [_loginButton setBackgroundColor:[UIColor redColor]];
        [_loginButton setBackgroundImage:[UIImage imageNamed:@"不可点击确认"] forState:UIControlStateNormal];
        _loginButton.layer.cornerRadius = loginButtonHigth/2;
        [_loginButton addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _loginButton;
}

- (UIView *)loginView {
    if (!_loginView) {
        _loginView = [[UIView alloc]initWithFrame:CGRectMake(0, 120, SCREEN_WIDTH, SCREEN_HEIGHT - 120 - 50 - 64)];
        [_loginView setBackgroundColor:[UIColor yellowColor]];
        NSLog(@"mainScreen.bounds:%@",NSStringFromCGRect([UIScreen mainScreen].bounds));
        //添加一个不透明的跟当前视图大小一样的按钮，作为取消键盘的点击按钮
        UIButton * resignKeyboardResbonder = [[UIButton alloc]initWithFrame:_loginView.bounds];
        //        [resignKeyboardResbonder setBackgroundColor:[UIColor blackColor]];
        [resignKeyboardResbonder addTarget:self action:@selector(resignTextFieldFirstResponder) forControlEvents:UIControlEventTouchUpInside];
        [_loginView addSubview:resignKeyboardResbonder];
        [_loginView addSubview:self.loginButton];
        
    }
    return _loginView;
}


#pragma mark 底部视图
- (UIView *)huobanFootView {
    if (!_huobanFootView) {
        _huobanFootView = [[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT -50- 64, SCREEN_WIDTH, 50)];
        NSLog(@"footView.frame:%@",NSStringFromCGRect(_huobanFootView.frame));
        [_huobanFootView setBackgroundColor:[UIColor yellowColor]];
        [_huobanFootView addSubview:self.huobanFoundPassWorld];
        [_huobanFootView addSubview:self.huobanRegister];
    }
    return _huobanFootView;
}



- (UIButton *)huobanFoundPassWorld {
    if (!_huobanFoundPassWorld) {
        _huobanFoundPassWorld = [[UIButton alloc]initWithFrame:CGRectMake(12, 0, 100, 50)];
        [_huobanFoundPassWorld setTitle:@"找回密码" forState:UIControlStateNormal];
        [_huobanFoundPassWorld setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_huobanFoundPassWorld addTarget:self action:@selector(foundPassworldAction:) forControlEvents:UIControlEventTouchUpInside];
//        _huobanFoundPassWorld.backgroundColor = [UIColor greenColor];
        _huobanFoundPassWorld.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_huobanFoundPassWorld setFont:PingFangSC(14.7)];
        //        [_huobanFoundPassWorld setBackgroundColor:[UIColor greenColor]];
    }
    return _huobanFoundPassWorld;
    
}

- (UIButton *)huobanRegister {
    if (!_huobanRegister) {
        _huobanRegister = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 12 - 100, 0, 100, 50)];
        [_huobanRegister setTitle:@"注册" forState:UIControlStateNormal];
        [_huobanRegister setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_huobanRegister addTarget:self action:@selector(registerAction:) forControlEvents:
         UIControlEventTouchUpInside];
        _huobanRegister.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_huobanRegister setFont:PingFangSC(14.7)];
        //        [_huobanRegister setBackgroundColor:[UIColor greenColor]];
    }
    return _huobanRegister;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor yellowColor]];
    
    _httpClassLogin = [[HttpClassSelf alloc]init];
    
    //添加电话号码文本框到当前视图
    [self.view addSubview:self.huobanPhoneNum];
    //添加密码文本框到当前视图
    [self.view addSubview:self.huobanPassWorld];
    //添加中部登录视图到当前视图
    [self.view addSubview:self.loginView];
    //添加底部footView到当前视图
    [self.view addSubview:self.huobanFootView];
    
    
    [self.view addSubview:self.passWorldCancel];
    
    
    //177,172,91
    UIColor * underLineColor = RGBCOLOR(177, 172, 91);
    [self addUnderLineWithObject:self.huobanPhoneNum color:underLineColor hight:1 isMainScreenWidth:0 isTopOrBottom:0];
    [self addUnderLineWithObject:self.huobanPassWorld color:underLineColor hight:1 isMainScreenWidth:0 isTopOrBottom:0];
    //    [self addUnderLineWithObject:self.loginView color:[UIColor redColor] hight:2 isMainScreenWidth:1];
    [self addUnderLineWithObject:self.huobanFootView color:underLineColor hight:1 isMainScreenWidth:3 isTopOrBottom:1];
    
    
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    //增加监听，当键退出时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    //    [self testAction];
}



- (void) testAction {
    UIButton * button = [[UIButton alloc]initWithFrame:CGRectMake(100, SCREEN_HEIGHT - 100 - 64, 100, 100)];
    [button setBackgroundColor:[UIColor blueColor]];
    [self.view addSubview:button];
    [button addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void) loginAction: (UIButton *)sender {
    
    NSLog(@"登录");
    if (self.huobanPhoneNum.text.length == 0) {
        //手机号不能为空
        [[ToolClass sharedInstance] showAlert:@"手机号不能为空"];
        return;
    }
    else if (self.huobanPhoneNum.text.length < 11)
    {
        //手机号长度不够
        [[ToolClass sharedInstance] showAlert:@"手机号码输入有误，请重新输入"];
        return;
    }
    else if (self.huobanPhoneNum.text.length == 0){
        //密码不能为空
        [[ToolClass sharedInstance] showAlert:@"密码不能为空"];
        return;
    }
    
    DataModel *dataModelLoginSuccess = [[DataModel alloc] init];
    [_httpClassLogin loginSetMobile:self.huobanPhoneNum.text pwd:self.huobanPassWorld.text umengid:@""mobibuild:[UIDevice currentDevice].model mobitype:@"iOS" CallBackYES:^(MKNetworkOperation *operatioin){
        
        NSData *data = [operatioin responseData];
        
        _resLoginDic = [[NSMutableDictionary alloc]initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
        
        if([_resLoginDic[@"status"] isEqualToString:@"success" ]){
            //登陆先清除缓存
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud removeObjectForKey:@"HomePageCellDataModel"];
            [ud removeObjectForKey:@"ProjectDynamicWM"];
            
            [ud setInteger:1 forKey:@"runtimes"];
            
            NSMutableDictionary *dic = _resLoginDic[@"data"];
            
            NSMutableDictionary *dicPOST = dic[@"post"];
            dataModelLoginSuccess.userInfomation.tokenID = _resLoginDic[@"token"];
            dataModelLoginSuccess.userInfomation.UserID = dic[@"_id"];
            dataModelLoginSuccess.userInfomation.userName = self.huobanPhoneNum.text;
            dataModelLoginSuccess.userInfomation.passWord = self.huobanPassWorld.text;
            dataModelLoginSuccess.userInfomation.ImagePreson = dic[@"image"];
            dataModelLoginSuccess.userInfomation.Name = dic[@"name"];
            dataModelLoginSuccess.userInfomation.payName = dicPOST[@"name"];
            dataModelLoginSuccess.userInfomation.payMobile = dicPOST[@"mobile"];
            dataModelLoginSuccess.userInfomation.add = dicPOST[@"address"];
            [dataModelLoginSuccess saveUserInfomation];
            [self actionAfterLoginSuccess];
            
        }else{
            NSData *dataFail = [operatioin responseData];
            
            NSMutableDictionary *dicFail = [NSJSONSerialization JSONObjectWithData:dataFail options:NSJSONReadingAllowFragments error:nil];
            [[ToolClass sharedInstance]showAlert:[NSString stringWithFormat:@"%@",dicFail[@"msg"]]];
        }
    }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
        [[ToolClass sharedInstance]showAlert:@"登录失败,请检查网络"];
    }];
}



//#pragma mark 登录成功后的操作
//- (void) actionAfterLoginSuccess {
//    
//    UserSelfMessageViewController * userSelfMessageViewController = [[UserSelfMessageViewController alloc]init];
//    
//    [self.navigationController popViewControllerAnimated:YES];
//    NSMutableArray * tabBarViewControllers = [NSMutableArray arrayWithArray:self.tabBarController.viewControllers];
//    huobanAuthViewController * authNavigationController = [[huobanAuthViewController alloc]initWithRootViewController:[[UserSelfMessageViewController alloc] init]];
//    
//    [authNavigationController.tabBarItem setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor yellowColor] }
//                                                       forState:UIControlStateNormal];
//    [authNavigationController.tabBarItem setImage:[[UIImage imageNamed:@"woNO"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ];
//    
//    [authNavigationController.tabBarItem setSelectedImage:[[UIImage imageNamed:@"woYES"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//    
//    [authNavigationController.tabBarItem setTitle:@"我"];
//    
//    
//    [tabBarViewControllers replaceObjectAtIndex:tabBarViewControllers.count-1 withObject:authNavigationController];
//    self.tabBarController.viewControllers = tabBarViewControllers;
//    //    self.tabBarController.tabBar.hidden = NO;
//    NSLog(@"登录成功后的操作");
//}

#pragma mark 找回密码按钮页面跳转
- (void) foundPassworldAction:(UIButton *)sender {
    NSLog(@"找回密码Action:");
    huobanRegisterViewController * huobanFoundPassworldVC = [[huobanRegisterViewController alloc]init];
    huobanFoundPassworldVC.title = @"找回密码";
    [self.navigationController pushViewController:huobanFoundPassworldVC animated:YES];
}

- (void) registerAction:(UIButton *)sender {
    huobanRegisterViewController * huobanRegisterVC = [[huobanRegisterViewController alloc]init];
    huobanRegisterVC.title = @"注册";
    [self.navigationController pushViewController:huobanRegisterVC animated:YES];
}

#pragma mark 密码清空按钮的事件
- (void) cancelPassWorld:(UIButton *)sender {
    self.huobanPassWorld.text = nil;
}


#pragma mark 键盘监听方法设置
//当键盘出现时调用
-(void)keyboardWillShow:(NSNotification *)aNotification{
    //第一个参数写输入view的父view即可，第二个写监听获得的notification，第三个写希望高于键盘的高度(只在被键盘遮挡时才启用,如控件未被遮挡,则无变化)
    //如果想不通输入view获得不同高度，可自己在此方法里分别判断区别
    [[CDPMonitorKeyboard defaultMonitorKeyboard] keyboardWillShowWithSuperView:self.view andNotification:aNotification higherThanKeyboard:0];
    
    //获取键盘高度
    NSDictionary *info = [aNotification userInfo];
    NSValue *value = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [value CGRectValue].size;
    
    
    CGRect _footViewNewFrame = self.huobanFootView.frame;
    
    _footViewNewFrame.origin.y = SCREEN_HEIGHT -  keyboardSize.height - _footViewNewFrame.size.height - 64;
    
    self.huobanFootView.frame = _footViewNewFrame;
    
    NSLog(@"huobanFootView.Frame:%@",NSStringFromCGRect(_footViewNewFrame));
    
    
    //    NSLog(@"keyboardCGSize:%@",NSStringFromCGSize(keyboardSize));
    
}
//当键退出时调用
-(void)keyboardWillHide:(NSNotification *)aNotification{
    [[CDPMonitorKeyboard defaultMonitorKeyboard] keyboardWillHide];
    
    
    self.huobanFootView.frame = CGRectMake(0, SCREEN_HEIGHT -50- 64, SCREEN_WIDTH, 50);
    
    NSLog(@"keyboardWillHide");
}
#pragma mark UITextFieldDelegate

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    
    //    if ([self.huobanPassWorld.text isEqualToString:@""]) {
    //        self.passWorldCancel.hidden = YES;
    //    }
    //    else {
    //        self.passWorldCancel.hidden = NO;
    //    }
    //    CGRect _footViewNewFrame = self.huobanFootView.frame;
    //    _footViewNewFrame.origin.y -= 194;
    //    self.huobanFootView.frame = _footViewNewFrame;
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if ([self.huobanPassWorld.text isEqualToString:@""]) {
        self.passWorldCancel.hidden = YES;
    }
    CGRect _footViewNewFrame = self.huobanFootView.frame;
    _footViewNewFrame.origin.y = SCREEN_HEIGHT - 64;
    self.huobanFootView.frame = _footViewNewFrame;
    
    
    if (self.huobanPassWorld.text.length > 1) {
        NSLog(@"passWorldChar>1");
    }
    
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([self.huobanPassWorld.text isEqualToString:@""]) {
        self.passWorldCancel.hidden = YES;
    }
    else {
        self.passWorldCancel.hidden = NO;
    }
    NSLog(@"输入中");
    
    if (self.huobanPassWorld.text.length == 5 && self.huobanPhoneNum.text.length!=0) {
        NSLog(@"shouldChangeCharactersInRange  passWorldChar = %zi",self.huobanPassWorld.text.length);
        [self.loginButton setBackgroundImage:[UIImage imageNamed:@"可点击确认"] forState:UIControlStateNormal];
    }
    
    
    return YES;
}


#pragma mark 取消键盘
- (void) resignTextFieldFirstResponder {
    
    NSLog(@"resignTextFieldFirstResponder");
    [self.huobanPassWorld resignFirstResponder];
    [self.huobanPhoneNum resignFirstResponder];
    
}


#pragma mark 登录成功后的操作
- (void) actionAfterLoginSuccess {
    
    UserSelfMessageViewController * userSelfMessageViewController = [[UserSelfMessageViewController alloc]init];
    
    [self.navigationController popViewControllerAnimated:YES];
    NSMutableArray * tabBarViewControllers = [NSMutableArray arrayWithArray:self.tabBarController.viewControllers];
    huobanAuthViewController * authNavigationController = [[huobanAuthViewController alloc]initWithRootViewController:[[UserSelfMessageViewController alloc] init]];
    
    [authNavigationController.tabBarItem setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor yellowColor] }
                                                       forState:UIControlStateNormal];
    [authNavigationController.tabBarItem setImage:[[UIImage imageNamed:@"woNO"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ];
    
    [authNavigationController.tabBarItem setSelectedImage:[[UIImage imageNamed:@"woYES"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [authNavigationController.tabBarItem setTitle:@"我"];
    
    
    [tabBarViewControllers replaceObjectAtIndex:tabBarViewControllers.count-1 withObject:authNavigationController];
    self.tabBarController.viewControllers = tabBarViewControllers;
    //    self.tabBarController.tabBar.hidden = NO;
    NSLog(@"登录成功后的操作");
    
    //登陆先清除缓存
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    [ud removeObjectForKey:@"HomePageCellDataModel"];
    [ud removeObjectForKey:@"ProjectDynamicWM"];
    
    [ud setInteger:1 forKey:@"runtimes"];
    
    
    NSMutableDictionary *dic = _resLoginDic[@"data"];
    //    dic = self.huoban
    
    NSMutableDictionary *dicPOST = dic[@"post"];
    
    DataModel *dataModelLoginSuccess = [[DataModel alloc] init];
    dataModelLoginSuccess.userInfomation.tokenID = _resLoginDic[@"token"];
    dataModelLoginSuccess.userInfomation.UserID = dic[@"_id"];
    dataModelLoginSuccess.userInfomation.userName = self.huobanPhoneNum.text;
    dataModelLoginSuccess.userInfomation.passWord = self.huobanPassWorld.text;
    dataModelLoginSuccess.userInfomation.ImagePreson = dic[@"image"];
    dataModelLoginSuccess.userInfomation.Name = dic[@"name"];
    dataModelLoginSuccess.userInfomation.payName = dicPOST[@"name"];
    dataModelLoginSuccess.userInfomation.payMobile = dicPOST[@"mobile"];
    dataModelLoginSuccess.userInfomation.add = dicPOST[@"address"];
    
    [dataModelLoginSuccess saveUserInfomation];
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"我_back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(popToLastNavigationController)];
    
    [self hideTabBar];
    
}

- (void) popToLastNavigationController {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    //    animated = YES;
    [self resignTextFieldFirstResponder];
    [self showTabBar];
}

//给指定对象添加下划线的方法


/*
 * 给对象添加下划线的方法
 *
 * @param object  要添加下划线的对象
 * @param color   下划线的颜色
 * @param hight   下划线的高度
 * @param isMainScreenWith   下划线是否按照屏幕宽度
 */
- (void) addUnderLineWithObject:(id)object color:(UIColor *)color hight:(CGFloat)hight isMainScreenWidth:(NSInteger)isMainScreenWidth isTopOrBottom:(NSInteger)isTop {
    NSInteger _lineWidth;
    NSInteger _hight;
    NSInteger _x = 0;
    NSInteger _y;
    NSInteger _isTop;
    
    _isTop = isTop;
    if ([object isKindOfClass:[UIView class]]) {
        UIView * _object = object;
        if (isMainScreenWidth == 1) {
            _lineWidth = SCREEN_WIDTH;
        }
        else {
            _lineWidth = _object.frame.size.width;
        }
        _hight = hight;
        if (isTop) {
            _y = 0;
        }
        else {
            _y = _object.frame.size.height - _hight;
        }
    }
    else if ([object isKindOfClass:[UITextField class]]) {
        UITextField * _object = object;
        
        if (isMainScreenWidth == 1) {
            _lineWidth = SCREEN_WIDTH;
        }
        else {
            _lineWidth = _object.frame.size.width;
        }
        _hight = hight;
        if (isTop) {
            _y = 0;
        }
        else {
            _y = _object.frame.size.height - _hight;
        }
    }
    UIButton * button = [[UIButton alloc]initWithFrame:CGRectMake(_x, _y, _lineWidth, _hight)];
    [button setBackgroundColor:color];
    UIButton * buttonTest = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 200, 2)];
    [buttonTest setBackgroundColor:color];
    //    [(UIControl *) object addSubview:button];
    
    [((UIControl *) object) addSubview:button];
    
}


- (NSAttributedString *) attributedPlaceholderWithString:(NSString *)str {
    
    NSMutableAttributedString * placeholderAttributedString = [[NSMutableAttributedString alloc]initWithString:str];
    [placeholderAttributedString addAttribute:NSFontAttributeName value:PlaceHolderFont range:NSMakeRange(0, placeholderAttributedString.string.length)];
    [placeholderAttributedString addAttribute:NSForegroundColorAttributeName value:PlaceHolderColor range:NSMakeRange(0, placeholderAttributedString.string.length)];
    
    return placeholderAttributedString;
}
- (void)hideTabBar {
    if (self.tabBarController.tabBar.hidden == YES) {
        return;
    }
    self.tabBarController.tabBar.hidden = YES;
}

- (void)showTabBar {
    if (self.tabBarController.tabBar.hidden == NO)
    {
        return;
    }
    self.tabBarController.tabBar.hidden = NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField endEditing:YES];
    return NO;
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
