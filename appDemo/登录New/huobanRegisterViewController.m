//
//  RegisterViewController.m
//  huoban登录注册部分
//
//  Created by 马锦航 on 15/12/17.
//  Copyright © 2015年 马锦航. All rights reserved.
//

#import "huobanRegisterViewController.h"
#import "CDPMonitorKeyboard.h"
#import "ToolClass.h"
#import "HttpClassSelf.h"
#import "UserInfomation.h"
#import "DataModel.h"
#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
#define FormItemHeight 55
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
// #B1AC5B  177,172,91
#define PlaceHolderColor  [UIColor colorWithRed:177/255.f green:172/255.f blue:91/255.f alpha:1.0]


#define PlaceHolderFont     [UIFont fontWithName:@"PingFangSC-Light" size:18.f]


//#B1AC5B  RGB:177,172,91
static     NSInteger isChangeBound = 0;
@interface  huobanRegisterViewController()<UITextFieldDelegate>
{
    HttpClassSelf *httpClass;
    NSTimer *_timerCheckIdRegister;
    NSTimer *_timerCheckId;
    NSMutableDictionary *_resRegisterDic;
    UserInfomation *_userInfomationRegister;
    DataModel *_dataModelRegister;
    NSMutableDictionary *_resFotgotPWDDic;
    DataModel *_dataModelFoegotPW;

}
@property (nonatomic,strong) UITextField * huobanPhoneNum;          //手机号

@property (nonatomic,strong) UITextField * huobanSecurity;          //验证码

@property (nonatomic,strong) UIButton * securityButton;             //验证码按钮

@property (nonatomic,strong) UITextField * huobanPassWorld;         //密码

@property (nonatomic,strong) UITextField * huobanPassWorldCheck;    //检查密码

@property (nonatomic,strong) UIView * huobanSecurityView;               //验证视图（上部分）

@property (nonatomic,strong) UIButton * huobanSubmit;                //注册/找回密码按钮

@property (nonatomic,strong) UIView * huobanFootView;               //底部视图（底部）

@property (nonatomic,strong) UIButton * huobanFoundPassWorld;       //找回密码

@property (nonatomic,strong) UIButton * huobanRegister;             //注册密码


@end

@implementation huobanRegisterViewController

#pragma mark - 懒加载

- (UITextField *)huobanPhoneNum {
    if (!_huobanPhoneNum) {
        _huobanPhoneNum = [[UITextField alloc]initWithFrame:CGRectMake(12, 0, SCREEN_WIDTH - 12*2, FormItemHeight)];
        [_huobanPhoneNum setFont:[UIFont fontWithName:@"PingFangSC-Light" size:14.7]];
        NSLog(@"%@",NSStringFromCGRect(_huobanPhoneNum.frame));
        _huobanPhoneNum.delegate = self;
        _huobanPhoneNum.attributedPlaceholder = [self attributedPlaceholderWithString:@"手机号"];
        //        [_huobanPhoneNum setBackgroundColor:[UIColor grayColor]];
        _huobanPhoneNum.returnKeyType = UIReturnKeyDone;
        
    }
    return _huobanPhoneNum;
}



- (UITextField *)huobanSecurity {
    if (!_huobanSecurity) {
        _huobanSecurity = [[UITextField alloc]initWithFrame:CGRectMake(12,self.huobanPhoneNum.frame.origin.y+self.huobanPhoneNum.frame.size.height, SCREEN_WIDTH - 12*2, FormItemHeight)];
        NSLog(@"%@",NSStringFromCGRect(_huobanPassWorld.frame));
//        [_huobanSecurity setPlaceholder:@"验证码"];
        
        [_huobanSecurity setFont:[UIFont fontWithName:@"PingFangSC-Light" size:14.7]];
        _huobanSecurity.attributedPlaceholder = [self attributedPlaceholderWithString:@"验证码"];
        _huobanSecurity.delegate = self;
        _huobanSecurity.returnKeyType = UIReturnKeyDone;
        
    }
    return _huobanSecurity;
}

#pragma mark 验证码按钮
- (UIButton *)securityButton {
    if (!_securityButton) {
        CGFloat _securityButtonOffsetTop = 10;
        CGFloat _securityButotnWith = 100;
        CGFloat _securityButtonHight = self.huobanSecurity.frame.size.height - _securityButtonOffsetTop*2;
    _securityButton = [[UIButton alloc]initWithFrame:CGRectMake(self.huobanSecurity.frame.origin.x+self.huobanSecurity.frame.size.width-_securityButotnWith,self.huobanSecurity.frame.origin.y + _securityButtonOffsetTop, _securityButotnWith, _securityButtonHight)];
//        [_securityButton setBackgroundColor:[UIColor redColor]];
        [_securityButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        _securityButton.titleLabel.tintColor = [UIColor blackColor];
        [_securityButton addTarget:self action:@selector(getPhoneCheckNumber:) forControlEvents:UIControlEventTouchUpInside];
        [_securityButton setTitle:@"获取" forState:UIControlStateNormal];

    }
    return _securityButton;
}

- (UITextField *)huobanPassWorld {
    if (!_huobanPassWorld) {
        _huobanPassWorld = [[UITextField alloc]initWithFrame:CGRectMake(12,self.huobanSecurity.frame.origin.y+self.huobanSecurity.frame.size.height, SCREEN_WIDTH - 12*2, FormItemHeight)];
//        [_huobanPassWorld setPlaceholder:@"6-16位密码"];
        
        [_huobanPassWorld setFont:[UIFont fontWithName:@"PingFangSC-Light" size:14.7]];
        _huobanPassWorld.secureTextEntry = YES;
        _huobanPassWorld.delegate = self;
        _huobanPassWorld.attributedPlaceholder = [self attributedPlaceholderWithString:@"6-16位密码"];
        _huobanPassWorld.returnKeyType = UIReturnKeyDone;
    }
    return _huobanPassWorld;
}

- (UITextField *)huobanPassWorldCheck {
    if (!_huobanPassWorldCheck) {
        _huobanPassWorldCheck = [[UITextField alloc]initWithFrame:CGRectMake(12,self.huobanPassWorld.frame.origin.y+self.huobanPassWorld.frame.size.height, SCREEN_WIDTH - 12*2, FormItemHeight)];
//        [_huobanPassWorldCheck setPlaceholder:@"确认密码"];
        _huobanPassWorldCheck.secureTextEntry = YES;
        [_huobanPassWorldCheck setFont:[UIFont fontWithName:@"PingFangSC-Light" size:14.7]];
        _huobanPassWorldCheck.attributedPlaceholder = [self attributedPlaceholderWithString:@"确认密码"];
        _huobanPassWorldCheck.delegate = self;
        _huobanPassWorldCheck.returnKeyType = UIReturnKeyDone;
    }
    return _huobanPassWorldCheck;

}

#pragma mark 上部分验证信息视图

- (UIView *)huobanSecurityView {
    if (!_huobanSecurityView) {
        _huobanSecurityView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.huobanPassWorldCheck.frame.origin.y + self.huobanPassWorldCheck.frame.size.height)];
//        [_huobanSecurityView setBackgroundColor:[UIColor orangeColor]];
        
    }
    return _huobanSecurityView;
}




#pragma mark 中部登录视图

//320*568   navi 64 + 60 *3 + 36 +12 +
//- (UIButton *)loginButton {
//    if (!_loginButton) {
//        NSInteger loginButtonHigth = 50;
//        _loginButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2 - loginButtonHigth/2 , 36, loginButtonHigth, loginButtonHigth)];
//        [_loginButton setBackgroundColor:[UIColor redColor]];
//        _loginButton.layer.cornerRadius = loginButtonHigth/2;
//    }
//    return _loginButton;
//}

//- (UIView *)loginView {
//    if (!_loginView) {
//        _loginView = [[UIView alloc]initWithFrame:CGRectMake(0, 120, SCREEN_WIDTH, SCREEN_HEIGHT - 120 - 50 - 64)];
//        [_loginView setBackgroundColor:[UIColor greenColor]];
//        NSLog(@"mainScreen.bounds:%@",NSStringFromCGRect([UIScreen mainScreen].bounds));
//        [_loginView addSubview:self.loginButton];
//    }
//    return _loginView;
//}

- (UIButton *)huobanSubmit {
    if (!_huobanSubmit) {
        NSInteger loginButtonHigth = 50;
        _huobanSubmit = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2 - loginButtonHigth/2 , 36, loginButtonHigth, loginButtonHigth)];
        //        [_loginButton setBackgroundColor:[UIColor redColor]];
        [_huobanSubmit setBackgroundImage:[UIImage imageNamed:@"可点击确认@2x"] forState:UIControlStateNormal];
        _huobanSubmit.layer.cornerRadius = loginButtonHigth/2;
        [_huobanSubmit addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _huobanSubmit;
}

#pragma mark 底部视图
- (UIView *)huobanFootView {
    if (!_huobanFootView) {
        _huobanFootView = [[UIView alloc]initWithFrame:CGRectMake(0,self.huobanPassWorldCheck.frame.origin.y + self.huobanPassWorldCheck.frame.size.height, SCREEN_WIDTH, SCREEN_HEIGHT - _huobanFootView.frame.origin.y)];
        UIButton * resignKeyboardResbonder = [[UIButton alloc]initWithFrame:_huobanFootView.bounds];
//                [resignKeyboardResbonder setBackgroundColor:[UIColor blackColor]];
        [resignKeyboardResbonder addTarget:self action:@selector(resignTextFieldFirstResponder) forControlEvents:UIControlEventTouchUpInside];
        [_huobanFootView addSubview:resignKeyboardResbonder];
        [_huobanFootView setBackgroundColor:[UIColor yellowColor]];
        [_huobanFootView addSubview:self.huobanSubmit];
//        [_huobanFootView addSubview:self.huobanFoundPassWorld];
//        [_huobanFootView addSubview:self.huobanRegister];
    }
    return _huobanFootView;
}
#if 0



- (UIButton *)huobanFoundPassWorld {
    if (!_huobanFoundPassWorld) {
        _huobanFoundPassWorld = [[UIButton alloc]initWithFrame:CGRectMake(12, 0, 100, 50)];
        [_huobanFoundPassWorld setTitle:@"找回密码" forState:UIControlStateNormal];
        [_huobanFoundPassWorld setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //        [_huobanFoundPassWorld setBackgroundColor:[UIColor greenColor]];
    }
    return _huobanFoundPassWorld;
    
}

- (UIButton *)huobanRegister {
    if (!_huobanRegister) {
        _huobanRegister = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 12 - 100, 0, 100, 50)];
        [_huobanRegister setTitle:@"注册" forState:UIControlStateNormal];
        [_huobanRegister setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        //        [_huobanRegister setBackgroundColor:[UIColor greenColor]];
    }
    return _huobanRegister;
}
#endif


- (instancetype)init
{
    self = [super init];
    if (self) {
        ;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor yellowColor]];
    httpClass = [[HttpClassSelf alloc]init];
    _dataModelRegister = [[DataModel alloc] init];
    _dataModelFoegotPW = [[DataModel alloc] init];
    [self addSubviews];
    
    //配置验证码计时器并添加到主线程中
    [self timerSetting];
    
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    //增加监听，当键退出时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    UIColor * underLineColor = RGBCOLOR(177, 172, 91);
    [self addUnderLineWithObject:self.huobanPhoneNum color:underLineColor hight:1 isMainScreenWidth:0];
    [self addUnderLineWithObject:self.huobanPassWorld color:underLineColor hight:1 isMainScreenWidth:0];
    [self addUnderLineWithObject:self.huobanSecurity color:underLineColor hight:1 isMainScreenWidth:0];
    [self addUnderLineWithObject:self.huobanPassWorldCheck color:underLineColor hight:1 isMainScreenWidth:0];
    
    
}

#pragma mark 加载视图
- (void) addSubviews {
    //添加电话号码文本框
    [self.huobanSecurityView addSubview:self.huobanPhoneNum];
    
    //添加验证码文本框
    [self.huobanSecurityView addSubview:self.huobanSecurity];
    //添加验证码按钮
    [self.huobanSecurityView addSubview:self.securityButton];
    //添加密码文本框
    [self.huobanSecurityView addSubview:self.huobanPassWorld];
    [self.huobanSecurityView addSubview:self.huobanPassWorldCheck];
    
    
    [self.view addSubview:self.huobanSecurityView];
    
    //添加底部登录视图到当前视图
    [self.view addSubview:self.huobanFootView];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//给指定对象添加下划线的方法


/*
 * 给对象添加下划线的方法
 *
 * @param object  要添加下划线的对象
 * @param color   下划线的颜色
 * @param hight   下划线的高度
 * @param isMainScreenWith   下划线是否按照屏幕宽度
 */
- (void) addUnderLineWithObject:(id)object color:(UIColor *)color hight:(CGFloat)hight isMainScreenWidth:(NSInteger)isMainScreenWidth {
    NSInteger _lineWidth;
    NSInteger _hight;
    NSInteger _x;
    NSInteger _y;
    
    
    
    if ([object isKindOfClass:[UIView class]]) {
        UIView * _object = object;
        if (isMainScreenWidth == 1) {
            _lineWidth = SCREEN_WIDTH;
            _x = 0;
        }
        else {
            _lineWidth = _object.frame.size.width;
            _x = _object.frame.origin.x;
        }
        _hight = hight;
        _y = _object.frame.origin.y + _object.frame.size.height - _hight;
    }
    else if ([object isKindOfClass:[UITextField class]]) {
        UITextField * _object = object;
        
        if (isMainScreenWidth == 1) {
            _lineWidth = SCREEN_WIDTH;
            _x = 0;
        }
        else {
            _lineWidth = _object.frame.size.width;
            _x = _object.frame.origin.x;
        }
        _hight = hight;
        _y = _object.frame.origin.y + _object.frame.size.height - _hight;
    }
    UIButton * button = [[UIButton alloc]initWithFrame:CGRectMake(_x, _y, _lineWidth, _hight)];
    [button setBackgroundColor:color];
    //    [(UIControl *) object addSubview:button];
    [((UIControl *) object).superview addSubview:button];
}

#pragma mark 取消键盘
- (void) resignTextFieldFirstResponder {
    
//    NSLog(@"resignTextFieldFirstResponder");
    [self.huobanPassWorld resignFirstResponder];
    [self.huobanPhoneNum resignFirstResponder];
    [self.huobanSecurity resignFirstResponder];
    [self.huobanPassWorldCheck resignFirstResponder];
    
}

#pragma mark 键盘监听方法设置
//当键盘出现时调用
-(void)keyboardWillShow:(NSNotification *)aNotification{
    //第一个参数写输入view的父view即可，第二个写监听获得的notification，第三个写希望高于键盘的高度(只在被键盘遮挡时才启用,如控件未被遮挡,则无变化)
    //如果想不通输入view获得不同高度，可自己在此方法里分别判断区别
    [[CDPMonitorKeyboard defaultMonitorKeyboard] keyboardWillShowWithSuperView:self.view andNotification:aNotification higherThanKeyboard:0];
    if (isChangeBound) {
        self.view.bounds = CGRectMake(0, -20, SCREEN_WIDTH, SCREEN_HEIGHT);
    }
    NSLog(@"keyboardWillShow");
    
}
//当键退出时调用
-(void)keyboardWillHide:(NSNotification *)aNotification{
    [[CDPMonitorKeyboard defaultMonitorKeyboard] keyboardWillHide];
    self.view.bounds = CGRectMake(0, -60, SCREEN_WIDTH, SCREEN_HEIGHT);
    NSLog(@"keyboardWillHide");
}
#pragma mark - 按钮点击事件

#pragma mark 注册／找回密码
- (void) submitAction:(UIButton *)sender {
    NSLog(@"注册／找回密码");
    NSString *strCheckKEY;
    
    NSLog(@"%@页面%@",self.title,self.title);
    if ([self.title isEqualToString:@"注册"]) {
        strCheckKEY = @"register";
        if(![self checkTextField:self.huobanPhoneNum.text andPW:self.huobanPassWorldCheck.text andCheckid:self.huobanSecurity.text]){
            return;
        }
        
        if(![self.huobanPassWorldCheck.text isEqualToString:self.huobanPassWorld.text]){
            [[ToolClass sharedInstance] showAlert:@"确认密码输入不一致，请重新输入"];
            
            return;
        }
        
        [httpClass CheckIDReviewForFixPWD:self.huobanPhoneNum.text getKey:strCheckKEY getCode:self.huobanSecurity.text CallBackYES:^(MKNetworkOperation *operatioin){
            
            NSData *dataCheckID = [operatioin responseData];
            
            NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:dataCheckID options:NSJSONReadingAllowFragments error:nil];
            
            if([dic[@"status"] isEqualToString:@"success"]){
                
                //http接口
                [httpClass RegisterSetMobile:self.huobanPhoneNum.text pwd:self.huobanPassWorld.text umengid:@"" mobibuild:[UIDevice currentDevice].model mobitype:@"iOS" CallBackYES:^(MKNetworkOperation *operatioin) {
                    NSLog(@"打印打印%@",_resRegisterDic);
                    //返回data数据
                    NSData *data = [operatioin responseData];
                    //词典对象
                    _resRegisterDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                    
                    NSLog(@"返回注册注册JSON%@",_resRegisterDic);
                    
                    
                    if([_resRegisterDic[@"status"] isEqualToString:@"success"]){
                        //                    NSLog(@"打印注册成功JSON%@",_resRegisterDic);
                        
                        NSMutableDictionary *dicData = _resRegisterDic[@"data"];
                        
                        //                    NSLog(@"%@,%@,%@",dicData[@"name"],dicData[@"image"],dicData[@"_id"]);
                        
                        _userInfomationRegister.userName = dicData[@"mobile"];
                        _userInfomationRegister.ImagePreson = dicData[@"image"];
                        _userInfomationRegister.Name = dicData[@"name"];
                        _userInfomationRegister.passWord = self.huobanPassWorld.text;
                        _userInfomationRegister.tokenID = _resRegisterDic[@"token"];
                        _userInfomationRegister.UserID = dicData[@"_id"];
                        
                        _dataModelRegister.userInfomation = _userInfomationRegister;
                        [_dataModelRegister saveUserInfomation];
                        
                        [[ToolClass sharedInstance]showAlert:[NSString stringWithFormat:@"%@成功，请登录",self.title]];
                        //                    [self.delegate RegisterAccountViewController:self getPOST:_resRegisterDic];
                        [self dismissViewControllerAnimated:YES completion:nil];
                        
                    }else{
                        NSData *dataFail = [operatioin responseData];
                        NSMutableDictionary *dicFail = [NSJSONSerialization JSONObjectWithData:dataFail options:NSJSONReadingAllowFragments error:nil];
                        [[ToolClass sharedInstance]showAlert:[NSString stringWithFormat:@"%@",dicFail[@"msg"]]];
                    }
                }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
                    _resRegisterDic = nil;
                    
                    [[ToolClass sharedInstance] showAlert:@"注册失败，请检查网络"];
                }];
                
            }else{
                
                [[ToolClass sharedInstance] showAlert:@"验证码验证失败，此账号已被注册"];
            }
            
        }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
            
            [[ToolClass sharedInstance]  showAlert:@"验证码验证失败，请检查网络"];
            
        }];
        
    }
    else {
        strCheckKEY = @"forgetpwd";
        NSLog(@"忘记密码");
        NSString *fixPWDKey = @"resetpwd";
        
        if(![self checkTextField:self.huobanPhoneNum.text andPW:self.huobanPassWorld.text andCheckid:self.huobanSecurity.text]){
            return;
        }
        
        NSLog(@"%@----%@",self.huobanPassWorld.text,self.huobanPassWorldCheck.text);
        if(![self.huobanPassWorld.text isEqualToString:self.huobanPassWorldCheck.text]){
            [[ToolClass sharedInstance] showAlert:@"确认密码输入不一致，请重新输入"];
            
            return;
        }
        
        
        [httpClass CheckIDReviewForFixPWD:self.huobanPhoneNum.text getKey:fixPWDKey getCode:self.huobanSecurity.text CallBackYES:^(MKNetworkOperation *operatioin){
            
            NSData *dataCheckID = [operatioin responseData];
            
            NSMutableDictionary *dicCheckID = [NSJSONSerialization JSONObjectWithData:dataCheckID options:NSJSONReadingAllowFragments error:nil];
            
            if([dicCheckID[@"status"]isEqualToString:@"success"]){
                
                [httpClass forgotPWDSetMobile:self.huobanPhoneNum.text pwd:self.huobanPassWorld.text umengid:@"" mobibuild:[UIDevice currentDevice].model mobitype:@"iOS" CallBackYES:^(MKNetworkOperation *operatioin){
                    
                    NSData *data = [operatioin responseData];
                    
                    //词典对象
                    _resFotgotPWDDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                    
                    if([_resFotgotPWDDic[@"status"] isEqualToString:@"success"]){
                        _dataModelFoegotPW.userInfomation.userName = self.huobanPhoneNum.text;
                        _dataModelFoegotPW.userInfomation.passWord = self.huobanPassWorld.text;
                        
                        [_dataModelFoegotPW saveUserInfomation];
                        
                        [[ToolClass sharedInstance]showAlert:@"更改密码成功"];
                        
                        //                    [self.delegate ForgotPWDViewController:self getPOST:_resFotgotPWDDic];
                        
                        //                    [self.delegate ForgotPWDViewController:self getPOST:_resFotgotPWDDic];
                        
                        //                    [self.delegate forgotPWDViewController:self getDic:_resFotgotPWDDic];
                        [self dismissViewControllerAnimated:YES completion:nil];
                        
                    }else{
                        [[ToolClass sharedInstance]showAlert:@"更改密码失败，请检查手机号"];
                    }
                }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
                    [[ToolClass sharedInstance]showAlert:@"更改密码失败，请检查网络"];
                }];
            }else{
                NSData *dataFail = [operatioin responseData];
                
                NSMutableDictionary *dicFail = [NSJSONSerialization JSONObjectWithData:dataFail options:NSJSONReadingAllowFragments error:nil];
                
                [[ToolClass sharedInstance]showAlert:[NSString stringWithFormat:@"%@",dicFail[@"msg"]]];
                
            }
        }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
            [[ToolClass sharedInstance]showAlert:@"验证码验证失败，请检查网络"];
            
        }];

    }
    
}

#pragma mark 验证码

- (void) getPhoneCheckNumber:(UIButton *)sender {
    NSLog(@"获取验证码按钮");
    NSString *strKey;
    NSLog(@"%@页面验证码按钮",self.title);

    if ([self.title isEqualToString:@"注册"]) {
        
        strKey = @"register";
        if(![self.huobanPhoneNum.text isEqualToString:@""]){
            NSLog(@"不为空");
            [httpClass GetCheckIDSetTelephoneNumForCreate:self.huobanPhoneNum.text getKey:strKey CallBackYES:^(MKNetworkOperation *operatioin){
                
                NSLog(@"%@",operatioin);
                NSData *data = [operatioin responseData];
                
                NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                NSLog(@"打印回传JSON%@",dic);
                if([dic[@"status"] isEqualToString:@"success"]){
                    [[ToolClass sharedInstance] showAlert:@"验证码已发送，请尽快完成操作，60秒后可再次获取验证码"];
                    //调用改变验证码按钮状态的方法
                    [self setCheckIDButtonEnabled];
                }else{
                    
                    NSData *dataFail = [operatioin responseData];
                    NSMutableDictionary *dicFail = [NSJSONSerialization JSONObjectWithData:dataFail options:NSJSONReadingAllowFragments error:nil];
                    [[ToolClass sharedInstance]showAlert:[NSString stringWithFormat:@"%@",dicFail[@"msg"]]];
                    NSLog(@"网络连接失败");
                }
            }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
                [[ToolClass sharedInstance] showAlert:@"验证码获取失败，请检查网络"];
            }];
        }else{
            [[ToolClass sharedInstance] showAlert:@"请填写账号"];
        }
        
    }
    else {
        strKey = @"forgetpwd";
        NSString *checkIdkey = @"resetpwd";
        //        NSString *checkIdkey = @"register";
        
        if(![self.huobanPhoneNum.text isEqualToString:@""]){
            [httpClass GetCheckIDSetTelephoneNumForFixPWD:self.huobanPhoneNum.text getKey:checkIdkey CallBackYES:^(MKNetworkOperation *operatioin){
                
                NSData *data = [operatioin responseData];
                
                NSMutableDictionary *resdic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                
                NSLog(@"返回:%@",resdic);
                
                if([resdic[@"status"] isEqualToString:@"success"]){
                    //调用改变验证码按钮状态的方法
                    [[ToolClass sharedInstance] showAlert:@"验证码已发送，请尽快完成操作，60秒后可再次获取验证码"];
                    [self setCheckIDButtonEnabled];

                    
                    
//                    [self.buttonCheckID setBackgroundColor:[UIColor colorWithRed:199.0/255 green:199.0/255 blue:199.0/255 alpha:1]];
                    
                    //延时对象
                    _timerCheckId = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(timerButtonCheckID) userInfo:nil repeats:NO];
                    
//                    self.buttonCheckID.enabled = NO;
                    
                }else{
                    
                    NSData *dataFail = [operatioin responseData];
                    
                    NSMutableDictionary *dicFail = [NSJSONSerialization JSONObjectWithData:dataFail options:NSJSONReadingAllowFragments error:nil];
                    
                    [[ToolClass sharedInstance]showAlert:[NSString stringWithFormat:@"%@",dicFail[@"msg"]]];
                    
                }
                
            }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
                [[ToolClass sharedInstance] showAlert:@"获取验证码失败，请检查网络"];
            }];
            
        }else{
            [[ToolClass sharedInstance] showAlert:@"请填写账号"];
        }
        
    }
    

    
    
}


-(void)timerButtonCheckID{
    
    //    [self.buttonCheckID setBackgroundColor:[UIColor colorWithRed:51.0/255 green:163.0/255 blue:219.0/255 alpha:1]];
    //change
    //    self.buttonCheckID.enabled = NO;
    //    [_timerCheckIdRegister invalidate];
    static NSInteger _timerRecentTime = 60;
    
    if ([self.securityButton.titleLabel.text isEqualToString:@"1s"]) {
        
//        [_timerCheckIdRegister invalidate];
        //停止计时器
//                [_timerCheckIdRegister setFireDate:[NSDate distantFuture]];
//                [_timerCheckIdRegister invalidate];
        [self.securityButton setTitle:@"验证码" forState:UIControlStateNormal];
//        UIButton * _button = [[UIButton alloc]init];
        //        [self.buttonCheckID setBackgroundColor:_button.backgroundColor];
        [self.securityButton setEnabled:YES];
        return;
        //        _timerRecentTime = 60;
    }
    else {
        //        NSLog(@"%zi",self.buttonCheckID.titleLabel.text);
        //        NSString * _checkIDTitle = [NSString stringWithFormat:@"%zi",([self.buttonCheckID.titleLabel.text integerValue]-1)];
        _timerRecentTime -- ;
        [self.securityButton setTitle:[NSString stringWithFormat:@"%zis",_timerRecentTime] forState:UIControlStateNormal];
    }
    NSLog(@"timer");
    
}

- (void) timerSetting{
    _timerCheckIdRegister = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(timerButtonCheckID) userInfo:nil repeats:YES];
    [_timerCheckIdRegister setFireDate:[NSDate distantFuture]];
    [[NSRunLoop currentRunLoop] addTimer:_timerCheckIdRegister forMode:NSDefaultRunLoopMode];
}
#pragma mark UITextFieldDelegate

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
//    NSLog(@"textFieldShouldBeginEditing");
//    NSLog(@"%@",NSStringFromCGRect(self.view.bounds));
    if (textField == self.huobanPassWorld || textField == self.huobanPassWorldCheck) {
        isChangeBound = 1;
    }
    else {
        isChangeBound = 0;
    }
    return YES;
}


#pragma mark textField检查
-(BOOL)checkTextField:(NSString*)username andPW:(NSString*)password andCheckid:(NSString*)checkID{
    BOOL returnChose = YES;
    
    if(username == nil){
        username = @"";
    }else if(password == nil){
        password = @"";
    }else if (checkID == nil){
        checkID = @"";
    }
    
    if([username isEqualToString:@""] && ![password isEqualToString:@""] && ![checkID isEqualToString:@""]){
        [[ToolClass sharedInstance]showAlert:@"手机号不可为空"];
        returnChose = NO;
    }else if(![username isEqualToString:@""] && [password isEqualToString:@""] && ![checkID isEqualToString:@""]){
        [[ToolClass sharedInstance]showAlert:@"密码不可为空"];
        returnChose = NO;
    }else if(![username isEqualToString:@""] && ![password isEqualToString:@""] && [checkID isEqualToString:@""]){
        [[ToolClass sharedInstance]showAlert:@"验证码不可为空"];
        returnChose = NO;
    }else if(![username isEqualToString:@""] && ![password isEqualToString:@""] && ![checkID isEqualToString:@""]){
        if ([password length] <6 || [password length]> 16){
            [[ToolClass sharedInstance]showAlert:@"'密码位数不正确'密码为6~16字母&数字组合，请重新填写"];
            returnChose = NO;
        }
    }else if([username isEqualToString:@""] && [password isEqualToString:@""] && [checkID isEqualToString:@""]){
        [[ToolClass sharedInstance]showAlert:@"请正确填写'账号''验证码''密码'信息"];
        returnChose = NO;
    }else if(![username isEqualToString:@""] && [password isEqualToString:@""] && [checkID isEqualToString:@""]){
        [[ToolClass sharedInstance]showAlert:@"请正确填写'验证码''密码'信息"];
        returnChose = NO;
    }
    return returnChose;
}

//
- (void) setCheckIDButtonEnabled {
    [self.securityButton setEnabled:NO];
    //    [self.buttonCheckID setBackgroundColor:[UIColor colorWithRed:199.0/255 green:199.0/255 blue:199.0/255 alpha:1]];
    [self.securityButton setTitle:@"60s" forState:UIControlStateNormal];
    [_timerCheckIdRegister setFireDate:[NSDate distantPast]];
    [self.securityButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
//    NSLog(@"textFieldShouldEndEditing");
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    
    [textField endEditing:YES];
    self.view.bounds = [UIScreen mainScreen].bounds;
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"我_back@2x"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(popToLastNavigationController)];
    [self hideTabBar];
}

- (void) popToLastNavigationController {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillDisappear:(BOOL)animated {
    [_timerCheckIdRegister setFireDate:[NSDate distantFuture]];
    [self showTabBar];
}


- (void)hideTabBar {
    if (self.tabBarController.tabBar.hidden == YES) {
        return;
    }
//    UIView *contentView;
//    if ( [[self.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]] )
//        contentView = [self.tabBarController.view.subviews objectAtIndex:1];
//    else
//        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
//    contentView.frame = CGRectMake(contentView.bounds.origin.x,  contentView.bounds.origin.y,  contentView.bounds.size.width, contentView.bounds.size.height + self.tabBarController.tabBar.frame.size.height);
    self.tabBarController.tabBar.hidden = YES;
    
}

- (void)showTabBar

{
    if (self.tabBarController.tabBar.hidden == NO)
    {
        return;
    }
//    UIView *contentView;
//    if ([[self.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]])
//        
//        contentView = [self.tabBarController.view.subviews objectAtIndex:1];
//    
//    else
//        
//        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
//    contentView.frame = CGRectMake(contentView.bounds.origin.x, contentView.bounds.origin.y,  contentView.bounds.size.width, contentView.bounds.size.height - self.tabBarController.tabBar.frame.size.height);
    self.tabBarController.tabBar.hidden = NO;
    
}

- (NSAttributedString *) attributedPlaceholderWithString:(NSString *)str {
    NSMutableAttributedString * placeholderAttributedString = [[NSMutableAttributedString alloc]initWithString:str];
    [placeholderAttributedString addAttribute:NSFontAttributeName value:PlaceHolderFont range:NSMakeRange(0, placeholderAttributedString.string.length)];
    [placeholderAttributedString addAttribute:NSForegroundColorAttributeName value:PlaceHolderColor range:NSMakeRange(0, placeholderAttributedString.string.length)];
    return placeholderAttributedString;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
