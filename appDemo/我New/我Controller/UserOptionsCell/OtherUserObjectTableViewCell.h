//
//  OtherUserObjectTableViewCell.h
//  huoban
//
//  Created by 马锦航 on 16/1/18.
//  Copyright © 2016年 lyc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HuoBanUserProjectData.h"


@interface OtherUserObjectTableViewCell : UITableViewCell

@property (nonatomic,strong) HuoBanUserProjectData * model;

@property (nonatomic,strong) UIButton * objectJoin;


@end
