//
//  OtherUserObjectTableViewCell.m
//  huoban
//
//  Created by 马锦航 on 16/1/18.
//  Copyright © 2016年 lyc. All rights reserved.
//

#import "OtherUserObjectTableViewCell.h"


#import "UIImageView+WebCache.h"
#import "SDImageCache.h"

#define RGB(r,g,b) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.f alpha:1.0]

//fill:#33A3DB  RGB:51,163,219  去社群
#define objectJoin_group_bgColor  [UIColor colorWithRed:51/255.f green:163/255.f blue:219/255.f alpha:1.0]

//fill:#4FC2B1  RGB:79,194,177  支持更多
#define objectJoin_support_bgColor [UIColor colorWithRed:79/255.f green:194/255.f blue:177/255.f alpha:1.0]

// MainScreen Height&Width
#define Main_Screen_Height      [[UIScreen mainScreen] bounds].size.height
#define Main_Screen_Width       [[UIScreen mainScreen] bounds].size.width



//字体宏
#define PingFangSC_Regular(s)     [UIFont fontWithName:@"PingFangSC-Regular" size:s]
//字体宏
#define PingFangSC_Light(s)     [UIFont fontWithName:@"PingFangSC-Light" size:s]

//判断iphone 屏幕大小宏定义

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)


#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)

//#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0) ?

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)


@interface OtherUserObjectTableViewCell ()

@property (nonatomic,strong) UIImageView * objectImage;

@property (nonatomic,strong) UILabel * objectStateDiscribe;

@property (nonatomic,strong) UILabel * objectState;

@property (nonatomic,strong) UIButton * isFollow;           //是否关注按钮

@property (nonatomic,strong) UILabel * isSuccess;           //成功显示成功  固定

@end

@implementation OtherUserObjectTableViewCell


- (void)setModel:(HuoBanUserProjectData *)model {
    _model = model;
    
    NSLog(@"%@",model.title);
    
    [self.objectImage sd_setImageWithURL:[NSURL URLWithString:model.images[0]]];
    [self.objectStateDiscribe setText:[NSString stringWithFormat:@"%@",model.title]];
    [self.objectStateDiscribe sizeToFit];
    
    [self objectStateStringWithModel:model];
    
    
    [self.objectState sizeToFit];
    
    //    [self compareNowWithDate:[model.endDate dataUsingEncoding:[NSStringEncodingDetectionAllowLossyKey]]];
    //    self compareNowWithDate:[model.endDate ]
    
}

//项目图片
- (UIImageView *)objectImage {
    if (!_objectImage) {
        _objectImage = [[UIImageView alloc]initWithFrame:CGRectMake(12, 12, 120, 120)];
        //setCornerRadius:YES
        //        _objectImage.layer setcor
        [_objectImage.layer setMasksToBounds:YES];
        _objectImage.layer.cornerRadius = 6;
        
        //        [_objectImage setBackgroundColor:[UIColor grayColor]];
    }
    return _objectImage;
}

//项目描述
-(UILabel *)objectStateDiscribe {
    if (!_objectStateDiscribe) {
//        _objectStateDiscribe = [[UILabel alloc]initWithFrame:CGRectMake(140, 22, self.frame.size.width-140, 24)];
        _objectStateDiscribe = [[UILabel alloc]initWithFrame:CGRectMake(140, 36, self.frame.size.width-140, 24)];

        [_objectStateDiscribe setText:@"正在进行的发起的项目"];
//        [_objectStateDiscribe setFont:[UIFont systemFontOfSize:16]];
//        _objectStateDiscribe.backgroundColor = [UIColor redColor];
        _objectStateDiscribe.font = PingFangSC_Regular(16);
    }
    return _objectStateDiscribe;
}

//是否是发起人
- (UILabel *)objectState {
    if (!_objectState) {
//        _objectState = [[UILabel alloc] initWithFrame:CGRectMake(148, 58, self.frame.size.width-140, 12)];
        
//        CGRectMake(140, 22, self.frame.size.width-140, 24)
        _objectState = [[UILabel alloc] initWithFrame:CGRectMake(140, 12, self.frame.size.width-140, 12)];
        
        
        [_objectState setText:@"进行中"];
        _objectState.textColor = RGB(170, 170, 170);
        _objectState.font = PingFangSC_Regular(12);
//        [_objectState setFont:[UIFont systemFontOfSize:10]];
        
    }
    return _objectState;
}

//加入按钮
//- (UIButton *)objectJoin {
//    if (!_objectJoin) {
//        _objectJoin = [[UIButton alloc]initWithFrame:CGRectMake(140, 82, 72, 36)];
//        [_objectJoin setBackgroundColor:objectJoin_group_bgColor];
//        _objectJoin.layer.cornerRadius = 6;
//        _objectJoin.titleLabel.font = PingFangSC_Regular(12);
//        
//        
//        //        [_objectJoin addTarget:self action:@selector(joinButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//    }
//    return _objectJoin;
//}

- (UIButton *)isFollow {
    if (!_isFollow) {
        _isFollow = [[UIButton alloc]init];

        //212*48
        _isFollow.frame = CGRectMake(140, self.objectStateDiscribe.frame.origin.y + self.objectStateDiscribe.frame.size.height + 12, 106, 24);
        [_isFollow setFont:PingFangSC_Light(12)];

        if (Main_Screen_Height == 667.0) {//384*90
            _isFollow.frame = CGRectMake(140, self.objectStateDiscribe.frame.origin.y + self.objectStateDiscribe.frame.size.height + 12, 192, 45);
            [_isFollow setFont:PingFangSC_Light(14.7)];

            NSLog(@"%zi",Main_Screen_Height);
        }

//        [_isFollow setImage:[UIImage imageNamed:@"你关注了此项目"] forState:UIControlStateNormal];
        [_isFollow setBackgroundImage:[UIImage imageNamed:@"你关注了此项目"] forState:UIControlStateNormal];
        [_isFollow setTitle:@"你关注了此项目" forState:UIControlStateNormal];
        
        
        [_isFollow setTitleColor:RGB(238, 238, 238) forState:UIControlStateNormal];

//        [_isFollow setBackgroundColor:[UIColor redColor]];
    }
    return _isFollow;
}

- (UILabel *)isSuccess {
    
//    __IPHONE_OS_VERSION_MAX_ALLOWED
    if (!_isSuccess) {
        _isSuccess = [[UILabel alloc]init];
        _isSuccess.frame = CGRectMake(140, self.objectStateDiscribe.frame.origin.y + self.objectStateDiscribe.frame.size.height+12,Main_Screen_Width - 140 , 12);
//        [_isSuccess setBackgroundColor:[UIColor blueColor]];
        _isSuccess.textColor = RGB(170, 170, 170);
        _isSuccess.font = PingFangSC_Regular(12);
        _isSuccess.text = @"已成功!";
    }
    return _isSuccess;
    
    
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //        NSLog(@"UserObjectTableViewCell.frame:%@",NSStringFromCGRect(self.frame));
        [self addSubview:self.objectImage];
        [self addSubview:self.objectStateDiscribe];
        
        [self addSubview:self.isSuccess];

        
        [self addSubview:self.objectState];
        [self addSubview:self.isFollow];
        
//        [self addSubview:self.objectJoin];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
//        IS_IPHONE_6P NSLog(@"6+++++");
        
        
//        NSInteger * width = Main_Screen_Width;
//        NSLog(@"%f",Main_Screen_Height);

//        if (Main_Screen_Height == 667.0) {
//            NSLog(@"3倍图");
//            NSLog(@"%zi",Main_Screen_Height);
//        }
        
        
    }
    return self;
}


//对不同状态下项目进行判断
- (void)objectStateStringWithModel:(HuoBanUserProjectData *)model {
    
    
    NSLog(@"%@",[model toDictionary]);
    //注：type : create(创建的项目),order(加入的项目),focus(关注的项目),end(结束的项目)
    
    if ([model.type isEqualToString:@"end"]) {      //判断是否是已经结束
        self.objectState.text = @"成功！";
        [self.objectJoin setTitle:@"去群组" forState:UIControlStateNormal];
        if (model.factMoney < model.wantedMoney) {        //判断是否是失败项目
            self.objectState.text = @"失败";
        }
        
    } else {                                                //正在进行的项目
        //        model
        if ([model.type isEqualToString:@"create"]) {
            self.objectState.text = @"进行中";
            [self.objectJoin setTitle:@"去群组" forState:UIControlStateNormal];
            
        }
        if ([model.type isEqualToString:@"focus"]) {
//            self.objectState.text = @"已关注";
//            [self.objectJoin setTitle:@"加入他们" forState:UIControlStateNormal];
            
            
        }
        if ([model.type isEqualToString:@"order"]) {
//            self.objectState.text = [NSString stringWithFormat:@"已支持%zi元",model.money];
//            [self.objectJoin setTitle:@"支持更多" forState:UIControlStateNormal];
            
            [self.isFollow setTitle:@"你也加入了此项目" forState:UIControlStateNormal];
            [self.isFollow setBackgroundImage:[UIImage imageNamed:@"你也加入了此项目"] forState:UIControlStateNormal];
            //236*48
            self.isFollow.frame = CGRectMake(140, self.objectStateDiscribe.frame.origin.y + self.objectStateDiscribe.frame.size.height + 12, 118, 24);
            //429*93
            if (Main_Screen_Height == 667.0) {
                self.isFollow.frame = CGRectMake(140, self.objectStateDiscribe.frame.origin.y + self.objectStateDiscribe.frame.size.height + 12, 214.5, 46.5);
            }
            

            
        }
        
    }
    
    
    
    //    if ([model.type isEqualToString:@"end"]) {
    //        //项目结束处理
    //        if (model.factMoney >= model.wantedMoney) {
    //            NSLog(@"成功项目");
    //            self.objectState.text = @"成功!";
    //            [self.objectJoin setTitle:@"去群组" forState:UIControlStateNormal];
    //
    //        }
    //        else {
    //            NSLog(@"失败项目");
    //            self.objectState.text = @"已结束";
    //            [self.objectJoin setTitle:@"去群组" forState:UIControlStateNormal];
    //        }
    //    }
    //    else if ([model.type isEqualToString:@"create"]) {
    //        //创建的项目
    //        self.objectState.text = @"进行中";
    //    }
    //    else if ([model.type isEqualToString:@"focus"]) {
    //        //关注的项目
    //        self.objectState.text = @"已关注";
    //        [self.objectJoin setTitle:@"加入他们" forState:UIControlStateNormal];
    //    }
    //    else if ([model.type isEqualToString:@"order"]) {
    //        //加入的项目
    //        self.objectState.text = [NSString stringWithFormat:@"已支持%zi元",model.money];
    //        [self.objectJoin setTitle:@"支持更多" forState:UIControlStateNormal];
    //    }
    
}

- (void) compareNowWithDate:(NSDate *)date  {
    NSDate * _now = [[NSDate alloc]init];
    [_now compare:date];
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
