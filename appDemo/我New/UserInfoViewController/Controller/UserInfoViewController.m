//
//  UserInfoViewController.m
//  huoban
//
//  Created by 马锦航 on 16/1/11.
//  Copyright © 2016年 lyc. All rights reserved.
//

#import "UserInfoViewController.h"
#import "UserInfoHeader.h"
#import "CDPMonitorKeyboard.h"
#import "UserInfoEditTableViewCell.h"
#import "HttpClassSelf.h"
#import "ToolClass.h"
#import "AddressView.h"
#import "EditPayForAddAndNameViewController.h"

// MainScreen Height&Width
#define Main_Screen_Height      [[UIScreen mainScreen] bounds].size.height
#define Main_Screen_Width       [[UIScreen mainScreen] bounds].size.width

//输入框最小高度
#define kMinHeightTextView          (34)

//输入框最大高度
#define kMaxHeightTextView   (84)

//默认输入框和父控件底部间隔
#define kDefaultBottomTextView_SupView  (5)

#define kDefaultTopTextView_SupView  (5)


//字体宏
#define PingFangSC(s)     [UIFont fontWithName:@"PingFangSC-Regular" size:s]
//字体宏
#define PingFangSC_Light(s)     [UIFont fontWithName:@"PingFangSC-Light" size:s]

#define RGB(r,g,b) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.f alpha:1.0]


#define Navigation_TextColor [UIColor colorWithRed:170/255.0f green:170/255.0f blue:170/255.f alpha:1.0]

//238,238,238  #EEEEEE
#define BorderColor [UIColor colorWithRed:238/255.0f green:238/255.0f blue:238/255.f alpha:1.0]

//170,170,170  #AAAAAA  改成D8D8D8   216,216, 216
#define UserInfoItemPlaceColor [UIColor colorWithRed:216/255.0f green:216/255.0f blue:216/255.f alpha:1.0]

//#212121  33,33,33
#define UserInfoItemFontColor [UIColor colorWithRed:33/255.0f green:33/255.0f blue:33/255.f alpha:1.0]


//#4FC2B1  79,194,177
#define ReciveButtonBgColor [UIColor colorWithRed:79/255.0f green:194/255.0f blue:177/255.f alpha:1.0]


#if 0
@interface UserInfoCell : UITableViewCell

@property (nonatomic,copy) NSString * textPlaceTitle;

@end

@interface UserInfoCell ()<UITextFieldDelegate>

@property (nonatomic,strong) UITextField * userInfo;

@property (nonatomic,strong) UIView * underLine;

@end

@implementation UserInfoCell



@end

#endif

//CGFloat userInfoItemWidth = Main_Screen_Width - 24;
CGFloat userInfoItemHeight = 61;
CGFloat userSexPickerHeight = 220;


@interface UserInfoViewController ()<UITableViewDataSource,UITableViewDelegate,UserHeaderViewClickDelegate,UITextFieldDelegate,UITextViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIActionSheetDelegate>

{
    
    CGSize huoboanKeyBoardSize;
}
//头部View
@property (nonatomic,strong) UserInfoHeader * userHeaderView;

//用户基本信息TableView
@property (nonatomic,strong) UITableView * userInfoTableView;

//用户底部View
@property (nonatomic,strong) UIView * userFootView;

//底部编辑收货信息
@property (nonatomic,strong) UIButton * editReciveAdress;

//保存用户信息类型的数组
@property (nonatomic,copy) NSArray * userInfoTypeArray;

//保存用户信息的数组
@property (nonatomic,copy) NSMutableArray * userInfoArray;


@property (nonatomic,copy) NSMutableArray * userInfoTextFieldArray;

@property (nonatomic,strong) UIView * userInfoView;                    //用户基本信息列表

@property (nonatomic,strong) UIActionSheet * sexAcitonSheet;            //用户选择性别的sheet

@property (nonatomic,strong) UITextField * userName;
@property (nonatomic,strong) UITextField * userSex;
@property (nonatomic,strong) UITextField * userProfession;
@property (nonatomic,strong) UITextField * userAdress;
//@property (nonatomic,strong) UIButton * userAdress;

@property (nonatomic,strong) UITextView * userDescribe;



@property (nonatomic,strong) UIPickerView * userSexPicker;              //用户选择性别的picker

@property (nonatomic,strong) UIPickerView * userAdressPicker;           //用户选择地址的picker

@property (nonatomic, strong) AddressView *addressView;                 //用户选择地址的picker

@property (nonatomic, strong) UIToolbar *addressViewToolBar;            //用户选择地址picker上侧的toolBar


@end



@implementation UserInfoViewController

#pragma mark 懒加载


- (void)setUserBaseModel:(huobanUserBaseInfoModel *)userBaseModel {
    _userBaseModel = userBaseModel;
    self.userHeaderView.userHeaderUrl = userBaseModel.data.image;
}

- (NSMutableArray *)userInfoTextFieldArray {
    if (!_userInfoTextFieldArray) {
        _userInfoTextFieldArray = [[NSMutableArray alloc]init];
    }
    return _userInfoTextFieldArray;
}

- (UserInfoHeader *)userHeaderView {
    if (!_userHeaderView) {
        _userHeaderView = [[UserInfoHeader alloc]init];
        [_userHeaderView setFrame:CGRectMake(0, 0, Main_Screen_Width, 90)];
        [_userHeaderView setBackgroundColor:[UIColor whiteColor]];
        _userHeaderView.delegate = self;
    }
    return _userHeaderView;
}

- (UITableView *)userInfoTableView {
    if (!_userInfoTableView) {
        
        //Main_Screen_Height- 258 - self.userDescribe.frame.size.height
        _userInfoTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, self.userFootView.frame.origin.y) style:UITableViewStyleGrouped];
        
        _userInfoTableView.delegate = self;
        _userInfoTableView.dataSource = self;
        _userInfoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_userInfoTableView setBackgroundColor:[UIColor whiteColor]];
        _userInfoTableView.bounces = NO;
//        _userInfoTableView.contentInset = UIEdgeInsetsMake(0, 0, -28, 0);

        
//        _userInfoTableView.tableFooterView = [UIView new];
//        _userInfoTableView.tableHeaderView = [UIView new];
//        _userInfoTableView.contentInset = UIEdgeInsetsMake(-35, 0, 0, 0);
    }
    return _userInfoTableView;
}

- (UIView *)userInfoView {
    if (!_userInfoView) {
        _userInfoView = [UIView new];
        _userInfoView.frame = CGRectMake(12, 0, Main_Screen_Width - 12*2, self.userDescribe.frame.origin.y+self.userDescribe.frame.size.height + 1);
        [_userInfoView setBackgroundColor:BorderColor];
        [_userInfoView addSubview:self.userName];
        [_userInfoView addSubview:self.userSex];
        [_userInfoView addSubview:self.userProfession];
        [_userInfoView addSubview:self.userAdress];
        [_userInfoView addSubview:self.userDescribe];
    }
    return _userInfoView;
}

- (UITextField *)userName {
    if (!_userName) {
        _userName = [UITextField new];
        _userName.frame = CGRectMake(0, 0, Main_Screen_Width - 24, userInfoItemHeight - 1);
        _userName.delegate = self;
        _userName.backgroundColor = [UIColor whiteColor];
        _userName.textColor = UserInfoItemFontColor;
        _userName.attributedPlaceholder = [self attributedPlaceholderWithString:@"姓名" font:PingFangSC_Light(18) color:UserInfoItemPlaceColor];
        _userName.font = PingFangSC(18);
        
#warning 不显示键盘
//        _userName.userInteractionEnabled = NO;
        
    }
    return _userName;
}

- (UITextField *)userSex {
    if (!_userSex) {
        _userSex = [UITextField new];
        _userSex.frame = CGRectMake(0, self.userName.frame.origin.y+userInfoItemHeight, Main_Screen_Width - 24, userInfoItemHeight-1);
//        _userSex.delegate = self;
        _userSex.backgroundColor = [UIColor whiteColor];
        _userSex.delegate = self;
//        [_userSex setTitle:@"性别" forState:UIControlStateNormal];
//        _userSex.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
//        _userSex.titleLabel.textAlignment = NSTextAlignmentLeft;
//        _userSex.contentHorizontalAlignment = 1;
        
//        [_userSex setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
//        _userSex.titleLabel.attributedText = [self attributedPlaceholderWithString:@"性别" font:PingFangSC_Light(18) color:UserInfoItemPlaceColor];
        [_userSex addTarget:self action:@selector(userSexButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        _userSex.font = PingFangSC(18);
        _userSex.placeholder = @"性别";
        
        _userSex.textColor = UserInfoItemFontColor;
    }
    return _userSex;
}

- (UITextField *)userProfession {
    if (!_userProfession) {
        _userProfession = [UITextField new];
        _userProfession.frame = CGRectMake(0, self.userSex.frame.origin.y+userInfoItemHeight, Main_Screen_Width - 24, userInfoItemHeight -1 );
        _userProfession.delegate = self;
        _userProfession.backgroundColor = [UIColor whiteColor];
        _userProfession.attributedPlaceholder = [self attributedPlaceholderWithString:@"职业" font:PingFangSC_Light(18) color:UserInfoItemPlaceColor];
        _userProfession.returnKeyType = UIReturnKeyDone;
        _userProfession.font = PingFangSC(18);
        _userProfession.textColor = UserInfoItemFontColor;
    }
    return _userProfession;
}

//- (UIButton *)userAdress {
//    if (!_userAdress) {
//        _userAdress = [UIButton new];
//        _userAdress.frame = CGRectMake(0, self.userProfession.frame.origin.y+userInfoItemHeight, Main_Screen_Width - 24, userInfoItemHeight -1);
////        _userAdress.delegate = self;
//        _userAdress.backgroundColor = [UIColor whiteColor];
//        [_userAdress setTitle:@"所在地" forState:UIControlStateNormal];
//        [_userAdress setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
//        [_userAdress addTarget:self action:@selector(userAddressButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//        _userAdress.contentHorizontalAlignment = 1;
////    _userAdress.placeholder = @"所在地";
//    }
//    return _userAdress;
//}


- (UITextField *)userAdress {
    if (!_userAdress) {
        _userAdress = [UITextField new];
        _userAdress.frame = CGRectMake(0, self.userProfession.frame.origin.y+userInfoItemHeight, Main_Screen_Width - 24, userInfoItemHeight -1);
        //        _userAdress.delegate = self;
        _userAdress.backgroundColor = [UIColor whiteColor];
        
        _userAdress.attributedPlaceholder = [self attributedPlaceholderWithString:@"所在地" font:PingFangSC_Light(18) color:UserInfoItemPlaceColor];
        _userAdress.font = PingFangSC(18);
        _userAdress.textColor = UserInfoItemFontColor;
    }
    return _userAdress;
}


- (UITextView *)userDescribe {
    if (!_userDescribe) {
        _userDescribe = [UITextView new];
        _userDescribe.frame = CGRectMake(0, self.userAdress.frame.origin.y + userInfoItemHeight, Main_Screen_Width - 24, 70);
        _userDescribe.delegate = self;
        _userDescribe.attributedText = [self attributedPlaceholderWithString:@"简介" font:PingFangSC_Light(18) color:UserInfoItemPlaceColor];
        _userDescribe.returnKeyType = UIReturnKeyDone;
        
    }
    return _userDescribe;
}


//底部向上30px

- (UIView *)userFootView {
    
    if (!_userFootView) {
        _userFootView = [UIView new];
        CGFloat _footViewHeight = 114;
        _userFootView.frame = CGRectMake(0, Main_Screen_Height -_footViewHeight- 44, Main_Screen_Width, _footViewHeight);
        _userFootView.backgroundColor = [UIColor whiteColor];
        [_userFootView addSubview:self.editReciveAdress];
    }
    return _userFootView;
}

- (UIButton *)editReciveAdress {
    if (!_editReciveAdress) {
        _editReciveAdress = [UIButton new];
        _editReciveAdress.frame = CGRectMake(12, 24, Main_Screen_Width - 12*2, 60);
        _editReciveAdress.backgroundColor = ReciveButtonBgColor;
        [_editReciveAdress setTitle:@"编辑收货信息" forState:0];
        _editReciveAdress.titleLabel.font = PingFangSC(14.7);
        [_editReciveAdress setTitleColor:BorderColor forState:UIControlStateNormal];
        [_editReciveAdress addTarget:self action:@selector(editeReciveInfo) forControlEvents:UIControlEventTouchUpInside];
        _editReciveAdress.layer.cornerRadius = 6;
        _editReciveAdress.layer.masksToBounds = YES;
        
//        _editReciveAdress.titleLabel.text = @"编辑收货信息";
    }
    return _editReciveAdress;
}

- (NSArray *)userInfoTypeArray {
    if (!_userInfoTypeArray) {
        _userInfoTypeArray = @[@"姓名",@"性别",@"职业",@"所在地",@"简介"];
    }
    return _userInfoTypeArray;
}

- (NSMutableArray *)userInfoArray {
    if (!_userInfoArray) {
        _userInfoArray = [[NSMutableArray alloc]init];
    }
    return _userInfoArray;
}


- (UIPickerView *)userSexPicker {
    if (!_userSexPicker) {
        _userSexPicker = [[UIPickerView alloc]init];
        _userSexPicker.frame = CGRectMake(0, Main_Screen_Height + userSexPickerHeight, Main_Screen_Width, userSexPickerHeight);
        _userSexPicker.dataSource = self;
        _userSexPicker.delegate = self;
//        _userSexPicker.backgroundColor = [UIColor grayColor];
    }
    return _userSexPicker;
}


- (UIActionSheet *)sexAcitonSheet {
    if (!_sexAcitonSheet) {
        _sexAcitonSheet = [[UIActionSheet alloc] initWithTitle:@"请选择性别" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"男",@"女", nil];
        _sexAcitonSheet.frame = CGRectMake(0, Main_Screen_Height - 300, Main_Screen_Width, 300);
        _sexAcitonSheet.delegate = self;

    }
    return _sexAcitonSheet;
}

- (UIToolbar *)addressViewToolBar {
    if (!_addressViewToolBar) {
        _addressViewToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
        _addressViewToolBar.barTintColor=[UIColor lightGrayColor];

        
        UIBarButtonItem *item1=[[UIBarButtonItem alloc]initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(addressAlreadySelected)];

        UIBarButtonItem *item2=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        UIBarButtonItem *item3=[[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(addressAlreadySelected)];
        _addressViewToolBar.items = @[item1,item2, item3];

    }
    return _addressViewToolBar;
}

- (void)viewWillAppear:(BOOL)animated {
    animated = YES;
    [self hideTabBar];
    [self setNavigationController];
    
    
    self.addressView = [[AddressView alloc] init];
    self.userAdress.inputView = self.addressView;
    self.userAdress.inputAccessoryView = self.addressViewToolBar;

    
    [self.view addSubview:self.userInfoTableView];
    [self.view addSubview:self.userHeaderView];
    [self.view addSubview:self.userFootView];
    
    [self.view addSubview:self.userSexPicker];
    
    
//    NSLog(@"%@",[self.userBaseModel toDictionary]);
    self.userName.text = self.userBaseModel.data.name;
    self.userAdress.text = self.userBaseModel.data.city;
    self.userProfession.text = self.userBaseModel.data.profession;
    NSLog(@"个人信息%@",[self.userBaseModel.data toDictionary]);
    self.userDescribe.attributedText = [self attributedPlaceholderWithString:self.userBaseModel.data.desc font:PingFangSC(18) color:UserInfoItemFontColor];

    self.userSex.text = self.userBaseModel.data.sex? @"男":@"女";
//    self.edgesForExtendedLayout = UIEdgeInsetsMake(0, 0, -20, 0);
    
//    self.edgesForExtendedLayout = UIRectEdgeNone;

}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    //增加监听，当键退出时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    
    UITapGestureRecognizer*tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Actiondo:)];
    [self.userHeaderView addGestureRecognizer:tapGesture];
    

    
    // Do any additional setup after loading the view.
    
    
}



#pragma mark - tableViewDelegate/DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.userInfoTypeArray.count;
    
    return 0;
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 61;
}
//
//- (CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 61;
//}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * reuserIdentifier = @"userInfoViewCell";
    UserInfoEditTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:reuserIdentifier];
    
    if (!cell) {
        cell = [[UserInfoEditTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuserIdentifier];
    }
    
    cell.textPlaceTitle = self.userInfoTypeArray[indexPath.row];
    
    [self.userInfoTextFieldArray addObject:cell.userInfo];
    
//    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;

    return cell;
//    return self.userInfoView;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    tableView.tableFooterView
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    return self.userHeaderView;
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView * view = [UIView new];
    view.frame = CGRectMake(0, 0, Main_Screen_Width, self.userInfoView.frame.size.height);
    [view addSubview:self.userInfoView];
    return view;
//    return self.userInfoView;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return self.userHeaderView.frame.size.height;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
//    return 200;
    return self.userInfoView.frame.size.height;
}



#pragma mark 对navigation中包括左右按钮title显示的设置
- (void) setNavigationController {
    //导航栏背景
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:1]];
    
    //修改导航栏偏色
    self.navigationController.navigationBar.translucent = NO;
    
    //图片保持原有颜色不改变，不做处理的话，图片会被强制渲染成蓝色
    
    //    UIBarButtonItem * leftBarButton = [UIBarButtonItem alloc]initWithImage:<#(nullable UIImage *)#> style:<#(UIBarButtonItemStyle)#> target:<#(nullable id)#> action:<#(nullable SEL)#>
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"系统设置"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonAction)];
    
    
    
//    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:1]];
    
//    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:19],NSForegroundColorAttributeName :[UIColor colorWithRed:245.0/255 green:230.0/255 blue:0.0/255 alpha:1]} forState:UIControlStateNormal];
    
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName : [UIColor colorWithRed:245.0/255 green:230.0/255 blue:0.0/255 alpha:1]}];
    
//    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:245.0/255 green:230.0/255 blue:0.0/255 alpha:1];
    
    self.navigationController.navigationBar.tintColor = RGB(249, 237, 49);

    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStyleBordered target:self action:@selector(rightBarButtonAction)];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"我_back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(leftBarButtonAction)];
    
    self.navigationItem.title = @"个人信息";
    //    self.tabBarItem.title = @"我";
    //设置navigationItem.title的颜色  249,237,49
    //顶栏「个人信息」应为AAAAAA，14.7，light；「保存」应为14.7。
    
//    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:RGB(170, 170, 170) forKey:UITextAttributeTextColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:PingFangSC_Light(14.7),NSForegroundColorAttributeName:RGB(170, 170, 170)}];
    
    
//    [self attributedPlaceholderWithString:<#(NSString *)#> font:<#(UIFont *)#> color:<#(UIColor *)#>];
//    self attributedPlaceholderWithString:<#(NSString *)#> font:<#(UIFont *)#> color:<#(UIColor *)#>
    
    
}



- (void) leftBarButtonAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) rightBarButtonAction {

//    UpdateUserSettingSetUser
    
    NSLog(@"right");
    
    //名字、职业输入最多8个字符。简介最多80个字符。
    if (self.userName.text.length > 8) {
        [[ToolClass sharedInstance] showAlert:@"用户名不得超过8个字符"];
        return;
    }
    if (self.userProfession.text.length > 8) {
        [[ToolClass sharedInstance] showAlert:@"职业不得超过8个字符"];
        return;

    }
    if (self.userDescribe.text.length > 80) {
        [[ToolClass sharedInstance] showAlert:@"简介不得超过80个字符"];
//        self.userDescribe.text = [NSString stringWithFormat:@""];
        return;
    }
    
    
    [[HttpClassSelf new] UpdateUserSettingSetUser:self.userBaseModel.token
                                         setToken:self.userBaseModel.token
                                         setImage:@""
                                          setName:self.userName.text
                                           setSex:self.userSex.text
                                       setAddress:@""
                                          setSign:@""
                                          setDesc:self.userDescribe.text
                                          payName:@""
                                        payMobile:@""
                                      payPostCode:@""
                                       profession:self.userProfession.text
                                             city:self.userAdress.text
CallBackYES:^(MKNetworkOperation *operatioin) {
    
    
//    self.userProfession
//    UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"保存成功" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
//    [alertView show];
    [[ToolClass sharedInstance] showAlert:@"保存成功!"];
    NSLog(@"保存成功");
    ;
} CallBackNO:^(MKNetworkOperation *errorOp, NSError *err) {
    NSLog(@"%@",err.localizedDescription);
}];
    
}

- (void)hideTabBar {
    if (self.tabBarController.tabBar.hidden == YES) {
        return;
    }
    self.tabBarController.tabBar.hidden = YES;
}

- (void)showTabBar {
    if (self.tabBarController.tabBar.hidden == NO)
    {
        return;
    }
    self.tabBarController.tabBar.hidden = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark 键盘监听方法设置
//当键盘出现时调用
-(void)keyboardWillShow:(NSNotification *)aNotification{
    //第一个参数写输入view的父view即可，第二个写监听获得的notification，第三个写希望高于键盘的高度(只在被键盘遮挡时才启用,如控件未被遮挡,则无变化)
    //如果想不通输入view获得不同高度，可自己在此方法里分别判断区别
    [[CDPMonitorKeyboard defaultMonitorKeyboard] keyboardWillShowWithSuperView:self.view andNotification:aNotification higherThanKeyboard:0];
    
    //获取键盘高度
    NSDictionary *info = [aNotification userInfo];
    NSValue *value = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
    huoboanKeyBoardSize= [value CGRectValue].size;
    
    
    //    NSLog(@"keyboardCGSize:%@",NSStringFromCGSize(keyboardSize));
    
}
//当键退出时调用
-(void)keyboardWillHide:(NSNotification *)aNotification{
    [[CDPMonitorKeyboard defaultMonitorKeyboard] keyboardWillHide];
    
    
    
    NSLog(@"keyboardWillHide");
}



- (void) Actiondo:(id)sender {
    NSLog(@"headerViewClick");
    
    if (sender == self.userSex) {
        NSLog(@"userSex");
        return;
    }
    
    [self.view endEditing:YES];
    [self hideSexPicker];
    
}






- (void) userSexButtonAction:(UIButton *)sender {
 
    NSLog(@"userSexButtonAction");
    
    [self sexPickerIsShow]?[self hideSexPicker]:[self showSexPicker];
    
}

- (void) userAddressButtonAction:(UIButton *)sender {
    
}

#pragma mark 取消键盘
- (void) resignTextFieldFirstResponder {
    
    NSLog(@"resignTextFieldFirstResponder");
    //    [self.huobanPassWorld resignFirstResponder];
    //    [self.huobanPhoneNum resignFirstResponder];
    
}


#pragma  mark pickerViewDelegate/Datasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 2;
}

- (NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
//    self.userSex.titleLabel.attributedText = [self attributedPlaceholderWithString:@"" font:PingFangSC(18) color:UserInfoItemFontColor];
    
    [self.userSex setFont:PingFangSC(18)];
//    [self.userSex setTitleColor:UserInfoItemFontColor forState:UIControlStateNormal];
    return row == 1? @"男":@"女";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (row == 1) {
//        [self.userSex setTitle:@"男" forState:UIControlStateNormal];
    }
    else {
//        [self.userSex setTitle:@"女" forState:UIControlStateNormal];
    }
}

//- pickerview

- (void)headerViewClick {
    NSLog(@"headerViewClick");
}


- (void) hideSexPicker {
    self.userSexPicker.frame = CGRectMake(0, Main_Screen_Height + userSexPickerHeight, Main_Screen_Width, userSexPickerHeight);
}

- (void) showSexPicker {
    self.userSexPicker.frame = CGRectMake(0, Main_Screen_Height - userSexPickerHeight - self.userFootView.frame.size.height, Main_Screen_Width, userSexPickerHeight);
}

- (BOOL) sexPickerIsShow {
    CGRect _pickerFrame = self.userSexPicker.frame;
    if (_pickerFrame.origin.y == Main_Screen_Height - userSexPickerHeight - self.userFootView.frame.size.height) {
        return YES;
    }
    return NO;
}


- (NSAttributedString *) attributedPlaceholderWithString:(NSString *)str font:(UIFont*)font color:(UIColor*)color  {
    
    NSMutableAttributedString * placeholderAttributedString = [[NSMutableAttributedString alloc]initWithString:str];
    [placeholderAttributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, placeholderAttributedString.string.length)];
    [placeholderAttributedString addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, placeholderAttributedString.string.length)];
    
    return placeholderAttributedString;
}


#pragma mark - textViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
//    textView.
    if ([textView.text isEqualToString:@"简介"]) {
        self.userDescribe.attributedText = [self attributedPlaceholderWithString:@" " font:PingFangSC(18) color:UserInfoItemFontColor];
    }
//    NSDictionary *info = [aNotification userInfo];
//    NSValue *value = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
//    CGSize keyboardSize = [value CGRectValue].size;
    NSLog(@"%@",NSStringFromCGSize(huoboanKeyBoardSize));
    
    [self addUserInfoTableViewWithKeyBoardSize:huoboanKeyBoardSize View:self.userDescribe];
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    // 在将要开始编辑的时候，判断是否是userSex文本输入框，如果是，就显示选择性别的sexActionSheet,然后返回NO，表示不进行编辑，就不会弹出键盘
    if (textField == self.userSex) {
        [self.view endEditing:YES];
        [self.sexAcitonSheet showInView:self.view];
        return NO;
    }
    
    return YES;
}


//根据输入文字多少，自动调整输入框的高度
-(void)textViewDidChange:(UITextView *)textView
{
    
    NSLog(@"textViewDidChange");
    //计算输入框最小高度
    CGSize size =  [textView sizeThatFits:CGSizeMake(textView.contentSize.width, 0)];
    
    CGFloat contentHeight;
    
    //输入框的高度不能超过最大高度
    if (size.height > kMaxHeightTextView)
    {
        contentHeight = kMaxHeightTextView;
        textView.scrollEnabled = YES;
    }else
    {
        contentHeight = size.height;
        textView.scrollEnabled = NO;
    }
//    if (self.userDescribe != contentHeight)
//    {//如果当前高度需要调整，就调整，避免多做无用功
//        self.userDescribe = contentHeight ;//重新设置自己的高度
//        [self invalidateIntrinsicContentSize];
//    }
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"]) {//按下return键
        //这里隐藏键盘，不做任何处理
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.userProfession) {
        [textField endEditing:YES];
    }
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    
    self.userInfoTableView.frame = CGRectMake(0, 0, Main_Screen_Width, self.userFootView.frame.origin.y);

    return YES;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
        {
            NSLog(@"男");
            self.userSex.text = @"男";
        }
            break;
        case 1:
        {
            NSLog(@"女");
            self.userSex.text = @"女";
        }
            break;
        case 2:
        {
            NSLog(@"取消");
        }
            break;
            
        default:
            break;
    }
}



- (void) addUserInfoTableViewWithKeyBoardSize:(CGSize)keyBoardSize View:(UIView *)view{
    
    NSLog(@"tableveiw.frame%@",NSStringFromCGRect(self.userInfoTableView.frame));
    
    if (self.userInfoTableView.frame.origin.y+self.userInfoTableView.frame.size.height>Main_Screen_Height - keyBoardSize.height - self.userDescribe.frame.size.height) {
        CGRect userInfoViewFrame = self.userInfoTableView.frame;
//        userInfoViewFrame.size.h = userInfoViewFrame.origin.y -
        userInfoViewFrame.origin.y = Main_Screen_Height- keyBoardSize.height - userInfoViewFrame.size.height;
        
        if (self.userInfoTableView.frame.size.height < self.userHeaderView.frame.size.height + self.userInfoView.frame.size.height) {
            userInfoViewFrame.origin.y -= self.userHeaderView.frame.size.height + self.userInfoView.frame.size.height - self.userInfoTableView.frame.size.height - 20;
        }
        
        self.userInfoTableView.frame = userInfoViewFrame;
        
        
    }
    
    
//    CGRect userInfoViewFrame = self.userInfoTableView.frame;
    
//    NSLog(@"userInfoTableView:%@",NSStringFromCGRect(self.userInfoTableView.frame));
//    NSLog(@"%@",NSStringFromCGRect(view.superview.frame));
    
//    userInfoViewFrame.origin.y = self.userInfoTableView.frame.origin.y - 
    
//    self.userInfoTableView.frame = userInfoViewFrame;
    
//    self.userFootView.frame = userInfoViewFrame;

}

#pragma mark 地址picker取消按钮 
- (void) addressCancel {
    [self.userAdress endEditing:YES];
}
#pragma mark 地址picker完成按钮
- (void) addressAlreadySelected {
    
    if (self.userAdress.isFirstResponder) {
        [self.userAdress endEditing:YES];
    }
    self.userAdress.text = [NSString stringWithFormat:@"%@",self.addressView.city];

}


- (void) editeReciveInfo {
//    EditPayForAddAndNameViewController
//    EditPayForAddAndNameViewController
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    EditPayForAddAndNameViewController * EditPayForAddAndNameViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"EditPayForAddAndNameViewController"];
    self.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:EditPayForAddAndNameViewController animated:YES];

    
    
}

//- (NSAttributedString *) attributedPlaceholderWithString:(NSString *)str {
//    
//    NSMutableAttributedString * placeholderAttributedString = [[NSMutableAttributedString alloc]initWithString:str];
//    [placeholderAttributedString addAttribute:NSFontAttributeName value:PingFangSC_Light(18) range:NSMakeRange(0, placeholderAttributedString.string.length)];
//    [placeholderAttributedString addAttribute:NSForegroundColorAttributeName value:UserInfoItemPlaceColor range:NSMakeRange(0, placeholderAttributedString.string.length)];
//    
//    return placeholderAttributedString;
//}


/*s
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepa    [self hideTabBar];
reForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
