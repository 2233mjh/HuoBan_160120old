//
//  RemindTableViewController.m
//  huoban
//
//  Created by Lyc on 15/12/17.
//  Copyright © 2015年 lyc. All rights reserved.
//

#import "RemindTableViewController.h"
#import "RemindTableViewCell.h"
#import "DataModel.h"
#import "UIImageView+WebCache.h"
#import "ProjectCommentTableViewController.h"



@interface RemindTableViewController ()

@end

@implementation RemindTableViewController

NSString *_striOSRemind;
NSMutableArray *_aryRemindMessage;
DataModel *_dataModelRMCell;
NSTimeZone *_zoneRemnind;
UIView *_viewEmptyData;


NSDateFormatter *_dateFormatterRemin;

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    DataModel *dataModelAll = [[DataModel alloc] init];
    
        _dateFormatterRemin = [[NSDateFormatter alloc] init];
        [_dateFormatterRemin setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
         _zoneRemnind = [NSTimeZone systemTimeZone];
    
    //导航栏背景
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:1]];
    
    //导航栏整体
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.7],NSForegroundColorAttributeName : [UIColor colorWithRed:170.0/255 green:170.0/255 blue:170.0/255 alpha:1]}];
    
    self.title = @"消息";
    
    //修改导航栏偏色
    self.navigationController.navigationBar.translucent = NO;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    

    if([_striOSRemind isEqualToString:@"iPhone6Plus"]){
        self.imageClose.image = [UIImage imageNamed:@"back_blue_3x"];
    }else{
        self.imageClose.image = [UIImage imageNamed:@"back_blue_2x"];
    }
//    _aryRemindMessage  = [[NSUserDefaults standardUserDefaults] objectForKey:@"RemindMessage"];

    [self EmptyViewAppear];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    _dataModelRMCell = [[DataModel alloc] init];
    
    _striOSRemind = _dataModelRMCell.userInfomation.iOSDeviceSize;
    
    _aryRemindMessage = [[NSMutableArray alloc] init];
    
    [_aryRemindMessage removeAllObjects];
    
    for(NSMutableDictionary *dicRemind in _dataModelRMCell.userInfomation.aryRemindMessage){
        
        [_aryRemindMessage addObject:dicRemind];
        
    }
    


    NSLog(@"%@----%lu",_aryRemindMessage,[_aryRemindMessage count]);

    
}
-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    if([_aryRemindMessage count] == 0 ){
        _viewEmptyData.hidden = NO;
    }else{
        _viewEmptyData.hidden = YES;
        [self.tableView reloadData];
        
    }
    
    
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
//    return [_dataModelRMCell.aryRemindMessage count];
    return [_aryRemindMessage count];


}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RemindTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"RemindMessage"];
    
    if(cell == nil){
        cell = [[RemindTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RemindMessage"];
    }
    
    NSMutableDictionary *dicData = _aryRemindMessage[indexPath.row];
    
    NSLog(@"%@",dicData);
    cell.labelContents.text = [dicData[@"text"] substringFromIndex:2];
    cell.labelBy.text = dicData[@"name"];

    [cell.imagePreson sd_setImageWithURL:[NSURL URLWithString:dicData[@"image"]] placeholderImage:nil];
    cell.imagePreson.clipsToBounds = YES;
    cell.imagePreson.layer.cornerRadius = 22;
    
    NSLog(@"%@",dicData[@"type"]);
    NSInteger remindType = [dicData[@"type"] integerValue];
    if([dicData[@"type"] integerValue] == 1){
     
        cell.labelBy.textColor = [UIColor colorWithRed:255.0/255 green:51.0/255 blue:51.0/255 alpha:1];
        cell.imagePreson.layer.cornerRadius = 0;
        
    }else if([dicData[@"type"] integerValue] == 2){
        
        cell.labelBy.textColor = [UIColor colorWithRed:51.0/255 green:163.0/255 blue:219.0/255 alpha:1];
        cell.imagePreson.layer.cornerRadius = 0;

    }else if([dicData[@"type"] integerValue] == 3){
        cell.labelBy.textColor = [UIColor colorWithRed:33.0/255 green:33.0/255 blue:33.0/255 alpha:1];
        cell.imagePreson.layer.cornerRadius = 22;
    }else if ([dicData[@"type"] integerValue] == 4){
        cell.labelBy.textColor = [UIColor colorWithRed:33.0/255 green:33.0/255 blue:33.0/255 alpha:1];
        cell.imagePreson.layer.cornerRadius = 22;
    }else if([dicData[@"type"] integerValue] == 5){
        cell.labelBy.textColor = [UIColor colorWithRed:33.0/255 green:33.0/255 blue:33.0/255 alpha:1];
        cell.imagePreson.layer.cornerRadius = 22;
    }
    NSLog(@"%@---%@",[dicData[@"time"] substringToIndex:19],[_dateFormatterRemin dateFromString:[dicData[@"time"] substringToIndex:19]]);
    
    NSDate *dateR = [_dateFormatterRemin dateFromString:[dicData[@"time"] substringToIndex:19]];
    
    NSInteger interval = [_zoneRemnind secondsFromGMTForDate:[_dateFormatterRemin dateFromString:[dicData[@"time"] substringToIndex:19]]];
    
    NSDate *dateRemind = [dateR dateByAddingTimeInterval: interval];
    
    
    
    NSDate *dataNow = [NSDate date];
    
    NSInteger intervalNow = [_zoneRemnind secondsFromGMTForDate:dataNow];
    
    NSDate *dateNow = [dataNow dateByAddingTimeInterval: intervalNow];
    
    if([dateNow timeIntervalSinceDate:dateRemind]/60 < 60){
        
    cell.labelTime.text = [NSString stringWithFormat:@"%@分钟前",[[NSString stringWithFormat:@"%f",[dateNow timeIntervalSinceDate:dateRemind]/60] componentsSeparatedByString:@"."][0] ];

    }else if([dateNow timeIntervalSinceDate:dateRemind]/3600 < 24 && [dateNow timeIntervalSinceDate:dateRemind]/3600 >= 1){
        
        cell.labelTime.text = [NSString stringWithFormat:@"%@小时前",[[NSString stringWithFormat:@"%f小时前",[dateNow timeIntervalSinceDate:dateRemind]/3600] componentsSeparatedByString:@"."][0] ];
        
    }else if([dateNow timeIntervalSinceDate:dateRemind]/86400 < 30 && [dateNow timeIntervalSinceDate:dateRemind]/86400 >= 1){
        cell.labelTime.text =[NSString stringWithFormat:@"%@天前",[[NSString stringWithFormat:@"%f",[dateNow timeIntervalSinceDate:dateRemind]/86400 ] componentsSeparatedByString:@"."][0]];
    }else if([dateNow timeIntervalSinceDate:dateRemind]/2592000 >=1){
        cell.labelTime.text =[NSString stringWithFormat:@"%@个月前",[[NSString stringWithFormat:@"%f",[dateNow timeIntervalSinceDate:dateRemind]/2592000 ] componentsSeparatedByString:@"."][0]];
    }

    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"TalkSegue"]){
    
        UINavigationController *naVC = segue.destinationViewController;
        
        ProjectCommentTableViewController *controller = (ProjectCommentTableViewController*)naVC.topViewController;
        
//        TalkViewController *controller = (TalkViewController*)segue.destinationViewController;
        
//        controller.delegate = self;
        
        controller.strFeedID = sender;
    }

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DataModel *dataSegue = [[DataModel alloc] init];
    
    NSMutableDictionary *dicSegue = dataSegue.userInfomation.aryRemindMessage[indexPath.row];
    NSLog(@"%lu",(unsigned long)[dataSegue.userInfomation.aryRemindMessage count]);
    [dataSegue.userInfomation.aryRemindMessage removeObjectAtIndex:indexPath.row];
    [dataSegue saveUserInfomation];
    
    [self performSegueWithIdentifier:@"TalkSegue" sender:dicSegue[@"id"]];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 68;
}



- (IBAction)buttonCancel:(id)sender {
    
        DataModel *dataSegue = [[DataModel alloc] init];
    
    dataSegue.userInfomation.isRemind = @"0";
    dataSegue.userInfomation.isNewMessage = @"0";
    [dataSegue saveUserInfomation];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)EmptyViewAppear{
    //底层view
    _viewEmptyData = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.tableView.frame.size.width , self.tableView.frame.size.height - 108)];
    
    _viewEmptyData.backgroundColor = [UIColor whiteColor];
    
    UILabel *labelAAA = [[UILabel alloc] initWithFrame:CGRectMake(0,_viewEmptyData.frame.size.height/2 - 7.5,_viewEmptyData.frame.size.width,15)];
    labelAAA.text = @"没有新的提醒。";
    labelAAA.textColor = [UIColor colorWithRed:216.0/255 green:216.0/255 blue:216.0/255 alpha:1];
    labelAAA.font = [UIFont systemFontOfSize:14.7];
    labelAAA.textAlignment = NSTextAlignmentCenter;
    [_viewEmptyData addSubview:labelAAA];
//
//    UILabel *labelBBB = [[UILabel alloc] initWithFrame:CGRectMake(0,_viewEmptyData.frame.size.height/2 + 3,_viewEmptyData.frame.size.width,15)];
//    labelBBB.text = @"支持或关注项目，就可以加入社群。";
//    labelBBB.textColor = [UIColor colorWithRed:216.0/255 green:216.0/255 blue:216.0/255 alpha:1];
//    labelBBB.font = [UIFont systemFontOfSize:14.7];
//    labelBBB.textAlignment = NSTextAlignmentCenter;
//    [_viewEmptyData addSubview:labelBBB];
    
    [self.tableView addSubview:_viewEmptyData];
    
    
    _viewEmptyData.hidden = YES;
    
}


//-(void)commentTableViewDelete:(CommentTableViewController *)controller{
//    
//    [controller dismissViewControllerAnimated:YES completion:nil];
//}

@end
