//
//  ProjectMessageViewController.m
//  appDemo
//
//  Created by 刘雨辰 on 15/8/24.
//  Copyright (c) 2015年 lyc. All rights reserved.

#import "ProjectDetailsViewController.h"
#import "ProjectDynamicTableViewController.h"
#import "HttpClassSelf.h"
#import "ToolClass.h"
#import "ProjectModel.h"
#import "ProjectDynamicSelfTableViewController.h"
#import "ProjectMoreStoryViewController.h"
#import "ProjectDetailsShare.h"
#import "DataModel.h"
#import "JoinProjectTableViewController.h"
#import "ImageSameWidth.h" 
#import "ALMoviePlayerController.h"
#import "MoreDynamicViewController.h"
#import "SuppotPresonViewController.h"
#import "PayForProjectTableViewController.h"
#import "OtherUserInfoViewController.h"
#import "UserSelfMessageViewController.h"


@interface ProjectDetailsViewController ()

@end

NSDate *_DetailsDate;

@implementation ProjectDetailsViewController

ProjectDetailsShare *_shareView;
NSString *_strHTMLContents;
DataModel *_dataModelProjectDyanmic;
NSMutableArray *_aryLoveButtons;
NSMutableArray *_aryChou;
NSInteger  HeightHeight;
bool _seleted;
CGFloat _heightVideo;
CGFloat _widthVideo;
HttpClassSelf *_httpClassProjectDynamic;
NSString *_iOSSizeDetails;
CGFloat _heightTextContents;
NSDateFormatter *_dateFormatterDetails;
NSInteger _imageLineWidth;
NSInteger _viewTimeLineDown;
NSMutableArray *_dicCreateDetail;
NSMutableDictionary *_dicFocusDetail;
NSMutableArray *_aryPayView;
NSInteger _runtimesDetails;
float _floatPercentage;
CGSize _iOSDeviceDetails;
float _floatScorllHeight;
NSInteger _countDownData;
UIView *_viewProjectDetails;
UIView *_viewButtons;
UIImageView *_CreatorImageView;
UIImageView *_FocusImageView;
UILabel *_labelFocus;
UIView *_viewChou;
UILabel *_labelTitleChouView;
UILabel *_labelContentsChouView;
UILabel *_labelSendTimeChouView;
UILabel *_labelPresonCountChouView;
UIButton *_buttonPay;
NSInteger _viewChouYYY;
NSString *_strShareText;
NSString *_strShareTitle;
NSString *_strShareImage;
NSString *_strProjectID;
NSURL *_strVideoURL;
UIView *_viewVideoFull;
UIView *_viewShareShaow;
NSInteger _intShareView;
UIButton *buttonJoinDidProjectDetails;
NSMutableArray *_aryChouViewiSJoined;
bool _isEndProject;
bool _isJoinProject;


bool _isVideoAppear;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addNotification];
    
    _dataModelProjectDyanmic = [[DataModel alloc] init];
    
    _dateFormatterDetails = [[NSDateFormatter alloc] init];
    
    [_dateFormatterDetails setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    _aryPayView = [[NSMutableArray alloc] initWithCapacity:20];
    _aryChouViewiSJoined = [[NSMutableArray alloc] initWithCapacity:20];
    self.scrollView.delegate = self;
    _iOSDeviceDetails = [UIScreen mainScreen].bounds.size;
    _iOSSizeDetails = _dataModelProjectDyanmic.userInfomation.iOSDeviceSize;

    if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
        
        self.imageVideoLeft.constant = 0;
        _widthVideo = 414;
        _heightVideo = 233;
        self.imageVideoHeight.constant = 233;
        self.titleTOP.constant = 250;
        
        _imageLineWidth = 393.5;
        
        self.imageAdd.image = [UIImage imageNamed:@"tag@3x"];
        self.imageType.image = [UIImage imageNamed:@"add@3x"];
        self.imageViewButtonPlay.image = [UIImage imageNamed:@"videoPlay@3x"];

    }else if([_iOSSizeDetails isEqualToString:@"iPhone6"]){
        
        self.imageVideoLeft.constant = 4;
        _widthVideo = 375;
        _heightVideo = 211;
        _imageLineWidth = 362.5;
        
        self.imageVideoHeight.constant = 211;
        self.titleTOP.constant = 250;
        self.imageAdd.image = [UIImage imageNamed:@"tag@2x"];
        self.imageType.image = [UIImage imageNamed:@"add@2x"];
        self.imageViewButtonPlay.image = [UIImage imageNamed:@"videoPlay@2x"];
        

    }else if([_iOSSizeDetails isEqualToString:@"iPhone5"]){
        
        self.imageVideoLeft.constant = 4;
        _widthVideo = 320;
        _heightVideo = 185;
        self.imageVideoHeight.constant = 185;
        _imageLineWidth = 307.5;
        self.titleTOP.constant = 200;
        self.imageAdd.image = [UIImage imageNamed:@"tag@2x"];
        self.imageType.image = [UIImage imageNamed:@"add@2x"];
        self.imageViewButtonPlay.image = [UIImage imageNamed:@"videoPlay@2x"];
        


    }else if([_iOSSizeDetails isEqualToString:@"iPhone4"]){
        
        self.imageVideoLeft.constant = 4;
        _widthVideo = 320;
        _heightVideo = 185;
        _imageLineWidth = 307.5;
        self.imageVideoHeight.constant = 185;
        self.imageAdd.image = [UIImage imageNamed:@"tag@2x"];
        self.imageType.image = [UIImage imageNamed:@"add@2x"];
        self.imageViewButtonPlay.image = [UIImage imageNamed:@"videoPlay@2x"];
        self.titleTOP.constant = 200;
    
    }
    
    self.imageViewWidth.constant = _iOSDeviceDetails.width + 1;

      //大图圆角
    self.buttonMoreStory.layer.masksToBounds = YES;
    self.buttonMoreStory.layer.cornerRadius = 6;
    self.buttonMoreStory.layer.borderColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1].CGColor;

    //大图圆角
    self.buttonMoreDynamic.layer.masksToBounds = YES;
    self.buttonMoreDynamic.layer.cornerRadius = 6;
    self.buttonMoreDynamic.layer.borderColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1].CGColor;

    _aryChou = [[NSMutableArray alloc] init];
    
    [self viewCreatelogin];
    [self viewCreatebuttonJoin];
    [self viewCreateShareView];
    
    
    _httpClassProjectDynamic = [[HttpClassSelf alloc] init];
    
    [_httpClassProjectDynamic projectDetailsSetProjectID:self.strProjectID token:_dataModelProjectDyanmic.userInfomation.tokenID CallBackYES:^(MKNetworkOperation *operatioin){
        
        NSData *data = [operatioin responseData];
        NSMutableDictionary *resHomePageDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        if([resHomePageDic[@"status"] isEqualToString:@"success"]){
            
            NSLog(@"打印项目详情返回对象%@",resHomePageDic);
            
            NSMutableArray *aryVideos = [[NSMutableArray alloc]init];
            
            NSDictionary *dic  =  resHomePageDic[@"data"];
            
            _strProjectID = dic[@"_id"];
            
            for(NSString *strVideo in dic[@"videos"]){
                [aryVideos addObject:strVideo];
                
            }

            [self.imageViewVideo sd_setImageWithURL:[NSURL URLWithString:dic[@"video_image"]] placeholderImage:nil];
            
            NSMutableAttributedString  *strTextContents =[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",dic[@"lastfeed"]]];
            NSMutableParagraphStyle * paragraphStyle=[[NSMutableParagraphStyle alloc]init];
            
            NSLog(@"%@",dic[@"focus"]);
            
            _dicFocusDetail = dic[@"focus"];
            
            [paragraphStyle setMinimumLineHeight:24];
            
            [strTextContents addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [[NSString stringWithFormat:@"%@",dic[@"lastfeed"]] length])];
            
            
            _strVideoURL  = [NSURL URLWithString:aryVideos[0]];
            
            _dicCreateDetail = dic[@"creator"];
            
            _viewTimeLineDown = 40;
            //1200 330
            if([dic[@"images"] count] > 1){
//                self.imagesTimeLines.image  =  [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:dic[@"images"][1]]]];
                
                [self.imagesTimeLines sd_setImageWithURL:[NSURL URLWithString:dic[@"images"][1]] placeholderImage:nil];

                
                //时间线
                self.imagesTimeLines.clipsToBounds = YES;
                self.imagesTimeLines.contentMode = UIViewContentModeScaleAspectFit;
                [self.imagesTimeLines sizeToFit];
                self.imagelineHeight.constant = _imageLineWidth/(1200/330);
            }else{
                self.imagelineHeight.constant = 1;
                _viewTimeLineDown = 18;
            }
            
            for(NSMutableDictionary *dicChou in dic[@"chou"]){
                [_aryChou addObject:dicChou];
            }
            
            NSDateFormatter *_dateFormatterDetails = [[NSDateFormatter alloc] init];
            
            [_dateFormatterDetails setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSDate  * _dateDetals = [NSDate date];
            
            NSTimeZone *zone = [NSTimeZone systemTimeZone];
            
            NSInteger interval = [zone secondsFromGMTForDate:_dateDetals];
            
            NSDate *_dateNowNowNow = [_dateDetals dateByAddingTimeInterval: interval];
            
            NSLog(@"%@",_dateNowNowNow);
            
            NSDate *_dateEnd_Date = [_dateFormatterDetails dateFromString:dic[@"end_date"]];
            
            NSTimeZone *zoneEnd = [NSTimeZone systemTimeZone];
            
            NSInteger intervalEnd = [zoneEnd secondsFromGMTForDate:_dateEnd_Date];
            
            NSDate *_dateEndEndEnd = [_dateEnd_Date dateByAddingTimeInterval: intervalEnd];
            NSLog(@"%@",dic[@"end_date"]);
            NSLog(@"%@",_dateEndEndEnd);
            
            _DetailsDate = _dateEndEndEnd;
            
//            NSLog(@"今天--%@------结束日期%@",_dateNowNowNow,_dateEndEndEnd);
            
            NSLog(@"%lu",[dic[@"isorder"] longValue]);
            
            
            if([dic[@"isorder"] longValue ] == 0){
                //            if(/* DISABLES CODE */ (NO)){
                
                
                self.imageViewJoin.hidden = YES;
                self.imageViewLove.hidden = NO;
                self.buttonLove.enabled = YES;
                
                if([dic[@"isFocus"] longValue] == 0){
                    _seleted = NO;
                    if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
                        //                    [self.buttonLove setImage:[UIImage imageNamed:@"details_UnLove@3x"] forState:UIControlStateNormal];
                        self.imageViewLove.image = [UIImage imageNamed:@"details_UnLove@3x"];
                    }else{
                        //                    [self.buttonLove setImage:[UIImage imageNamed:@"details_UnLove@2x"] forState:UIControlStateNormal];
                        self.imageViewLove.image = [UIImage imageNamed:@"details_UnLove@2x"];
                    }
                }else{
                    _seleted = YES;
                    if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
                        
                        self.imageViewLove.image = [UIImage imageNamed:@"details_Love@3x"];
                        
                    }else{

                        self.imageViewLove.image = [UIImage imageNamed:@"details_Love@3x"];
                        
                    }
                    
                }
                
                [buttonJoinDidProjectDetails setTitle:@"加入他们" forState:UIControlStateNormal];
                _isJoinProject = NO;
                
            }else{
                
                self.imageViewJoin.hidden = NO;
                self.imageViewLove.hidden = YES;
                self.buttonLove.enabled = NO;
                
                if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
                    self.imageViewJoin.image = [UIImage imageNamed:@"ProjectDetailsJoined@3x"];
                }else{
                    self.imageViewJoin.image = [UIImage imageNamed:@"ProjectDetailsJoined@2x"];
                }
                [buttonJoinDidProjectDetails setTitle:@"支持更多" forState:UIControlStateNormal];
                _isJoinProject = YES;
            }

            self.textFactMoney.text = [NSString stringWithFormat:@"￥%ld/%ld",[dic[@"fact_money"] longValue],[dic[@"wanted_money"] longValue]];
            
            self.labelFactMoney.text = [NSString stringWithFormat:@"￥%ld/%ld",[dic[@"fact_money"] longValue],[dic[@"wanted_money"] longValue]];
            
            _floatPercentage = [dic[@"fact_money"] floatValue]/ [dic[@"wanted_money"] floatValue];
            
            //未被覆盖的颜色
            self.progressView.trackTintColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
            
            self.progressView.progressTintColor = [UIColor colorWithRed:255.0/255 green:51.0/255 blue:51.0/255 alpha:1];
            //进度条进度
            self.progressView.progress = _floatPercentage;
            
            self.progressView.backgroundColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
            
            //进度条百分比文字
            
            if(_floatPercentage * 100 < 10 ){
                
                self.textPercentage.text = [NSString stringWithFormat:@"%@%%",[[NSString stringWithFormat:@"%f",round(_floatPercentage * 100)] substringToIndex:1]];
                
                self.labelProgress.text = [NSString stringWithFormat:@"%@%%",[[NSString stringWithFormat:@"%f",round(_floatPercentage * 100)] substringToIndex:1]];
                
            }else if(_floatPercentage * 100 >= 10 && _floatPercentage * 100 < 100){
                
                self.textPercentage.text = [NSString stringWithFormat:@"%@%%",[[NSString stringWithFormat:@"%f",round(_floatPercentage * 100)] substringToIndex:2]];
                
                self.labelProgress.text = [NSString stringWithFormat:@"%@%%",[[NSString stringWithFormat:@"%f",round(_floatPercentage * 100)] substringToIndex:2]];
                
            }else if(_floatPercentage * 100 >= 100){
                
                //百分比文字
                self.textPercentage.text = [NSString stringWithFormat:@"%@%%",[[NSString stringWithFormat:@"%f",round(_floatPercentage * 100)] substringToIndex:3]];
                
                self.labelProgress.text = [NSString stringWithFormat:@"%@%%",[[NSString stringWithFormat:@"%f",round(_floatPercentage * 100)] substringToIndex:3]];

            }
            
            
            if((int)[_dateEndEndEnd timeIntervalSinceDate:_dateNowNowNow]/86400 <= 0 && _floatPercentage * 100 >= 100){

                self.textWantedMoney.text = @"已成功";
                self.viewSuccess.hidden = NO;
                self.progressView.hidden = YES;
                self.imageViewProgressRight.hidden = YES;
                self.labelProgress.hidden = YES;
                self.labelFactMoney.hidden = YES;
                self.textWantedMoney.hidden = YES;
                self.imageRight.hidden = YES;
                buttonJoinDidProjectDetails.hidden = YES;
                _isEndProject = YES;
                
                
                
                self.viewCreatorTopHeight.constant = 30.5;
                
            }else if((int)[_dateEndEndEnd timeIntervalSinceDate:_dateNowNowNow]/86400 <= 0 && _floatPercentage * 100 < 100){
                
                self.textWantedMoney.text = @"已结束";
                self.viewSuccess.hidden = YES;
               self.viewCreatorTopHeight.constant = 24;
                self.imageViewProgressRight.hidden = NO;
                self.progressView.hidden = NO;
                self.labelProgress.hidden = NO;
                self.labelFactMoney.hidden = NO;
                self.textWantedMoney.hidden = NO;
                self.imageRight.hidden = NO;
                buttonJoinDidProjectDetails.hidden = YES;
                _isEndProject = YES;
            }else{
                
                self.textWantedMoney.text = [NSString stringWithFormat:@" 剩余%i天 ",((int)[_dateEndEndEnd timeIntervalSinceDate:_dateNowNowNow]/86400)];
                self.viewSuccess.hidden = YES;
                self.viewCreatorTopHeight.constant = 24;
                self.imageViewProgressRight.hidden = NO;
                self.progressView.hidden = NO;
                self.labelProgress.hidden = NO;
                self.labelFactMoney.hidden = NO;
                self.textWantedMoney.hidden = NO;
                self.imageRight.hidden = NO;
                buttonJoinDidProjectDetails.hidden = NO;
                _isEndProject = NO;
            }

            _countDownData = [_dateEndEndEnd timeIntervalSinceDate:_dateNowNowNow]/86400;
            
            self.textCountry.text = dic[@"address"];
            self.textTitle.text = dic[@"title"];
//            self.textTitle.text = @"刘雨辰啊哈哈哈哈哈哈";
            self.textType.text = dic[@"type"];
            
            _strShareTitle = dic[@"title"];
            _strShareImage = dic[@"images"][0];
            
            
            
            NSMutableAttributedString  *strtextSummarize =[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",dic[@"desc"]]];
            
            [strtextSummarize addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [[NSString stringWithFormat:@"%@",dic[@"desc"]] length])];
            
            self.textSummarize.attributedText = strtextSummarize;
            _strHTMLContents = dic[@"desc_all"];
            
            _strShareText = dic[@"desc"];
            
            [self.textSummarize sizeToFit];
            [self.labelCreator sizeToFit];
        
            //「发起者」头像
            NSInteger creatorImageViewX = self.labelCreator.frame.origin.x + self.labelCreator.frame.size.width + 9;
            
            for(int i = 0;i <[_dicCreateDetail count];i++){
                
                NSMutableDictionary *dicCreatorItems = _dicCreateDetail[i];
                
                _CreatorImageView = [[UIImageView alloc] initWithFrame:CGRectMake( creatorImageViewX,0, 44,44)];
                
                _CreatorImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:dicCreatorItems[@"image"]]]];
                
                _CreatorImageView.clipsToBounds = YES;
                _CreatorImageView.layer.cornerRadius = 22;
                _CreatorImageView.userInteractionEnabled = YES;
                _CreatorImageView.contentMode = UIViewContentModeScaleAspectFill;
                [self.viewCreatorBlock addSubview:_CreatorImageView];
                
                creatorImageViewX += 53;
                
                UITapGestureRecognizer *imageFocusTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buttonCreatorImageClick:)];
                [_CreatorImageView addGestureRecognizer:imageFocusTap];
                _CreatorImageView.tag = i;
            }
            NSInteger endFocusXXX  = 0;
            NSInteger endFocusYYY  = 0;
            
            if([_dicFocusDetail[@"users"] count] < 6 && [_dicFocusDetail[@"users"] count] >0){
            
                //「火伴」头像
                NSInteger imageFocuePresonX = self.labelHuoban.frame.origin.x + self.labelHuoban.frame.size.width + 9;
                
                if([_iOSSizeDetails isEqualToString:@"iPhone6"] ||[_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
                    
                    self.viewFocusHeight.constant = 44;
                    
                    for(int i = 0;i < [_dicFocusDetail[@"users"] count] ;i++){
                        
                        if(i <= 4){
                            
                        NSMutableDictionary *dicFocusItem = _dicFocusDetail[@"users"][i];
                        
                        NSLog(@"%@",dicFocusItem);
                        
                        _FocusImageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageFocuePresonX, self.labelHuoban.frame.origin.y - 14.65, 44,44)];
                        
//                        _FocusImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:dicFocusItem[@"image"]]]];
                            
                            
                            [_FocusImageView sd_setImageWithURL:[NSURL URLWithString:dicFocusItem[@"image"]] placeholderImage:nil];
                        
                        _FocusImageView.clipsToBounds = YES;
                        _FocusImageView.layer.cornerRadius = 22;
                        _FocusImageView.userInteractionEnabled = YES;
                        _FocusImageView.contentMode = UIViewContentModeScaleAspectFill;
                        [self.viewFocusBlock addSubview:_FocusImageView];
                        if(i == 0){
                            imageFocuePresonX = self.labelHuoban.frame.origin.x + self.labelHuoban.frame.size.width + 9;
                        }else{
                            imageFocuePresonX = ( self.labelHuoban.frame.origin.x + self.labelHuoban.frame.size.width + 9) + (i * 53);
                        }
                        endFocusXXX = imageFocuePresonX;
                        endFocusYYY =  self.labelHuoban.frame.origin.y - 14.65;
                        }
                    }
                    
                }else{
                    
                    self.viewFocusHeight.constant = 97;
                    
                    for(int i = 0; i < [_dicFocusDetail[@"users"] count] ;i++){
                        
                        if(i < 4){
                            
                        NSMutableDictionary *dicFocusItem = _dicFocusDetail[@"users"][i];
                        
                        _FocusImageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageFocuePresonX, self.labelHuoban.frame.origin.y - 14.65,44,44)];
                        
//                        _FocusImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:dicFocusItem[@"image"]]]];
                            
                        [_FocusImageView sd_setImageWithURL:[NSURL URLWithString:dicFocusItem[@"image"]] placeholderImage:nil];
                        
                        _FocusImageView.clipsToBounds = YES;
                        _FocusImageView.layer.cornerRadius = 22;
                        _FocusImageView.userInteractionEnabled = YES;
                        _FocusImageView.contentMode = UIViewContentModeScaleAspectFill;
                        [self.viewFocusBlock addSubview:_FocusImageView];
                        
                        if(i == 0 ){
                            imageFocuePresonX =  self.labelHuoban.frame.origin.x + self.labelHuoban.frame.size.width + 9;
                        }else{
                            imageFocuePresonX = ( self.labelHuoban.frame.origin.x + self.labelHuoban.frame.size.width + 9) + (i * 53);
                        }
                        
                        
                        endFocusXXX = self.labelHuoban.frame.origin.x + self.labelHuoban.frame.size.width + 9;
                        endFocusYYY =  self.labelHuoban.frame.origin.y + 38.25;
                            
                        }
                        
                    }
                }
                
                if([_dicFocusDetail[@"users"] count] > 0 ){
                    UIView *viewMore = [[UIView alloc] initWithFrame:CGRectMake(endFocusXXX,endFocusYYY,44,44)];
                    viewMore.clipsToBounds = YES;
                    viewMore.layer.cornerRadius = 22;
                    viewMore.layer.borderWidth = 1;
                    viewMore.layer.borderColor = [UIColor redColor].CGColor;
                    [self.viewFocusBlock addSubview:viewMore];
                    
                    UIImageView *imageViewMore = [[UIImageView alloc]initWithFrame:CGRectMake(12,20,20,4)];
                    if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
                        imageViewMore.image = [UIImage imageNamed:@"morePreson@3x"];
                    }else{
                        imageViewMore.image = [UIImage imageNamed:@"morePreson@2x"];
                    }
                    imageViewMore.center = viewMore.center;
                    [viewMore addSubview:imageViewMore];
                }
        
                
                
            }else{
                
                //「火伴」头像
                NSInteger imageFocuePresonX = self.labelHuoban.frame.origin.x + self.labelHuoban.frame.size.width + 9;
                
                self.viewFocusHeight.constant = 97;
                
                if([_iOSSizeDetails isEqualToString:@"iPhone6"] ||[_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
                    
                    for(int i = 0;i < [_dicFocusDetail[@"users"] count] ;i++){

                        
                        if(i <= 5){
                            
                            NSMutableDictionary *dicFocusItem = _dicFocusDetail[@"users"][i];
                            
                            _FocusImageView= [[UIImageView alloc] initWithFrame:CGRectMake(imageFocuePresonX, self.labelHuoban.frame.origin.y - 14.65, 44,44)];
                            
                            _FocusImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:dicFocusItem[@"image"]]]];
                            
                            [_FocusImageView sd_setImageWithURL:[NSURL URLWithString:dicFocusItem[@"image"]] placeholderImage:nil];
                            
                            _FocusImageView.clipsToBounds = YES;
                            _FocusImageView.layer.cornerRadius = 22;
                            _FocusImageView.userInteractionEnabled = YES;
                            _FocusImageView.contentMode = UIViewContentModeScaleAspectFill;
                            [self.viewFocusBlock addSubview:_FocusImageView];
                            
                            imageFocuePresonX += 53;
                            
                            endFocusXXX = imageFocuePresonX;
                            endFocusYYY =  self.labelHuoban.frame.origin.y - 14.65;
                            
                            
                        }else if(i > 5 && i <= 10){
                            
                            imageFocuePresonX = ( self.labelHuoban.frame.origin.x + self.labelHuoban.frame.size.width + 9) + ((i-6) * 53);
                            
                            NSMutableDictionary *dicFocusItem = _dicFocusDetail[@"users"][i];
                            
                            _FocusImageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageFocuePresonX, self.labelHuoban.frame.origin.y + 38.25, 44,44)];
                            
//                            _FocusImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:dicFocusItem[@"image"]]]];
                            
                            [_FocusImageView sd_setImageWithURL:[NSURL URLWithString:dicFocusItem[@"image"]] placeholderImage:nil];
                            
                            _FocusImageView.clipsToBounds = YES;
                            _FocusImageView.layer.cornerRadius = 22;
                            _FocusImageView.userInteractionEnabled = YES;
                            _FocusImageView.contentMode = UIViewContentModeScaleAspectFill;
                            [self.viewFocusBlock addSubview:_FocusImageView];
                            
                            endFocusXXX =  imageFocuePresonX + 53;
                            endFocusYYY =  self.labelHuoban.frame.origin.y + 38.25;
                            
                        }
                    }
                }else{
                    
                    //「火伴」头像
                    NSInteger imageFocuePresonX = self.labelHuoban.frame.origin.x + self.labelHuoban.frame.size.width + 9;
                    
                    for(int i = 0;i < [_dicFocusDetail[@"users"] count] ;i++){
                        if(i <= 4){
                            
                            NSMutableDictionary *dicFocusItem = _dicFocusDetail[@"users"][i];
                            
                            _FocusImageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageFocuePresonX, self.labelHuoban.frame.origin.y - 14.65, 44,44)];
                            
//                            _FocusImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:dicFocusItem[@"image"]]]];
                            
                            [_FocusImageView sd_setImageWithURL:[NSURL URLWithString:dicFocusItem[@"image"]] placeholderImage:nil];
                            
                            _FocusImageView.clipsToBounds = YES;
                            _FocusImageView.layer.cornerRadius = 22;
                            _FocusImageView.userInteractionEnabled = YES;
                            _FocusImageView.contentMode = UIViewContentModeScaleAspectFill;
                            [self.viewFocusBlock addSubview:_FocusImageView];
                            
                            imageFocuePresonX += 53;
                            
                            endFocusXXX = imageFocuePresonX;
                            endFocusYYY =  self.labelHuoban.frame.origin.y - 14.65;
                            
                        }else if(i > 4 && i <= 8){
                            
                            imageFocuePresonX  = ( self.labelHuoban.frame.origin.x + self.labelHuoban.frame.size.width + 9) + ((i-5) * 53);
                            
                            NSMutableDictionary *dicFocusItem = _dicFocusDetail[@"users"][i];
                            
                            _FocusImageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageFocuePresonX, self.labelHuoban.frame.origin.y + 38.25, 44,44)];
                            
//                            _FocusImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:dicFocusItem[@"image"]]]];
                            
                            [_FocusImageView sd_setImageWithURL:[NSURL URLWithString:dicFocusItem[@"image"]] placeholderImage:nil];
                            
                            _FocusImageView.clipsToBounds = YES;
                            _FocusImageView.layer.cornerRadius = 22;
                            _FocusImageView.userInteractionEnabled = YES;
                            _FocusImageView.contentMode = UIViewContentModeScaleAspectFill;
                            [self.viewFocusBlock addSubview:_FocusImageView];
                            
                            endFocusXXX =  imageFocuePresonX + 53;
                            endFocusYYY =  self.labelHuoban.frame.origin.y + 38.25;
                            
                        }
                        
                    }
                    
                }
             
                if([_dicFocusDetail[@"users"] count] >0){
                    
                    UIView *viewMore = [[UIView alloc] initWithFrame:CGRectMake(endFocusXXX,endFocusYYY,44,44)];
                    viewMore.clipsToBounds = YES;
                    viewMore.layer.cornerRadius = 22;
                    viewMore.layer.borderWidth = 1;
                    viewMore.layer.borderColor = [UIColor redColor].CGColor;
                    [self.viewFocusBlock addSubview:viewMore];
                    
                    UIImageView *imageViewMore = [[UIImageView alloc]initWithFrame:CGRectMake(12,20,20,4)];
                    if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
                        imageViewMore.image = [UIImage imageNamed:@"morePreson@3x"];
                    }else{
                        imageViewMore.image = [UIImage imageNamed:@"morePreson@2x"];
                    }
                    //                imageViewMore.center = viewMore.center;
                    [viewMore addSubview:imageViewMore];
                    
                }

                
            }
            
            NSInteger viewHeightLastAryChou = 0;
            NSInteger labelTitleYYY = 0;
            NSInteger labelTitleWidth = 0;
            NSInteger viewChouWWWW = 16;
            NSInteger labelContentWWW = 23;
            NSInteger labelMoneyWidth = 0;
            NSInteger labelRightCount = 0;
            NSInteger labelPresonWidth = 0;
            NSInteger viewChouWidth = 0;
            
            
            _viewChouYYY = self.imagesTimeLines.frame.origin.y + self.imagelineHeight.constant  + _viewTimeLineDown;
            labelTitleYYY = 120;
            
            //众筹框
            if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){

                viewChouWWWW = 23;
                labelContentWWW = 28;
                labelMoneyWidth = 150;
                labelTitleWidth = 180;
                labelRightCount = 16;
                labelPresonWidth = 130;
                viewChouWidth = 32;
                
            }else if([_iOSSizeDetails isEqualToString:@"iPhone6"]){
                
                viewChouWWWW = 16;
                labelContentWWW = 23;
                labelMoneyWidth = 150;
                labelTitleWidth = 180;
                labelRightCount = 16;
                labelPresonWidth = 130;
                viewChouWidth = 24;
                
            }else if([_iOSSizeDetails isEqualToString:@"iPhone5"]){
                

                viewChouWWWW = 16;
                labelContentWWW = 23;
                labelMoneyWidth = 130;
                labelTitleWidth = 150;
                labelRightCount = 12;
                labelPresonWidth = 130;
                viewChouWidth = 24;
                
            }else if([_iOSSizeDetails isEqualToString:@"iPhone4"]){
                
                viewChouWWWW = 16;
                labelContentWWW = 23;
                labelMoneyWidth = 130;
                labelTitleWidth = 150;
                labelRightCount = 12;
                labelPresonWidth = 130;
                viewChouWidth = 24;
            }
            
            for(int i =0;i < [_aryChou count];i++){
                
                NSMutableDictionary *dicChouItems = _aryChou[i];
                NSLog(@"%@",dicChouItems);
                
                _viewChou = [[UIView alloc] initWithFrame:CGRectMake(12,_viewChouYYY + i,self.view.frame.size.width - 24,100)];
//                _viewChou.layer.borderColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1].CGColor;
                
                _viewChou.layer.borderWidth = 1;
                
                [self.scrollView addSubview:_viewChou];
                [_aryPayView addObject:_viewChou];
                
                //支持100元
                UILabel *labelMoney = [[UILabel alloc] initWithFrame:CGRectMake(12,12,labelMoneyWidth,14.7)];
                labelMoney.text = [NSString stringWithFormat:@"支持%ld元",[dicChouItems[@"money"]longValue]];
//                labelMoney.font = [UIFont fontWithName:@"Helvetica-Bold" size:14.7];
                labelMoney.font = [UIFont boldSystemFontOfSize:14.7];
//                labelMoney.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];

                [_viewChou addSubview:labelMoney];
                
                //title
                
                _labelTitleChouView = [[UILabel alloc] initWithFrame:CGRectMake(_viewChou.frame.size.width - labelTitleWidth - labelRightCount,12,labelTitleWidth,14.7)];
                _labelTitleChouView.text = [NSString stringWithFormat:@"%@",dicChouItems[@"title"]];
                _labelTitleChouView.font = [UIFont boldSystemFontOfSize:14.7];
//                _labelTitleChouView.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
                _labelTitleChouView.textAlignment = NSTextAlignmentRight;
                [_viewChou addSubview:_labelTitleChouView];
                
                
                //contents
                NSMutableAttributedString  *strContents =[[NSMutableAttributedString alloc]initWithString:dicChouItems[@"desc"]];
                
                NSMutableParagraphStyle * paragraphSummarizeStyle=[[NSMutableParagraphStyle alloc]init];
                
                [paragraphSummarizeStyle setMinimumLineHeight:24];
                
                NSLog(@"%@",dicChouItems[@"desc"]);
                
                [strContents addAttribute:NSParagraphStyleAttributeName value:paragraphSummarizeStyle range:NSMakeRange(0, [dicChouItems[@"desc"] length])];
                
                _labelContentsChouView= [[UILabel alloc] initWithFrame:CGRectMake(12,labelMoney.frame.origin.y + 12,0,0)];
                _labelContentsChouView.attributedText = strContents;
                _labelContentsChouView.textAlignment = NSTextAlignmentNatural;
                [_labelContentsChouView setNumberOfLines:0];
                
                CGSize labelContenteSize =  [ImageSameWidth getSizeWithStr:[NSString stringWithFormat:@"%@",strContents] width:_viewChou.frame.size.width - 38 font:14.7];
                
                _labelContentsChouView.font = [UIFont fontWithName:@"PingFang SC" size:14.7];
                
                _labelContentsChouView.frame =CGRectMake(12,labelMoney.frame.origin.y + 18,_viewChou.frame.size.width - labelRightCount,labelContenteSize.height);
            
                [_labelContentsChouView sizeToFit];
                
                [_viewChou addSubview:_labelContentsChouView];
                
                //预计发货时间
                _labelSendTimeChouView = [[UILabel alloc] initWithFrame:CGRectMake(12,_labelContentsChouView.frame.origin.y + _labelContentsChouView.frame.size.height + 10,150,12)];
                
                _labelSendTimeChouView.text =[NSString stringWithFormat:@"众筹结束后%lu天内发送",[dicChouItems[@"send_number"] longValue]];
                _labelSendTimeChouView.font = [UIFont systemFontOfSize:12];
                _labelSendTimeChouView.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
                //labelSendTime.layer.borderColor = [UIColor colorWithRed:64.0/255 green:171.0/255 blue:169.0/255 alpha:1].CGColor;
                //                    labelSendTime.textAlignment = NSTextAlignmentRight;
                [_viewChou addSubview:_labelSendTimeChouView];
                
                //剩余人数
                _labelPresonCountChouView = [[UILabel alloc] initWithFrame:CGRectMake(_viewChou.frame.size.width - 12 - labelRightCount,_labelContentsChouView.frame.origin.y + _labelContentsChouView.frame.size.height + 8,labelPresonWidth,12)];
                
                if([dicChouItems[@"want"]longValue] >10000 ){
                    _labelPresonCountChouView.text = [NSString stringWithFormat:@"%ld人支持",[dicChouItems[@"fact"] longValue]];
                }else{
                    _labelPresonCountChouView.text = [NSString stringWithFormat:@"%ld人支持/%ld个名额",[dicChouItems[@"fact"] longValue],[dicChouItems[@"want"] longValue]];
                    
                }
                
                _labelPresonCountChouView.font = [UIFont systemFontOfSize:12];
//                _labelPresonCountChouView.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
//                _labelPresonCountChouView.layer.borderColor = [UIColor colorWithRed:64.0/255 green:171.0/255 blue:169.0/255 alpha:1].CGColor;
                _labelPresonCountChouView.textAlignment = NSTextAlignmentRight;
                [_labelPresonCountChouView sizeToFit];
                
                
                _labelPresonCountChouView.frame = CGRectMake(_viewChou.frame.size.width - _labelPresonCountChouView.frame.size.width - labelRightCount,_labelContentsChouView.frame.origin.y + _labelContentsChouView.frame.size.height + 8,_labelPresonCountChouView.frame.size.width,12);
                [_viewChou addSubview:_labelPresonCountChouView];
                
//                _labelPresonCountChouView.layer.borderWidth = 1;
                
                UIImageView *viewFire = [[UIImageView alloc] initWithFrame:CGRectMake(_labelPresonCountChouView.frame.origin.x - 20, _labelPresonCountChouView.frame.origin.y - 3,17, 18)];
                


                [_viewChou addSubview:viewFire];
                

                _viewChou.frame = CGRectMake(16,_viewChouYYY,_iOSDeviceDetails.width - viewChouWidth,
                                                             _labelTitleChouView.frame.size.height + _labelContentsChouView.frame.size.height + _labelSendTimeChouView.frame.size.height + 38);

                _viewChou.layer.borderWidth = 1;
                
                _buttonPay = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                [_buttonPay addTarget:self
                               action:@selector(buttonPaySegue:)
                     forControlEvents:UIControlEventTouchUpInside];
                
                _buttonPay.frame = CGRectMake(0,0,_viewChou.frame.size.width,_viewChou.frame.size.height);
                
                _buttonPay.tag = i;
                [_viewChou addSubview:_buttonPay];
                
//                _buttonPay.layer.borderWidth = 1;
                
                _viewChouYYY += _viewChou.frame.size.height + 18;
                
                _viewChou.clipsToBounds = YES;
                _viewChou.layer.cornerRadius = 8;
                
                NSLog(@"众筹选项是否支持%ld",[dicChouItems[@"isorder"] longValue] );
                if([dicChouItems[@"isorder"] longValue] == 0){
                   
                    //颜色设置
                    //框体
                    _viewChou.layer.borderColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1].CGColor;
                    
                    _viewChou.backgroundColor = nil;
                    
                    //title
                    _labelTitleChouView.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
                    
                    //钱数
                    labelMoney.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
                    
                    //内容
                    _labelContentsChouView.textColor = [UIColor colorWithRed:170.0/255 green:170.0/255 blue:170.0/255 alpha:1];
                    
                    //预计发货时间
                    _labelSendTimeChouView.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
                    
                    //支持人数
                    _labelPresonCountChouView.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
                    
                    viewFire.image = [UIImage imageNamed:@"fireGreen"];
                    
                    [_aryChouViewiSJoined addObject:@"0"];
                    
                }else if([dicChouItems[@"isorder"] longValue] == 1){
                    //颜色设置
                    //框体
                 _viewChou.layer.borderColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1].CGColor;

                    _viewChou.backgroundColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
                    
                    //title
                    _labelTitleChouView.textColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
                    
                    //钱数
                    labelMoney.textColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
                    
                    //内容
                    _labelContentsChouView.textColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
                    
                    //预计发货时间
                    _labelSendTimeChouView.textColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
                    
                    //支持人数
                    _labelPresonCountChouView.textColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
                    
                    [_aryChouViewiSJoined addObject:@"1"];
                    
                    viewFire.image = [UIImage imageNamed:@"fireWhite"];
                }
                
                if(i == [_aryChou count] - 1){
                    viewHeightLastAryChou = _viewChou.frame.size.height;
                }
                
                NSLog(@"%f-----%f",_viewChou.frame.origin.x,_viewChou.frame.origin.y);
            }
            


            
            if([_aryChou count] != 0 ){
                
                
                //                if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
                //                    _floatScorllHeight =  viewChouYYY + 44 ;
                //                }else{
                //                    _floatScorllHeight =  viewChouYYY + 44 ;
                //                }
                
                NSLog(@"滚动高度滚动高度%f",_floatScorllHeight);
                
            }else{

            }
            [self.textTitle sizeToFit];
            [self.labelCreator sizeToFit];

//            [self.moviePlayer prepareToPlay];

        }else{
            [[ToolClass sharedInstance]showAlert:@"刷新失败，服务器报错"];
        }
    }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
        [[ToolClass sharedInstance]showAlert:@"刷新失败,请检查网络"];
    }];
   }

//发起人头像点击
-(void)buttonCreatorImageClick:(UITapGestureRecognizer*)sender{
    
//    [self performSegueWithIdentifier:@"OtherUserSegue" sender:_dicCreateDetail[sender.view.tag]];
    
    UserSelfMessageViewController * userInfoVC = [[UserSelfMessageViewController alloc]initForOtherUserWithUserID:_dicCreateDetail[sender.view.tag][@"_id"]];
    self.navigationItem.hidesBackButton = YES;
    
//    [self presentViewController:userInfoVC animated:YES completion:nil];
    
    [self.navigationController pushViewController:userInfoVC animated:YES];

#warning segue
}


-(void)buttonLoginClick:(UIButton*)sender{
    
    NSLog(@"点击弹出登陆按钮按钮按钮");
    _viewProjectDetails.hidden = YES;
    self.scrollView.scrollEnabled = YES;
    _viewButtons.hidden = NO;
    
    [self performSegueWithIdentifier:@"loginSegue" sender:nil];
    
}

-(void)buttonLoginCloseClick:(UIButton*)sender{
    
    _viewProjectDetails.hidden = YES;
    self.scrollView.scrollEnabled = YES;
    _viewButtons.hidden = NO;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    _runtimesDetails  = [[NSUserDefaults standardUserDefaults] integerForKey:@"runtimes"];
    
    _viewProjectDetails.hidden = YES;
    
    self.viewSuccess.hidden = YES;
    
    _isVideoAppear = NO;
  
    //发起人头像--11.03只有一个发起人，多个发起人需改代码
    for(int i = 0;i <[_aryPayView count];i++){

        
        
        UIView *viewPayList = _aryPayView[i];
        
        if([_aryChouViewiSJoined[i] isEqualToString:@"0"]){
            
            viewPayList.backgroundColor = nil;
            UILabel *labelMoney = viewPayList.subviews[0];
            labelMoney.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
            
            UILabel *labelTitle = viewPayList.subviews[1];
            labelTitle.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
            
            
            UILabel *labelContents = viewPayList.subviews[2];
            labelContents.textColor = [UIColor colorWithRed:170.0/255 green:170.0/255 blue:170.0/255 alpha:1];
            
            UILabel *labelSendTime = viewPayList.subviews[3];
            labelSendTime.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
            
            
            UILabel *labelPresonCount = viewPayList.subviews[4];
            labelPresonCount.textColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
            
            UIImageView *viewFire = viewPayList.subviews[5];
            viewFire.image = [UIImage imageNamed:@"fireGreen"];
        }else if([_aryChouViewiSJoined[i] isEqualToString:@"1"]){
            
            
            viewPayList.backgroundColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
            
            UILabel *labelMoney = viewPayList.subviews[0];
            labelMoney.textColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
            
            UILabel *labelTitle = viewPayList.subviews[1];
            labelTitle.textColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
            
            
            UILabel *labelContents = viewPayList.subviews[2];
            labelContents.textColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
            
            UILabel *labelSendTime = viewPayList.subviews[3];
            labelSendTime.textColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
            
            UILabel *labelPresonCount = viewPayList.subviews[4];
            labelPresonCount.textColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
            
            UIImageView *viewFire = viewPayList.subviews[5];
            viewFire.image = [UIImage imageNamed:@"fireWhite"];
        }
        


        
    }
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
        [self setNeedsStatusBarAppearanceUpdate];
    
//    self.imagesTimeLines.layer.borderWidth = 1;
    
    _viewChouYYY = self.imagesTimeLines.frame.origin.y + self.imagelineHeight.constant  ;
    
    for(int i = 0;i <[_aryPayView count];i++){
        
        UIView *viewPayList = _aryPayView[i];
        
        viewPayList.frame = CGRectMake(viewPayList.frame.origin.x, _viewChouYYY,viewPayList.frame.size.width,viewPayList.frame.size.height);
    
        
        _viewChouYYY += viewPayList.frame.size.height + 18;
    }
    
//    NSLog(@"记录记录记录-----%f--%f",self.labelCreator.frame.origin.x,self.labelCreator.frame.origin.y);
    
    self.scrollView.clipsToBounds = YES;

    NSLog(@"%f",_floatScorllHeight);
    CGSize scrollViewHeight =  CGSizeMake (self.scrollView.frame.size.width,_viewChouYYY + 44);
    self.scrollView.contentSize  = scrollViewHeight;
    
    
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
//    
//    if(self.videoKRController != nil){
//       
//    }
    //0109更改，退出kill视频控件
    [self.videoKRController dismiss];
     [self.videoKRController.view removeFromSuperview];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    if(self.moviePlayer != nil){
        
        [self.moviePlayer pause];
        
    }
    

    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)segmentedClick:(id)sender{
    UISegmentedControl *control = (UISegmentedControl*)sender;
    
    
    if(control.selectedSegmentIndex == 0){
            [self dismissViewControllerAnimated:YES completion:nil];
    }else if(control.selectedSegmentIndex == 1){
        
        [self performSegueWithIdentifier:@"JoinProjectSegue" sender:_aryChou];
        
        NSLog(@"%@",_aryChou);
        
    }else if(control.selectedSegmentIndex == 2){
        
//       _shareView = [[ProjectDetailsShare alloc] initWithFrame:CGRectMake(80, 497,200,100)];
//    
//        
//        [_shareView.buttonWeChat addTarget:self action:@selector(buttonWeChatClick:) forControlEvents:UIControlEventTouchUpInside];
//        
//        
//        [_shareView.buttonWeChatS addTarget:self action:@selector(buttonWeChatsClick:)forControlEvents:UIControlEventTouchUpInside];
//        
//        
//        [_shareView.buttonClose addTarget:self action:@selector(buttonCloseClick:) forControlEvents:UIControlEventTouchUpInside];
//        
//        [self.view addSubview:_shareView];
        
        [[ToolClass sharedInstance] showAlert:@"「分享」功能正在开发，稍后上线~"];
    }
}
//-(void)buttonWeChatClick:(UIButton*)sender{
//    
//    NSLog(@"弹出微信分享框");
//}
//-(void)buttonWeChatsClick:(UIButton*)sender{
//    NSLog(@"弹出朋友圈分享框");
//}

-(void)buttonCloseClick:(UIButton*)sender{
    
    [_shareView removeFromSuperview];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"JoinProjectSegue"]){
        
        UINavigationController *naVC = segue.destinationViewController;
        
        JoinProjectTableViewController *controller = (JoinProjectTableViewController*)naVC.topViewController;
        
        controller.aryDataChou = _aryChou;
        controller.countDownJoinDate = _countDownData;
        controller.dataDate = _DetailsDate;
        controller.projectID = self.strProjectID;
        
    }else if([segue.identifier isEqualToString:@"MoreStorySegue"]){
        
        
        UINavigationController *naVC = segue.destinationViewController;
        
        ProjectMoreStoryViewController *controller = (ProjectMoreStoryViewController*)naVC.topViewController;
        
        controller.strContents = _strHTMLContents;
        controller.aryChouMoreStory = _aryChou;
        controller.strProjectID = self.strProjectID;
        controller.countPayDate = _countDownData;
        controller.dateEnd = _DetailsDate;
        controller.strShareImage = _strShareImage;
        controller.strShareText = _strShareText;
        controller.strShareTitle = _strShareTitle;
        controller.isProjectEnd = _isEndProject;
        controller.isJoinProject = _isJoinProject;
        
    }else if([segue.identifier isEqualToString:@"MoreDynamicSegue"]){
        
        UINavigationController *naVC = segue.destinationViewController;
        
        MoreDynamicViewController *controller = (MoreDynamicViewController*)naVC.topViewController;
        
        controller.strProjectID = self.strProjectID;
        controller.aryChouMordDynameic = _aryChou;
        controller.countPayDate = _countDownData;
        controller.dateEnd = _DetailsDate;
        controller.strShareImage = _strShareImage;
        controller.strShareText = _strShareText;
        controller.strShareTitle = _strShareTitle;
        controller.isProjectEnd = _isEndProject;
        controller.isjoinProject = _isJoinProject;
        
    }else if([segue.identifier isEqualToString:@"showHuobanPresonSegue"]){
        
        UINavigationController *naVC = segue.destinationViewController;
        
        SuppotPresonViewController *controller = (SuppotPresonViewController*)naVC.topViewController;
        
        controller.dicCreator = _dicCreateDetail;
        controller.dicChou = _dicFocusDetail;
        controller.strShareImage = _strShareImage;
        controller.strShareText = _strShareText;
        controller.strShareTitle = _strShareTitle;
        controller.isProjectEnd = _isEndProject;
        controller.isJoinProject = _isJoinProject;
        controller.strProjectID = self.strProjectID;
        
    }else if([segue.identifier isEqualToString:@"PaySegue"]){
        
        UINavigationController *naVC = segue.destinationViewController;
        
        PayForProjectTableViewController *controller = (PayForProjectTableViewController*)naVC.topViewController;
        
        controller.dicPayDataModel = sender;
        controller.projectID = self.strProjectID;
        controller.projectEndDate = _DetailsDate;
        
    }else if([segue.identifier isEqualToString:@"loginSegue"]){
        
        LoginViewController *controller = segue.destinationViewController;
        
        controller.delegate = self;
        
    }else if([segue.identifier isEqualToString:@"OtherUserSegue"]){
        
        UINavigationController *naVC = segue.destinationViewController;
        
        OtherUserInfoViewController *controller = (OtherUserInfoViewController*)naVC.topViewController;
        
        controller.dicOtherUser = sender;
        
        
    }
    
}
- (IBAction)buttonMoreStory:(id)sender {
    
    if(self.moviePlayer != nil){
    [self.moviePlayer pause];
    }

    
    [self performSegueWithIdentifier:@"MoreStorySegue" sender:nil];
}

- (IBAction)buttonMoreDynamic:(id)sender {
    
//    ProjectDynamicSelfTableViewController *projectDynamic = [[ProjectDynamicSelfTableViewController alloc]init];
//    
//    projectDynamic.strProjectID = self.strProjectID;
//    projectDynamic.delegate = self;
//    
//    [self addChildViewController:projectDynamic];
////    [self presentViewController:projectDynamic animated:YES completion:nil];
//    
//    
//    if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
//        projectDynamic.view.frame = CGRectMake(15, 110, 384, 556);
//    }else if([_iOSSizeDetails isEqualToString:@"iPhone6"]){
//        projectDynamic.view.frame = CGRectMake(15, 110,345,507);
//    }else if([_iOSSizeDetails isEqualToString:@"iPhone5"]){
//        projectDynamic.view.frame = CGRectMake(15, 180,290,450);
//    }else{
//        projectDynamic.view.frame = CGRectMake(15, 60,290,390);
//    }
//    projectDynamic.view.layer.cornerRadius = 5;
//
//
////    self.buttonMoreMessage.alpha = 2;
////    self.buttonMoreStory.alpha = 2;
//    [self.view addSubview:projectDynamic.view];
//    
//    [projectDynamic didMoveToParentViewController:self];
//    
//    self.scrollView.viewForBaselineLayout.backgroundColor = [UIColor darkGrayColor];
//    self.scrollView.alpha = 0.9;
//    self.progressView.alpha = 0.3;
    
//    self.buttonMoreDynamic.backgroundColor = [UIColor colorWithRed:0.0/255 green:153.0/255 blue:153.0/255 alpha:1];
//    [self.buttonMoreDynamic setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    if(self.moviePlayer != nil){
    [self.moviePlayer pause];
    }
    

    
    [self performSegueWithIdentifier:@"MoreDynamicSegue" sender:nil];
    
}
//-(void)CloseProjectDynamicSelfController:(ProjectDynamicSelfTableViewController *)conttoller{
//    [conttoller.view removeFromSuperview];
//    
//    self.scrollView.viewForBaselineLayout.backgroundColor = [UIColor whiteColor];
//    self.scrollView.alpha = 1.0;
//    self.progressView.alpha = 1.0;
//}
//-(void)ProjectMoreStoryViewControllerButtonClose:(ProjectMoreStoryViewController *)controller{
//    [controller.view removeFromSuperview];
//    
//    self.scrollView.viewForBaselineLayout.backgroundColor = [UIColor whiteColor];
//    self.scrollView.alpha = 1.0;
//    self.progressView.alpha = 1.0;
//    
//}
- (IBAction)buttonLove:(id)sender {

    if(_runtimesDetails != 1){
        
        _viewProjectDetails.hidden = NO;
        self.scrollView.scrollEnabled = NO;
        _viewButtons.hidden = YES;
        
        
    }else{
    
    
    [_httpClassProjectDynamic projectDetailsSetProjectID:self.strProjectID token:_dataModelProjectDyanmic.userInfomation.tokenID CallBackYES:^(MKNetworkOperation *operatioin){
        NSData *data = [operatioin responseData];
        NSMutableDictionary *resHomePageDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        if([resHomePageDic[@"status"] isEqualToString:@"success"]){
            
            NSLog(@"打印项目详情返回对象%@",resHomePageDic);
            
            NSDictionary *dicLoveButtons  =  resHomePageDic[@"data"];

                _aryLoveButtons = [[NSMutableArray alloc] init];
                
                if([dicLoveButtons[@"isFocus"] longValue] == 0){
                    
                    [_httpClassProjectDynamic focusProjectselfSetProjectID:self.strProjectID up:1 token:_dataModelProjectDyanmic.userInfomation.tokenID CallBackYES:^(MKNetworkOperation *operatioin){
                        
                        NSData *data = [operatioin responseData];
                        
                        NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                        
                        //            NSLog(@"打印点赞返回%@",dic);
                        if([dic[@"status"] isEqualToString:@"success"]){
                            
                            [[ToolClass sharedInstance] showAlert:@"关注成功"];
                            if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
                                self.imageViewLove.image = [UIImage imageNamed:@"details_Love@3x"];
                            }else{
                                self.imageViewLove.image = [UIImage imageNamed:@"details_Love@2x"];                            }
                        }
                    }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
                        [[ToolClass sharedInstance] showAlert:@"关注失败，请检查网络"];
                    }];

                    
                }else{
                    
                    
                    [_httpClassProjectDynamic focusProjectselfSetProjectID:self.strProjectID up:0 token:_dataModelProjectDyanmic.userInfomation.tokenID CallBackYES:^(MKNetworkOperation *operatioin){
                        
                        NSData *data = [operatioin responseData];
                        
                        NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                        
                        NSLog(@"打印点赞返回%@",dic);
                        if([dic[@"status"] isEqualToString:@"success"]){
                            
                            [[ToolClass sharedInstance] showAlert:@"取消关注成功"];
                            if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
                                
                                self.imageViewLove.image =[UIImage imageNamed:@"details_UnLove@3x"];
                            }else{
                                self.imageViewLove.image =[UIImage imageNamed:@"details_UnLove@2x"];                            }
                        }
                    }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
                        [[ToolClass sharedInstance] showAlert:@"取消关注失败，请检查网络"];
                    }];
                
            }
        }else{
            [[ToolClass sharedInstance]showAlert:@"刷新失败，服务器报错"];
        }
    }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
        [[ToolClass sharedInstance]showAlert:@"刷新失败,请检查网络"];
    }];
    }
}

-(void)mediaPlayerGetInFull:(NSNotification *)notification{

    //保持比例
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    

    
//    CGAffineTransform landscapeTransform = CGAffineTransformMakeRotation(M_PI / 2);
//    self.moviePlayer.view.transform = landscapeTransform;

//    [self.moviePlayer.view setFrame:CGRectMake(0, 0,_iOSDeviceDetails.height,_iOSDeviceDetails.width)];
    
//   [self.moviePlayer.view setFrame:CGRectMake(0, 0,160,90)];
    
//    //  旋转
//    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
//    appDelegate.isRotation = YES;
    
    [self.moviePlayer play];
    
    
    
    
}



-(void)mediaPlayerGetInCloseFull:(NSNotification*)notification{
    
//    [self.moviePlayer.view setFrame:CGRectMake(0, 0,_iOSDeviceDetails.width + 10, _heightVideo)];
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;

//    CGAffineTransform landscapeTransform = CGAffineTransformMakeRotation(M_PI / 4);
//    self.moviePlayer.view.transform = landscapeTransform;
    
//    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
//    appDelegate.isRotation = NO;

    [self.moviePlayer play];
}

-(void)addNotification{
    NSNotificationCenter *notificationCenter=[NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self selector:@selector(mediaPlayerGetInFull:) name:MPMoviePlayerDidEnterFullscreenNotification object:self.moviePlayer];

    NSNotificationCenter *notificationCenterClose=[NSNotificationCenter defaultCenter];
    [notificationCenterClose addObserver:self selector:@selector(mediaPlayerGetInCloseFull:) name:MPMoviePlayerDidExitFullscreenNotification object:self.moviePlayer];
}

-(void)buttonCancelWill{
    
    if(self.isHomePage == YES){
//        [self.delegate projectDeatilsCancelFromHomePage:self];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    

}

-(void)buttonJoinWill{
    
    if(_runtimesDetails != 1){
        
    _viewProjectDetails.hidden = NO;
    self.scrollView.scrollEnabled = NO;
    _viewButtons.hidden = YES;
        
    }else{
    
    [self performSegueWithIdentifier:@"JoinProjectSegue" sender:_aryChou];
        
    }
}

-(void)buttonShareWill{
    
    
    /*
    //微信个人分享
   // 注意：分享到微信好友、微信朋友圈、微信收藏、QQ空间、QQ好友、来往好友、来往朋友圈、易信好友、易信朋友圈、Facebook、Twitter、Instagram等平台需要参考各自的集成方法
    [UMSocialData defaultData].extConfig.wechatSessionData.title = _strShareTitle;
//    [UMSocialData defaultData].extConfig.wechatSessionData.url = [NSString stringWithFormat:@"http://www.huoban.io/project_detail.html?p_id=%@",];
    [[UMSocialControllerService defaultControllerService] setShareText:_strShareText shareImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_strShareImage]]] socialUIDelegate:self];
    
    //设置分享内容和回调对象
    [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession].snsClickHandler(self,[UMSocialControllerService defaultControllerService],YES);
     */


    
    
    
    
    /*
    //注意：分享到微信好友、微信朋友圈、微信收藏、QQ空间、QQ好友、来往好友、来往朋友圈、易信好友、易信朋友圈、Facebook、Twitter、Instagram等平台需要参考各自的集成方法
    [[UMSocialControllerService defaultControllerService] setShareText:_strShareText shareImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_strShareImage]]] socialUIDelegate:self];
    
    //    [UMSocialData defaultData].extConfig.wxMessageType = UMSocialYXMessageTypeImage;
    //设置分享内容和回调对象
    [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatTimeline].snsClickHandler(self,[UMSocialControllerService defaultControllerService],YES);
    
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = @"http://huoban.io";
    [UMSocialData defaultData].extConfig.wechatSessionData.title = @"测试title";
    */
    
    
    
    //注意：分享到微信好友、微信朋友圈、微信收藏、QQ空间、QQ好友、来往好友、来往朋友圈、易信好友、易信朋友圈、Facebook、Twitter、Instagram等平台需要参考各自的集成方法
    //如果需要分享回调，请将delegate对象设置self，并实现下面的回调方法
//    [UMSocialData defaultData].extConfig.wechatSessionData.title = _strShareTitle;
//    [UMSocialData defaultData].extConfig.wechatSessionData.url = [NSString stringWithFormat:@"http://www.huoban.io/project_detail.html?p_id=%@",_strProjectID];
    
//    
//    [UMSocialSnsService presentSnsIconSheetView:self
//                                         appKey:@"5629c5de67e58e4ea8002490"
//                                      shareText:_strShareText
//                                     shareImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_strShareImage]]]
//                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToWechatSession,UMShareToWechatTimeline,nil]
//                                       delegate:self];

    
    
    
    [UIView animateWithDuration:0.3f animations:^{
        _viewShareShaow.hidden = NO;
        _viewShareShaow.alpha = 1.0;
    } completion:^(BOOL finished) {
        
    }];
    
}
//实现回调方法（可选）：
-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    //根据`responseCode`得到发送结果,如果分享成功
    if(response.responseCode == UMSResponseCodeSuccess)
    {
        //得到分享到的微博平台名
        NSLog(@"share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
    }
}


- (IBAction)buttonAllPreson:(id)sender {
    
    [self performSegueWithIdentifier:@"showHuobanPresonSegue" sender:nil];
}

-(void)buttonPaySegue:(UIButton*)sender{
    
    if(_runtimesDetails != 1){
        
        _viewProjectDetails.hidden = NO;
        self.scrollView.scrollEnabled = NO;
        _viewButtons.hidden = YES;
        
        
    }else{
    
    
    if(_countDownData <= 0 ){
        
        [[ToolClass sharedInstance] showAlert:@"此项目众筹期已结束，你仍可以「关注」来加入项目社群喔"];
        return;
    }
    

    NSMutableDictionary *dicItem = _aryChou[sender.tag];
    
//    labelPresonCount.text = [NSString stringWithFormat:@"已支持%ld/%ld个名额",[dicChouItems[@"fact"] longValue],[dicChouItems[@"want"] longValue]];
    
    if([dicItem[@"fact"] longValue] - [dicItem[@"want"] longValue] == 0){
        
        [[ToolClass sharedInstance] showAlert:@"此档位已满，请选择其他档位支持"];
        return;
    }
    
    UIView *viewPayList = _aryPayView[sender.tag];
    viewPayList.backgroundColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];
    
    UILabel *labelMoney = viewPayList.subviews[0];
    labelMoney.textColor = [UIColor whiteColor];
    
    UILabel *labelTitle = viewPayList.subviews[1];
    labelTitle.textColor =  [UIColor whiteColor];
    
    UILabel *labelContents = viewPayList.subviews[2];
    labelContents.textColor =[UIColor whiteColor];
    
    UILabel *labelSendTime = viewPayList.subviews[3];
    labelSendTime.textColor =[UIColor whiteColor];
    
    UILabel *labelPresonCount = viewPayList.subviews[4];
    labelPresonCount.textColor =[UIColor whiteColor];
    
    NSLog(@"%@",_aryChou[sender.tag]);
    [self performSegueWithIdentifier:@"PaySegue" sender:_aryChou[sender.tag]];
    NSLog(@"触发支付页");
        
    }
}

-(void)LoginViewControllerClose:(LoginViewController *)controller{
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark 创建视频按钮
- (IBAction)buttonVideoCreater:(id)sender {
    
    [self videoCreaterWithURL];
}
#pragma mark 创建视频
-(void)videoCreaterWithURL{


            self.imageViewVideo.alpha = 1.0;
    self.imageViewButtonPlay.alpha = 1.0;
        [UIView animateWithDuration:0.3f animations:^{
            self.imageViewVideo.alpha = 0;
            self.imageViewButtonPlay.alpha = 0;
        } completion:^(BOOL finished) {
        self.imageViewVideo.hidden = YES;

            //视频
            self.videoKRController = [[KRVideoPlayerController alloc] initWithFrame:CGRectMake(0, 0,self.imageVideo.frame.size.width,self.imageVideo.frame.size.height)];
            
            self.videoKRController.delegate = self;
            
            [self.scrollView bringSubviewToFront:self.videoKRController.view];

            [self.videoKRController showInWindow];

            self.videoKRController.view.alpha = 0.0;
            [UIView animateWithDuration:0.3f animations:^{
                self.videoKRController.view.alpha = 1.0;
            } completion:^(BOOL finished) {
                self.buttonVideoCreater.hidden = YES;
            }];
            
            self.videoKRController.contentURL = _strVideoURL;
        }];
}

#pragma mark 视频代码方法-关闭
-(void)videoDismissController:(KRVideoPlayerController *)controller{
    

            self.imageViewVideo.alpha = 0.0;
            self.imageViewButtonPlay.alpha = 0.0;
    
            [UIView animateWithDuration:0.3f animations:^{
                self.imageViewVideo.alpha = 1.0;
                self.imageViewButtonPlay.alpha = 1.0;
                _viewButtons.alpha = 1.0;
                _viewButtons.hidden = NO;
            } completion:^(BOOL finished) {
                self.imageViewVideo.hidden = NO;
    
                self.videoKRController.view.alpha = 1.0;
                [UIView animateWithDuration:0.3f animations:^{
                    self.videoKRController.view.alpha = 0.0;
                } completion:^(BOOL finished) {
                    self.scrollView.scrollEnabled = YES;
                    [self.videoKRController.view removeFromSuperview];
                self.buttonVideoCreater.hidden = NO;
                            }];
            }];

}

#pragma mark 视频代码方法-全屏
-(void)videoFullConteoller:(KRVideoPlayerController *)controller{
    
    self.scrollView.scrollEnabled = NO;
    
    [UIView animateWithDuration:0.3f animations:^{
        _viewButtons.alpha = 0.0;
        _viewButtons.hidden = YES;
    } completion:^(BOOL finished) {
        
    }];
    

}

#pragma mark 视频代码方法-退出全屏
-(void)videoDefullController:(KRVideoPlayerController *)controller{

//    [_viewVideoFull removeFromSuperview];
    
    self.scrollView.scrollEnabled = YES;

    [UIView animateWithDuration:0.3f animations:^{
        _viewButtons.alpha = 1.0;
        _viewButtons.hidden = NO;
    } completion:^(BOOL finished) {
        

        
    }];
    
}


#pragma mark tableView代理方法
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    self.videoKRController.frame = CGRectMake(0,0- self.scrollView.contentOffset.y,self.imageVideo.frame.size.width,self.imageVideo.frame.size.height);
    
}



#pragma mark 登陆视图
-(void)viewCreatelogin{
    
    //登陆视图
    _viewProjectDetails = [[UIView alloc] initWithFrame:CGRectMake(0 ,self.scrollView.contentOffset.y, _iOSDeviceDetails.width, _iOSDeviceDetails.height)];
    _viewProjectDetails.backgroundColor = [UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:0.95];
    [self.view addSubview:_viewProjectDetails];
    
    UIView *viewLoginBlock = [[UIView alloc] initWithFrame:CGRectMake(0,0,78,162)];
    viewLoginBlock.backgroundColor = [UIColor colorWithRed:38.0/255 green:55.0/255 blue:70.0/255 alpha:1];
    viewLoginBlock.clipsToBounds = YES;
    viewLoginBlock.layer.cornerRadius = 12;
    viewLoginBlock.center = _viewProjectDetails.center;
    [_viewProjectDetails addSubview:viewLoginBlock];
    
    UIButton *buttonLogin = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonLogin addTarget:self action:@selector(buttonLoginClick:) forControlEvents:UIControlEventTouchUpInside];
    buttonLogin.backgroundColor = [UIColor colorWithRed:119.0/255 green:134.0/255 blue:146.0/255 alpha:1];
    [buttonLogin setTitleColor:[UIColor colorWithRed:38.0/255 green:55.0/255 blue:70.0/255 alpha:1]forState:UIControlStateNormal];
    [buttonLogin setTitle:@"登录" forState:UIControlStateNormal];
    buttonLogin.clipsToBounds = YES;
    buttonLogin.layer.cornerRadius = 27;
    [buttonLogin setFrame:CGRectMake(12,84,54,54)];
    [viewLoginBlock addSubview:buttonLogin];
    
    
    UIImageView *viewClose = [[UIImageView alloc] initWithFrame:CGRectMake(31,24,16,16)];
    if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
        viewClose.image = [UIImage imageNamed:@"close_grey_@3x"];
    }else{
        viewClose.image = [UIImage imageNamed:@"close_grey_@2x"];
    }
    [viewLoginBlock addSubview:viewClose];
    
    UIButton *buttonClose = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonClose addTarget:self action:@selector(buttonLoginCloseClick:) forControlEvents:UIControlEventTouchUpInside];
    [buttonClose setFrame:CGRectMake(17,18,44,44)];
    [viewLoginBlock addSubview:buttonClose];
    
    _viewProjectDetails.hidden = YES;
}

#pragma mark 分段按钮
-(void)viewCreatebuttonJoin{
    
    _viewButtons = [[UIView alloc] initWithFrame:CGRectMake(0,_iOSDeviceDetails.height-50, _iOSDeviceDetails.width, 50)];
    
    _viewButtons.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:_viewButtons];
    
    UIButton *buttonCancelDid = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonCancelDid addTarget:self
                        action:@selector(buttonCancelWill)
              forControlEvents:UIControlEventTouchUpInside];
    
    [buttonCancelDid setBackgroundColor:[UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:1]];
    
    buttonCancelDid.frame = CGRectMake(0,0,70,50);
    
    [buttonCancelDid setTitleColor:[UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1] forState:UIControlStateNormal];
    
    [_viewButtons addSubview:buttonCancelDid];
    
    UIImageView *imageViewCancel = [[UIImageView alloc] initWithFrame:CGRectMake(buttonCancelDid.frame.size.width/2 - 11, 14, 14,22)];
    if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
        imageViewCancel.image = [UIImage imageNamed:@"back_grew_3x"];
    }else{
        imageViewCancel.image = [UIImage imageNamed:@"back_grew_2x"];
    }
    
    [buttonCancelDid addSubview:imageViewCancel];
    
    //分享
    UIButton *buttonShare = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonShare addTarget:self
                    action:@selector(buttonShareWill)
          forControlEvents:UIControlEventTouchUpInside];
    
    [buttonShare setBackgroundColor:[UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:1]];
    
    buttonShare.frame = CGRectMake(_viewButtons.frame.size.width - 70,0,70,50);
    
    [buttonShare setTitleColor:[UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1]  forState:UIControlStateNormal];
    
    [_viewButtons addSubview:buttonShare];
    
    UIImageView *imageViewShare = [[UIImageView alloc] initWithFrame:CGRectMake(buttonShare.frame.size.width/2 - 8, 12, 18,24)];
    if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
        imageViewShare.image = [UIImage imageNamed:@"share_grew@3x"];
    }else{
        imageViewShare.image = [UIImage imageNamed:@"share_grew@2x"];
    }
    
    [buttonShare addSubview:imageViewShare];
    
    
buttonJoinDidProjectDetails = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonJoinDidProjectDetails addTarget:self
                      action:@selector(buttonJoinWill)
            forControlEvents:UIControlEventTouchUpInside];
    
    [buttonJoinDidProjectDetails setBackgroundColor:[UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1]];
    [buttonJoinDidProjectDetails setTitle:@"加入他们" forState:UIControlStateNormal];
    
    buttonJoinDidProjectDetails.titleLabel.font = [UIFont boldSystemFontOfSize:14.7];
    
    buttonJoinDidProjectDetails.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    buttonJoinDidProjectDetails.contentHorizontalAlignment =UIControlContentHorizontalAlignmentCenter;
    
    buttonJoinDidProjectDetails.frame = CGRectMake(70,0,_viewButtons.frame.size.width - 140,50);
    
    [buttonJoinDidProjectDetails setTitleColor:[UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1]  forState:UIControlStateNormal];
    
    [_viewButtons addSubview:buttonJoinDidProjectDetails];
    
    _viewButtons.backgroundColor = [UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:1];
}

#pragma mark 分享视图
-(void)viewCreateShareView{
    
//    _intShareView = _iOSDeviceDetails.width/3;
//    
//    _viewShareShaow = [[UIView alloc] initWithFrame:CGRectMake(0,0 ,_iOSDeviceDetails.width,_iOSDeviceDetails.height)];
//    
//    _viewShareShaow.backgroundColor = [UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:0.5];
//
////    _viewShareShaow.layer.borderWidth = 1;
////    _viewShareShaow.layer.borderColor = [UIColor redColor].CGColor;
//
//    UIButton *buttonShareCancel = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    buttonShareCancel.frame = CGRectMake(0,0, _viewShareShaow.frame.size.width,_viewShareShaow.frame.size.height - 90);
//    [buttonShareCancel addTarget:self action:@selector(buttonShareCancel:) forControlEvents:UIControlEventTouchUpInside];
//    
////    buttonShareCancel.layer.borderColor = [UIColor redColor].CGColor;
////    buttonShareCancel.layer.borderWidth = 1;
//    
//    [_viewShareShaow addSubview:buttonShareCancel];
//
//    UIView *viewShare = [[UIView alloc] initWithFrame:CGRectMake(0,_iOSDeviceDetails.height - 90, _iOSDeviceDetails.width, 90)];
//    viewShare.backgroundColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
//    [_viewShareShaow addSubview:viewShare];
//    
//    UIImageView *imageWeChat = [[UIImageView alloc] initWithFrame:CGRectMake(_intShareView/2 - 24, 12, 48, 48)];
//    imageWeChat.clipsToBounds = YES;
//    imageWeChat.layer.cornerRadius = 24;
//    [viewShare addSubview:imageWeChat];
//    UILabel *labelWeChat =[[UILabel alloc] initWithFrame:CGRectMake(_intShareView/2  - 24, 66, 48, 12)];
//    labelWeChat.text = @"微信好友";
//    labelWeChat.textColor = [UIColor colorWithRed:199.0/255 green:199.0/255 blue:199.0/255 alpha:1];
//    labelWeChat.font = [UIFont systemFontOfSize:12];
//    labelWeChat.textAlignment = NSTextAlignmentCenter;
//    [viewShare addSubview:labelWeChat];
//    
//    UIButton *buttonWeChat = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [buttonWeChat addTarget:self action:@selector(buttonShareWeChat:) forControlEvents:UIControlEventTouchUpInside];
//    buttonWeChat.frame = CGRectMake(_intShareView/2  - 24, 12, 50, 50);
////    buttonWeChat.layer.borderWidth = 1;
//    [viewShare addSubview:buttonWeChat];
//    
//    UIImageView *imageWeChatRound = [[UIImageView alloc] initWithFrame:CGRectMake(_intShareView*2 - _intShareView/2 - 24 , 12, 48, 48)];
//    imageWeChatRound.clipsToBounds = YES;
//    imageWeChatRound.layer.cornerRadius = 24;
//    [viewShare addSubview:imageWeChatRound];
//    
//    UILabel *labelWeChatRound =[[UILabel alloc] initWithFrame:CGRectMake(_intShareView*2 - _intShareView/2 - 24 , 66, 48, 12)];
//    labelWeChatRound.text = @"朋友圈";
//    labelWeChatRound.textColor = [UIColor colorWithRed:199.0/255 green:199.0/255 blue:199.0/255 alpha:1];
//    labelWeChatRound.font = [UIFont systemFontOfSize:12];
//    labelWeChatRound.textAlignment = NSTextAlignmentCenter;
//    [viewShare addSubview:labelWeChatRound];
//    
//    UIButton *buttonWeChatRound = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [buttonWeChatRound addTarget:self action:@selector(buttonShareWeChatRound:) forControlEvents:UIControlEventTouchUpInside];
//    buttonWeChatRound.frame = CGRectMake(_intShareView*2 - _intShareView/2 - 24 , 12, 50, 50);
////    buttonWeChatRound.layer.borderWidth =1;
//    [viewShare addSubview:buttonWeChatRound];
//    
//    
//    
//    UIImageView *imageWeiBo = [[UIImageView alloc] initWithFrame:CGRectMake(_iOSDeviceDetails.width - _intShareView/2 - 24, 12, 48, 48)];
//    imageWeiBo.clipsToBounds = YES;
//    imageWeiBo.layer.cornerRadius = 24;
//    [viewShare addSubview:imageWeiBo];
//    
//    UILabel *labelWeiBo =[[UILabel alloc] initWithFrame:CGRectMake(_iOSDeviceDetails.width - _intShareView/2 - 50, 66, 100, 12)];
//    labelWeiBo.text = @"COMMING SOON";
//    labelWeiBo.textColor = [UIColor colorWithRed:199.0/255 green:199.0/255 blue:199.0/255 alpha:1];
//    labelWeiBo.font = [UIFont systemFontOfSize:12];
//    labelWeiBo.textAlignment = NSTextAlignmentCenter;
//    [viewShare addSubview:labelWeiBo];
//    
//    UIButton *buttonWeiBo = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [buttonWeiBo addTarget:self action:@selector(buttonShareWeiBo:) forControlEvents:UIControlEventTouchUpInside];
//    buttonWeiBo.frame = CGRectMake(_iOSDeviceDetails.width - _intShareView/2 - 24, 12, 50, 50);
////    buttonWeiBo.layer.borderWidth = 1;
//    [viewShare addSubview:buttonWeiBo];
//
//    
//    if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
//        imageWeChat.image = [UIImage imageNamed:@"ShareWeChatFriend@3x"];
//        imageWeChatRound.image = [UIImage imageNamed:@"ShareWeChatPreson@3x"];
////        imageWeiBo.image = [UIImage imageNamed:@"Shareweibo@3x"];
//    }else{
//        imageWeChat.image = [UIImage imageNamed:@"ShareWeChatFriend@2x"];
//        imageWeChatRound.image = [UIImage imageNamed:@"ShareWeChatPreson@2x"];
////        imageWeiBo.image = [UIImage imageNamed:@"Shareweibo@2x"];
//    }
//    
//    
//    
//    [self.view addSubview:_viewShareShaow];
//    
//    _viewShareShaow.hidden = YES;
//    _viewShareShaow.alpha = 0.0;
    
    _intShareView = _iOSDeviceDetails.width/2;
    
    _viewShareShaow = [[UIView alloc] initWithFrame:CGRectMake(0,0 ,_iOSDeviceDetails.width,_iOSDeviceDetails.height)];
    
    _viewShareShaow.backgroundColor = [UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:0.5];
    
    //    _viewShareShaow.layer.borderWidth = 1;
    //    _viewShareShaow.layer.borderColor = [UIColor redColor].CGColor;
    
    UIButton *buttonShareCancel = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    buttonShareCancel.frame = CGRectMake(0,0, _viewShareShaow.frame.size.width,_viewShareShaow.frame.size.height - 90);
    [buttonShareCancel addTarget:self action:@selector(buttonShareCancel:) forControlEvents:UIControlEventTouchUpInside];
    
    //    buttonShareCancel.layer.borderColor = [UIColor redColor].CGColor;
    //    buttonShareCancel.layer.borderWidth = 1;
    
    [_viewShareShaow addSubview:buttonShareCancel];
    
    UIView *viewShare = [[UIView alloc] initWithFrame:CGRectMake(0,_iOSDeviceDetails.height - 90, _iOSDeviceDetails.width, 90)];
    viewShare.backgroundColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
    [_viewShareShaow addSubview:viewShare];
    
    UIImageView *imageWeChat = [[UIImageView alloc] initWithFrame:CGRectMake(_intShareView/2 - 24, 12, 48, 48)];
    imageWeChat.clipsToBounds = YES;
    imageWeChat.layer.cornerRadius = 24;
    [viewShare addSubview:imageWeChat];
    UILabel *labelWeChat =[[UILabel alloc] initWithFrame:CGRectMake(_intShareView/2  - 24, 66, 48, 12)];
    labelWeChat.text = @"微信好友";
    labelWeChat.textColor = [UIColor colorWithRed:199.0/255 green:199.0/255 blue:199.0/255 alpha:1];
    labelWeChat.font = [UIFont systemFontOfSize:12];
    labelWeChat.textAlignment = NSTextAlignmentCenter;
    [viewShare addSubview:labelWeChat];
    
    UIButton *buttonWeChat = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonWeChat addTarget:self action:@selector(buttonShareWeChat:) forControlEvents:UIControlEventTouchUpInside];
    buttonWeChat.frame = CGRectMake(_intShareView/2  - 24, 12, 50, 50);
    //    buttonWeChat.layer.borderWidth = 1;
    [viewShare addSubview:buttonWeChat];
    
    UIImageView *imageWeChatRound = [[UIImageView alloc] initWithFrame:CGRectMake(_intShareView*2 - _intShareView/2 - 24 , 12, 48, 48)];
    imageWeChatRound.clipsToBounds = YES;
    imageWeChatRound.layer.cornerRadius = 24;
    [viewShare addSubview:imageWeChatRound];
    
    UILabel *labelWeChatRound =[[UILabel alloc] initWithFrame:CGRectMake(_intShareView*2 - _intShareView/2 - 24 , 66, 48, 12)];
    labelWeChatRound.text = @"朋友圈";
    labelWeChatRound.textColor = [UIColor colorWithRed:199.0/255 green:199.0/255 blue:199.0/255 alpha:1];
    labelWeChatRound.font = [UIFont systemFontOfSize:12];
    labelWeChatRound.textAlignment = NSTextAlignmentCenter;
    [viewShare addSubview:labelWeChatRound];
    
    UIButton *buttonWeChatRound = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonWeChatRound addTarget:self action:@selector(buttonShareWeChatRound:) forControlEvents:UIControlEventTouchUpInside];
    buttonWeChatRound.frame = CGRectMake(_intShareView*2 - _intShareView/2 - 24 , 12, 50, 50);
    //    buttonWeChatRound.layer.borderWidth =1;
    [viewShare addSubview:buttonWeChatRound];
    
    
//    
//    UIImageView *imageWeiBo = [[UIImageView alloc] initWithFrame:CGRectMake(_iOSDeviceDetails.width - _intShareView/2 - 24, 12, 48, 48)];
//    imageWeiBo.clipsToBounds = YES;
//    imageWeiBo.layer.cornerRadius = 24;
//    [viewShare addSubview:imageWeiBo];
//    
//    UILabel *labelWeiBo =[[UILabel alloc] initWithFrame:CGRectMake(_iOSDeviceDetails.width - _intShareView/2 - 50, 66, 100, 12)];
//    labelWeiBo.text = @"COMMING SOON";
//    labelWeiBo.textColor = [UIColor colorWithRed:199.0/255 green:199.0/255 blue:199.0/255 alpha:1];
//    labelWeiBo.font = [UIFont systemFontOfSize:12];
//    labelWeiBo.textAlignment = NSTextAlignmentCenter;
//    [viewShare addSubview:labelWeiBo];
//    
//    UIButton *buttonWeiBo = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [buttonWeiBo addTarget:self action:@selector(buttonShareWeiBo:) forControlEvents:UIControlEventTouchUpInside];
//    buttonWeiBo.frame = CGRectMake(_iOSDeviceDetails.width - _intShareView/2 - 24, 12, 50, 50);
//    //    buttonWeiBo.layer.borderWidth = 1;
//    [viewShare addSubview:buttonWeiBo];
    
    
    if([_iOSSizeDetails isEqualToString:@"iPhone6Plus"]){
        imageWeChat.image = [UIImage imageNamed:@"ShareWeChatFriend@3x"];
        imageWeChatRound.image = [UIImage imageNamed:@"ShareWeChatPreson@3x"];
        //        imageWeiBo.image = [UIImage imageNamed:@"Shareweibo@3x"];
    }else{
        imageWeChat.image = [UIImage imageNamed:@"ShareWeChatFriend@2x"];
        imageWeChatRound.image = [UIImage imageNamed:@"ShareWeChatPreson@2x"];
        //        imageWeiBo.image = [UIImage imageNamed:@"Shareweibo@2x"];
    }
    
    
    
    [self.view addSubview:_viewShareShaow];
    
    _viewShareShaow.hidden = YES;
    _viewShareShaow.alpha = 0.0;
}

#pragma mark 隐藏分享视图
-(void)buttonShareCancel:(UIButton*)sender{
    
    [UIView animateWithDuration:0.3f animations:^{

        _viewShareShaow.alpha = 0.0;
    } completion:^(BOOL finished) {
        _viewShareShaow.hidden = YES;
    }];

    
}

#pragma mark 分享
-(void)buttonShareWeChat:(UIButton*)sender{
    
    
    //注意：分享到微信好友、微信朋友圈、微信收藏、QQ空间、QQ好友、来往好友、来往朋友圈、易信好友、易信朋友圈、Facebook、Twitter、Instagram等平台需要参考各自的集成方法
//    [[UMSocialControllerService defaultControllerService] setShareText:_strShareText shareImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_strShareImage]]] socialUIDelegate:self];
    //设置分享内容和回调对象
//    [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession].snsClickHandler(self,[UMSocialControllerService defaultControllerService],YES);
    
    [UMSocialData defaultData].extConfig.wechatSessionData.title = _strShareTitle;
    [UMSocialData defaultData].extConfig.wechatSessionData.url = [NSString stringWithFormat:@"http://www.huoban.io/project_detail.html?p_id=%@",_strProjectID];
    
    //使用UMShareToWechatSession,UMShareToWechatTimeline,UMShareToWechatFavorite分别代表微信好友、微信朋友圈、微信收藏
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatSession] content:_strShareText image:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_strShareImage]]] location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
            NSLog(@"分享成功！");
        }
    }];
}

-(void)buttonShareWeiBo:(UIButton*)sender{
 
    
    [[ToolClass sharedInstance] showAlert:@"微博分享稍后上线~"];
    
    
}
-(void)buttonShareWeChatRound:(UIButton*)sender{
    
    //注意：分享到微信好友、微信朋友圈、微信收藏、QQ空间、QQ好友、来往好友、来往朋友圈、易信好友、易信朋友圈、Facebook、Twitter、Instagram等平台需要参考各自的集成方法
//    [[UMSocialControllerService defaultControllerService] setShareText:_strShareText shareImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_strShareImage]]] socialUIDelegate:self];
    //设置分享内容和回调对象
//    [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession].snsClickHandler(self,[UMSocialControllerService defaultControllerService],YES);
    
    [UMSocialData defaultData].extConfig.wechatSessionData.title = _strShareTitle;
    [UMSocialData defaultData].extConfig.wechatSessionData.url = [NSString stringWithFormat:@"http://www.huoban.io/project_detail.html?p_id=%@",_strProjectID];
    
    //使用UMShareToWechatSession,UMShareToWechatTimeline,UMShareToWechatFavorite分别代表微信好友、微信朋友圈、微信收藏
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatTimeline] content:_strShareText image:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_strShareImage]]] location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
            NSLog(@"分享成功！");
        }
    }];
}
@end
