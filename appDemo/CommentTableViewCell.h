//
//  CommentTableViewCell.h
//  huoban
//
//  Created by Lyc on 15/12/23.
//  Copyright © 2015年 lyc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageViewPreson;

@property (weak, nonatomic) IBOutlet UIButton *buttonPreson;
@property (weak, nonatomic) IBOutlet UILabel *labelBy;
@property (weak, nonatomic) IBOutlet UILabel *labelCommentContents;

@property (weak, nonatomic) IBOutlet UIButton *buttonAnswer;
@end
