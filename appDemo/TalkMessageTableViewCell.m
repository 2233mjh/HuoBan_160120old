//
//  TalkMessageTableViewCell.m
//  huoban
//
//  Created by Lyc on 15/12/23.
//  Copyright © 2015年 lyc. All rights reserved.
//

#import "TalkMessageTableViewCell.h"
#import "DataModel.h"


@implementation TalkMessageTableViewCell

DataModel *_dataModelTalkMessageCell;



- (void)awakeFromNib {
    // Initialization code

    _dataModelTalkMessageCell = [[DataModel alloc] init];
    
    NSString *iOSDeivce = _dataModelTalkMessageCell.userInfomation.iOSDeviceSize;
    
    
    self.imageViewPreson.clipsToBounds = YES;
    self.imageViewPreson.layer.cornerRadius = 22;
    
    self.imageViewContents.clipsToBounds = YES;
    self.imageViewContents.layer.cornerRadius = 8;
    
    
    if([iOSDeivce isEqualToString:@"iPhone6Plus"]){
        
        self.imageViewDelete.image = [UIImage imageNamed:@"DeleteDynamic@3x"];
        
    }else{
        self.imageViewDelete.image = [UIImage imageNamed:@"DeleteDynamic@2x"];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
