//
//  ProjectAllMessageTableViewController.h
//  appDemo
//
//  Created by 刘雨辰 on 15/9/8.
//  Copyright (c) 2015年 lyc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CreateDynamicViewController.h"
#import "SendProjectListTableViewController.h"
#import "PaySuccessViewController.h"
#import "ProjectDynamic.h"
#import "HttpClassSelf.h"
#import "ToolClass.h"
#import "MJRefresh.h"
#import "ProjectModel.h"
#import "DataModel.h"
#import "ImageSameWidth.h"
#import "ProjectDetailsViewController.h"
#import "OtherUserInfoViewController.h"
#import "DataModelCellProjectDetailDynamic.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "CreateDynamicNavigationController.h"
#import "OneProjectDynamicTableViewController.h"
#import "ProjectCommentTableViewController.h"


@interface ProjectDynamicTableViewController : UITableViewController<CreateDynamicDelegate,SendProjectListDelegate,UIAlertViewDelegate,paySuccessControllerDelegate>

- (IBAction)buttonRemind:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *labelNaLeftTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonRemind;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewRemid;
@property (weak, nonatomic) IBOutlet UIImageView *imageRedPoint;

- (IBAction)buttonPresonMessageClick:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *buttonAddBar;

- (IBAction)buttonAddBar:(id)sender;

-(void)sendDynamicPush:(NSString*)strProjectID title:(NSString*)strTitle;

@property(nonatomic)NSInteger isRemindMessage;
@end
