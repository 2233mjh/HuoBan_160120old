//
//  CommentTableViewController.m
//  huoban
//
//  Created by Lyc on 15/12/23.
//  Copyright © 2015年 lyc. All rights reserved.
//

#import "ProjectCommentTableViewController.h"
#import "UIImageView+WebCache.h"
#import "OtherUserInfoViewController.h"
#import "DynamicUpListTableViewController.h"
#import "UserSelfMessageViewController.h"

@interface ProjectCommentTableViewController ()

@end

CommentSubmitBar *_commentBar;
NSMutableDictionary *_dicTalkMessage;
NSMutableArray *_aryCommentModel;
NSDateFormatter *_dataForMatterComment;
NSMutableParagraphStyle * _paragraphSummarizeStyleComment;
HttpClassSelf *_httpComment;
NSString *_iOSDevieceComment;
CGSize _iOSBoundComment;
NSInteger _imageContenHeight;
NSInteger _imageContenWidth;
NSInteger _floatLabelContentHeight;
NSInteger _floatImageViewHeight;
NSInteger _floatLabelCommentHeiht;
NSArray *_aryImageCommentSub;
NSCharacterSet * _subImageCommentStr;
NSTimeZone *_zoneComment;
NSMutableArray *_aryUserList;
NSInteger _keyBoardHeight;
DataModel *_dataModelComment;
BOOL _keyboardAppear;
BOOL _loveBool;

@implementation ProjectCommentTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //修改导航栏偏色
    self.navigationController.navigationBar.translucent = NO;
    
    //上放导航栏
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:1]];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.7],NSForegroundColorAttributeName : [UIColor colorWithRed:51.0/255 green:163.0/255 blue:219.0/255 alpha:1]}];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:51.0/255 green:163.0/255 blue:219.0/255 alpha:1];
    
        _dataModelComment = [[DataModel alloc] init];
    
        _iOSDevieceComment = _dataModelComment.userInfomation.iOSDeviceSize;
    
    if([_iOSDevieceComment isEqualToString:@"iPhone6Plus"]){
        
        self.imageClose.image = [UIImage imageNamed:@"back_blue_3x"];
        
    }else{

        
        self.imageClose.image = [UIImage imageNamed:@"back_blue_2x"];
        
    }
    //注册键盘通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillCommentShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillCommentHide:) name:UIKeyboardWillHideNotification object:nil];
    

    self.tableView.backgroundColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
    
    _iOSBoundComment = [UIScreen mainScreen].bounds.size;

    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _paragraphSummarizeStyleComment = [[NSMutableParagraphStyle alloc] init];
    
    [_paragraphSummarizeStyleComment setMinimumLineHeight:24];
    
    _aryImageCommentSub = [[NSMutableArray alloc] init];
    
    _dataForMatterComment = [[NSDateFormatter alloc] init];
    
    _aryUserList = [[NSMutableArray alloc] init];
    
    _subImageCommentStr = [NSCharacterSet characterSetWithCharactersInString:@"!*"];
    
    _zoneComment = [NSTimeZone systemTimeZone];
    
    
    _commentBar = [[CommentSubmitBar alloc]initWithFrame:CGRectMake(0,_iOSBoundComment.height - 114,_iOSBoundComment.width,50)];
    
//        _commentBar = [[CommentSubmitBar alloc]initWithFrame:CGRectMake(0,100,_iOSBoundComment.width,50)];
    
    _commentBar.backgroundColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];

    _commentBar.delegate = self;
    _commentBar.clearInputWhenSend = YES;
    _commentBar.resignFirstResponderWhenSend = YES;
//    _commentBar.layer.borderWidth = 20;
    
    [self.tableView addSubview:_commentBar];
    
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,100,100,50)]
//    ;
//        [self.tableView addSubview:view];

    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0,62,0);
    
//    self.buttonTEST.imageView.image = [UIImage imageNamed:@"back_yellow_2x"];
    
    [self.buttonTEST setImage:[UIImage imageNamed:@"back_yellow_2x"] forState:UIControlStateNormal];
    
    
    self.buttonTEST.layer.borderWidth = 1;
    self.buttonTEST.layer.borderColor = [UIColor yellowColor].CGColor;
//    self.buttonTEST.imageEdgeInsets = UIEdgeInsetsMake(10,0,0,14);
    self.buttonTEST.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [self.buttonTEST setTitle:@"测试" forState:UIControlStateNormal];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if(_keyboardAppear == NO){
        
        _commentBar.frame = CGRectMake(_commentBar.frame.origin.x,scrollView.contentOffset.y + _iOSBoundComment.height - 114, _commentBar.frame.size.width,_commentBar.frame.size.height);
        
    }else{

        _commentBar.frame = CGRectMake(_commentBar.frame.origin.x,scrollView.contentOffset.y + _keyBoardHeight - 114, _commentBar.frame.size.width,_commentBar.frame.size.height);

    }
    
    
}

//-(void)keyBoardShow:(CommentSubmitBar *)subBar{
//    
//    CGRect _keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
//
//    _commentBar.frame = CGRectMake(_commentBar.frame.origin.x,scrollView.contentOffset.y + _iOSBoundComment.height - 50, _commentBar.frame.size.width,_commentBar.frame.size.height);
//    
//    
//}

-(void)keyboardWillCommentShow:(NSNotification*)aNotification{
    
    
    CGRect _keyboardRect = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    _keyBoardHeight = _keyboardRect.origin.y;
    
    _commentBar.frame = CGRectMake(_commentBar.frame.origin.x,self.tableView.contentOffset.y + _keyBoardHeight - 114, _commentBar.frame.size.width,_commentBar.frame.size.height);
    
    _keyboardAppear = YES;
    
}

-(void)keyboardWillCommentHide:(NSNotification*)aNotifacation{
    
    _commentBar.frame = CGRectMake(_commentBar.frame.origin.x,self.tableView.contentOffset.y + _iOSBoundComment.height - 114, _commentBar.frame.size.width,_commentBar.frame.size.height);

    _keyboardAppear = NO;
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];

    
    NSLog(@"接受FEED%@",    self.strFeedID);
    

    

    _aryCommentModel = [[NSMutableArray alloc] init];
    
    _httpComment = [[HttpClassSelf alloc] init];
    
    if(self.isKeyBoardAppar == YES){
        
        [_commentBar.textField becomeFirstResponder];
        
    }else{
        [_commentBar.textField resignFirstResponder];
    }
    

    
    [self reloadDataHttp];
}

-(void)reloadDataHttp{
    
    DataModel *dataModelCommentAll = [[DataModel alloc] init];
    
    [_httpComment getOneDynamic:self.strFeedID token:dataModelCommentAll.userInfomation.tokenID CallBackYES:^(MKNetworkOperation *operatioin){
        
        NSData *data = [operatioin responseData];
        
        NSMutableDictionary *dicData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        
        if([dicData[@"status"] isEqualToString:@"success"]){
            
            _dicTalkMessage = [[NSMutableDictionary alloc] init];
            
            _dicTalkMessage = dicData[@"data"];
            
            
            [_httpComment projectTalkSetFeed:self.strFeedID numPage:0 numNum:500 token:dataModelCommentAll.userInfomation.tokenID CallBackyes:^(MKNetworkOperation *operatioin){
                
                NSData *dataComment = [operatioin responseData];
                
                NSMutableDictionary *dicCommentDown = [NSJSONSerialization JSONObjectWithData:dataComment options:NSJSONReadingAllowFragments error:nil];
                
                if([dicCommentDown[@"status"] isEqualToString:@"success"]){
                    
                    [_aryCommentModel removeAllObjects];
                    
                    for(NSMutableDictionary *dicCommentList in dicCommentDown[@"data"]){
                        
                        [_aryCommentModel addObject:dicCommentList];
                    }
                    [self.tableView reloadData];
                    
                }else{
                    [[ToolClass sharedInstance] showAlert:@"刷新失败，服务器错误"];
                }
            }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
                
                [[ToolClass sharedInstance] showAlert:@"刷新失败，服务器错误"];
            }];
        }else{
            [[ToolClass sharedInstance] showAlert:@"刷新失败，服务器错误"];
        }
    }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
        
        [[ToolClass sharedInstance] showAlert:@"刷新失败，服务器错误"];
        
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if(_dicTalkMessage != nil){
        
        if([_aryCommentModel count] == 0){
        
                return 1;
        }else{
    
                return [_aryCommentModel count] + 1;
        }
    
    }else{
                return  0 ;
    }
}


-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    self.isKeyBoardAppar = NO;
    
    [_commentBar.textField resignFirstResponder];
    
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if(indexPath.row == 0){


        TalkMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"talkMessage"];
        
        if (cell == nil)
        {
            cell = [[TalkMessageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"talkMessage"];
        }
        
        if(_dicTalkMessage != nil){
            

        
        NSMutableDictionary *dicUser = _dicTalkMessage[@"user"];
        
        [cell.imageViewPreson sd_setImageWithURL:[NSURL URLWithString:dicUser[@"image"] ] placeholderImage:nil];
        
        [cell.buttonPreson addTarget:self action:@selector(buttonPresonODClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.buttonLove addTarget:self action:@selector(buttonLoveODClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.buttonDelete addTarget:self action:@selector(buttonDeleteODClick:) forControlEvents:UIControlEventTouchUpInside];
            [cell.buttonUpPresonList addTarget:self action:@selector(buttonUpPresonODCList:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.labelBy.text = dicUser[@"name"];
            
            
            if([dicUser[@"_id"] isEqualToString:_dataModelComment.userInfomation.UserID]){
                
                cell.buttonDelete.hidden = NO;
                cell.imageViewDelete.hidden = NO;
                cell.buttonLoveRight.constant = 36;
                
                
            }else{
                cell.buttonDelete.hidden = YES;
                cell.imageViewDelete.hidden = YES;
                cell.buttonLoveRight.constant = 0;
            }
            
            
        
        if([_dicTalkMessage[@"isCreator"] longValue] == 1){
            cell.labelCreator.text = @"发起人";
            
        }else{
            cell.labelCreator.text = @"";
        }
        [cell.labelCreator sizeToFit];
        
        NSMutableDictionary *dicProject = _dicTalkMessage[@"project"];
        
        cell.labelProjectName.text = [NSString stringWithFormat:@"@ %@",dicProject[@"title"]];
        
        
        //contents
        NSMutableAttributedString  *strContents =[[NSMutableAttributedString alloc]initWithString: _dicTalkMessage[@"message"]];
        
        [strContents addAttribute:NSParagraphStyleAttributeName value:_paragraphSummarizeStyleComment range:NSMakeRange(0, [_dicTalkMessage[@"message"] length])];
        
        cell.labelContents.attributedText = strContents;
        [cell.labelContents sizeToFit];
            _floatLabelContentHeight = cell.labelContents.frame.size.height;
        
            NSLog(@"%@",dicProject[@"image"]);
        if([_dicTalkMessage[@"image"] isEqualToString:@""] || _dicTalkMessage[@"image"] == nil){
            
            cell.imageViewContents.image = nil;
            cell.imageViewContentsHeight.constant = 1;
            
            
            
            _floatImageViewHeight = 1;
        }else{
        
            //上线
        _aryImageCommentSub = [_dicTalkMessage[@"image"] componentsSeparatedByCharactersInSet:_subImageCommentStr];
            
            if(160/([_aryImageCommentSub[2] floatValue]/[_aryImageCommentSub[1] floatValue]) > _iOSBoundComment.width - 74){
                
                cell.imageViewContentsHeight.constant =  (_iOSBoundComment.width - 74)/([_aryImageCommentSub[1] floatValue]/[_aryImageCommentSub[2] floatValue]);
                cell.imageViewContentsWidth.constant = _iOSBoundComment.width - 74;
                
                [cell.imageViewContents sd_setImageWithURL:[NSURL URLWithString:_dicTalkMessage[@"image"]] placeholderImage:nil];
                _floatImageViewHeight = cell.imageViewContentsHeight.constant;
            }else{
                cell.imageViewContentsWidth.constant = 160/([_aryImageCommentSub[2] floatValue]/[_aryImageCommentSub[1] floatValue]);
                cell.imageViewContentsHeight.constant = 160;
                _floatImageViewHeight = 160;
                
                [cell.imageViewContents sd_setImageWithURL:[NSURL URLWithString:_dicTalkMessage[@"image"]] placeholderImage:nil];
            }
        
//            [cell.imageViewContents sd_setImageWithURL:[NSURL URLWithString:_dicTalkMessage[@"image"] ] placeholderImage:nil];
//            
//            cell.imageViewContentsWidth.constant = 160/([_aryImageCommentSub[2] floatValue]/[_aryImageCommentSub[1] floatValue]);
//            
//            cell.imageViewContentsHeight.constant = 160;
//            
//            _imageContenHeight = 160;
//            
//            _floatImageViewHeight = 160;
            
        }
        
        if([_dicTalkMessage[@"isUp"] longValue] == 1){
            
            if([_iOSDevieceComment isEqualToString:@"iPhone6Plus"]){
                
                cell.imageViewLove.image = [UIImage imageNamed:@"women_like_blue_3x"];
                
            }else{
                cell.imageViewLove.image = [UIImage imageNamed:@"women_like_blue_3x"];
            }
            
            _loveBool = YES;
        }else{
            if([_iOSDevieceComment isEqualToString:@"iPhone6Plus"]){
                
                cell.imageViewLove.image = [UIImage imageNamed:@"women_like_grey_3x"];
                
            }else{
                cell.imageViewLove.image = [UIImage imageNamed:@"women_like_grey_3x"];
            }
            
            _loveBool = NO;
            
        }

        NSDate *dateProject = [_dataForMatterComment dateFromString:[_dicTalkMessage[@"time"] substringToIndex:19]];
        
        NSInteger interval = [_zoneComment secondsFromGMTForDate:dateProject];
        
        NSDate *dateComment = [dateProject dateByAddingTimeInterval: interval];
    
        
        NSDate *dataNow = [NSDate date];
        
        NSInteger intervalNow = [_zoneComment secondsFromGMTForDate:dataNow];
        
        NSDate *dateNow = [dataNow dateByAddingTimeInterval: intervalNow];
        
        if([dateNow timeIntervalSinceDate:dateComment]/60 <= 1){
            
            cell.labelTime.text = @"刚刚";
            
        }else if([dateNow timeIntervalSinceDate:dateComment]/60 < 60 || [dateNow timeIntervalSinceDate:dateComment]/60 > 1){
            
            cell.labelTime.text = [NSString stringWithFormat:@"%@分钟前",[[NSString stringWithFormat:@"%f",[dateNow timeIntervalSinceDate:dateComment]/60] componentsSeparatedByString:@"."][0] ];
            
        }else if([dateNow timeIntervalSinceDate:dateComment]/3600 < 24 && [dateNow timeIntervalSinceDate:dateComment]/3600 >= 1){
            
            cell.labelTime.text = [NSString stringWithFormat:@"%@小时前",[[NSString stringWithFormat:@"%f小时前",[dateNow timeIntervalSinceDate:dateComment]/3600] componentsSeparatedByString:@"."][0] ];
        
            
        }else if([dateNow timeIntervalSinceDate:dateComment]/86400 < 30 && [dateNow timeIntervalSinceDate:dateComment]/86400 >= 1){
            cell.labelTime.text =[NSString stringWithFormat:@"%@天前",[[NSString stringWithFormat:@"%f",[dateNow timeIntervalSinceDate:dateComment]/86400 ] componentsSeparatedByString:@"."][0]];
        }else if([dateNow timeIntervalSinceDate:dateComment]/2592000 >=1){
            cell.labelTime.text =[NSString stringWithFormat:@"%@个月前",[[NSString stringWithFormat:@"%f",[dateNow timeIntervalSinceDate:dateComment]/2592000 ] componentsSeparatedByString:@"."][0]];
        }
        
        NSMutableDictionary *dicCommentCount = _dicTalkMessage[@"up"];
        
            [_aryUserList removeAllObjects];

            for(NSMutableDictionary *dicUserList in dicCommentCount[@"users"]){
                
                [_aryUserList addObject:dicUserList];
                
            }
            
    
        if([dicCommentCount[@"count"] longValue]> 0){
            
            cell.labelUpPresonList.text = [NSString stringWithFormat:@"%@ 等%li个火伴喜欢",_aryUserList[0][@"name"],[dicCommentCount[@"count"] longValue]];
         
            }
        }
        return  cell;
    }else{
        
        
        CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentMessage"];
        
        if (cell == nil)
        {
            cell = [[CommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CommentMessage"];
        }
        
        [cell.buttonAnswer addTarget:self action:@selector(buttonAnswerCommentClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.buttonAnswer.tag = indexPath.row -1;
        
        
        NSMutableDictionary *dicCommentContent = _aryCommentModel[indexPath.row - 1];
        
        [cell.buttonPreson addTarget:self action:@selector(buttonPresonCommentClick:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.buttonPreson.tag = indexPath.row -1;
        
        NSMutableDictionary *dicUser = dicCommentContent[@"user"];
        
        cell.labelBy.text = dicUser[@"name"];
        
        [cell.imageViewPreson sd_setImageWithURL:[NSURL URLWithString:dicUser[@"image"]] placeholderImage:nil];
        
        //contents
        NSMutableAttributedString  *strComment =[[NSMutableAttributedString alloc]initWithString: dicCommentContent[@"message"]];
        
        [strComment addAttribute:NSParagraphStyleAttributeName value:_paragraphSummarizeStyleComment range:NSMakeRange(0, [dicCommentContent[@"message"] length])];
        
//        cell.labelContents.attributedText = strComment;
//        [cell.labelContents sizeToFit];
       
        cell.labelCommentContents.attributedText = strComment;
        
//        cell.labelCommentContents.layer.borderWidth =1;
        
        [cell.labelCommentContents sizeToFit];
        
        _floatLabelCommentHeiht = cell.labelCommentContents.frame.size.height;

    
        return cell;
    }
    

}

-(void)buttonAnswerCommentClick:(UIButton*)sender{
    
    NSMutableDictionary *dicCommentContent = _aryCommentModel[sender.tag];
    
    NSMutableDictionary *dicUser = dicCommentContent[@"user"];
    
    
    [_commentBar answerBecomeKeyBoardByName:dicUser[@"name"] CommentID:dicCommentContent[@"_id"]];
    
}


-(void)buttonUpPresonODCList:(UIButton*)sender{
    
    [self performSegueWithIdentifier:@"ShowUpPresonListSegue" sender:_aryUserList];
    
}
-(void)commentSubmitBar:(CommentSubmitBar *)answerBar ansert:(UIButton *)sendBtb withAnsertString:(NSString *)str commentID:(NSString *)commentID{
    
    
    
    [_httpComment answerComment:commentID token:_dataModelComment.userInfomation.tokenID answerID:@"" message:str CallBackYES:^(MKNetworkOperation *operatioin){
        
        NSData *data = [operatioin responseData];
        
        NSMutableDictionary *dicData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        
        if([dicData[@"status"] isEqualToString:@"success"]){
         
            
            [[ToolClass sharedInstance] showAlert:@"发布评论成功"];
            
            [self reloadDataHttp];
        }else{
                [[ToolClass sharedInstance] showAlert:@"发布评论失败,服务器错误"];
        }
        
    }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
                [[ToolClass sharedInstance] showAlert:@"发布评论失败,服务器错误"];
        
        
    }];
    
}

-(void)inputBar:(CommentSubmitBar *)inputBar sendBtnPress:(UIButton *)sendBtn withInputString:(NSString *)str{
    
    
    
    if(![str isEqualToString:@""]){
        
        [_httpComment commentProjectselDynamicSetFeed:_dicTalkMessage[@"_id"] strToken:_dataModelComment.userInfomation.tokenID message:str CallBackYES:^(MKNetworkOperation *operatioin){
            
            NSData *data = [operatioin responseData];
            NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            
            NSLog(@"%@",dic);
            if([dic[@"status"] isEqualToString:@"success"]){
                
//                _strSendContents = str;
                
                [[ToolClass sharedInstance] showAlert:@"发布评论成功"];
                
                [self reloadDataHttp];
            }else{
                
                [[ToolClass sharedInstance] showAlert:@"发布评论失败,服务器错误"];
            }
            
        }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
            [[ToolClass sharedInstance] showAlert:@"发布评论失败,请检查网络"];
        }];
    }else{
        
        [[ToolClass sharedInstance] showAlert:@"评论不可为空~"];
        
    }
    
    
}

-(void)buttonDeleteODClick:(UIButton*)sender{
    
    
    
    UIAlertView *alertDelete = [[UIAlertView alloc] initWithTitle:@"确定要删除吗？" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    
    //    [alertView dismissWithClickedButtonIndex:0 animated:nil];
    
    [alertDelete show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 1){
        
        DataModel *dataModelDeleteDynamic = [[DataModel alloc] init];
        
        [_httpComment deleteProjectDynamic:self.strFeedID token:dataModelDeleteDynamic.userInfomation.tokenID CallBackYES:^(MKNetworkOperation *operatioin){
            
            NSData *data = [operatioin responseData];
            
            NSMutableDictionary *dicDelete = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            
            if([dicDelete[@"status"] isEqualToString:@"success"]){
                
                [[ToolClass sharedInstance] showAlert:@"删除动态成功"];
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
            }else{
                
                [[ToolClass sharedInstance] showAlert:@"删除动态失败，服务器错误"];
                
            }
            
        }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
            
            [[ToolClass sharedInstance] showAlert:@"删除动态失败，请检查网络"];
        }];
   
    }
    
}

-(void)buttonPresonCommentClick:(UIButton*)sender{
    
    NSMutableDictionary *dicPresonMessage = _aryCommentModel[sender.tag];
    
    NSMutableDictionary *dicAnswer = dicPresonMessage[@"user"];
    
    UserSelfMessageViewController * userInfoVC = [[UserSelfMessageViewController alloc]initForOtherUserWithUserID:dicAnswer[@"_id"]];
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController pushViewController:userInfoVC animated:YES];

//    [self performSegueWithIdentifier:@"otherUserSegue" sender:dicAnswer];
#warning segue
    
}

-(void)buttonPresonODClick:(UIButton*)sender{
    

//    NSMutableDictionary *dic = _aryProjectDynamic[sender.tag];
    
//    NSMutableDictionary *dicUserMessage = dic[@"user"];
    
    UserSelfMessageViewController * userInfoVC = [[UserSelfMessageViewController alloc]initForOtherUserWithUserID:_dicTalkMessage[@"user"][@"_id"]];
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController pushViewController:userInfoVC animated:YES];

#warning segue
//    [self performSegueWithIdentifier:@"otherUserSegue" sender:_dicTalkMessage[@"user"]];
    
    
}

-(void)buttonLoveODClick:(UIButton*)sender{

    NSIndexPath *indexPathLove = [NSIndexPath indexPathForRow:0 inSection:0];
    
    TalkMessageTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPathLove];

    _loveBool = !_loveBool;
    
//    if([_dicTalkMessage[@"isUp"] longValue] == 0){
    
    if(_loveBool == YES){
    
        
        [_httpComment loveProjectselfSetFeed:self.strFeedID up:1 token:_dataModelComment.userInfomation.tokenID CallBackYES:^(MKNetworkOperation *operatioin){
            
            NSData *data = [operatioin responseData];
            
            NSMutableDictionary *dicData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            
            if([dicData[@"status"] isEqualToString:@"success"]){
                
                
                if([_iOSDevieceComment isEqualToString:@"iPhone6Plus"]){
                    //[sender setImage:[UIImage imageNamed:@"loveYES15blue@3x"] forState:UIControlStateNormal];
                    
                    cell.imageViewLove.image = [UIImage imageNamed:@"women_like_blue_3x"];
                    
                }else{

                    
                    cell.imageViewLove.image = [UIImage imageNamed:@"women_like_blue_2x"];
                    //[cell.imageLoveIM setImage:[UIImage imageNamed:@"loveYES15Blue@2x"]];
                }
                _loveBool = YES;
                
//                cell.labelLove.text = [NSString stringWithFormat:@"%lld个喜欢，",[[cell.labelLove.text substringToIndex:[cell.labelLove.text length]-3] longLongValue] + 1];
                
            }else{
                [[ToolClass sharedInstance]showAlert:@"'喜欢'失败，服务器错误"];
            }
        }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
            [[ToolClass sharedInstance]showAlert:@"'喜欢'失败，请检查网络"];
        }];

    
    }else{
        
        [_httpComment loveProjectselfSetFeed:self.strFeedID up:0 token:_dataModelComment.userInfomation.tokenID CallBackYES:^(MKNetworkOperation *operatioin){
            
            NSData *data = [operatioin responseData];
            
            NSMutableDictionary *dicData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            
            if([dicData[@"status"] isEqualToString:@"success"]){
                
                
                if([_iOSDevieceComment isEqualToString:@"iPhone6Plus"]){

                    cell.imageViewLove.image = [UIImage imageNamed:@"women_like_grey_3x"];
                    
                }else{
                    
                    
                    cell.imageViewLove.image = [UIImage imageNamed:@"women_like_grey_2x"];
                }
                
                _loveBool = NO;
                
            }else{
                [[ToolClass sharedInstance]showAlert:@"'喜欢'失败，服务器错误"];
            }
        }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
            [[ToolClass sharedInstance]showAlert:@"'喜欢'失败，请检查网络"];
        }];
    }
    
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"otherUserSegue"]){
        
        UINavigationController *naVC = segue.destinationViewController;
        
        OtherUserInfoViewController *controller = (OtherUserInfoViewController*)naVC.topViewController;
        
        controller.dicOtherUser = sender;
        
        
        
    }else if([segue.identifier isEqualToString:@"ShowUpPresonListSegue"]){
        
        UINavigationController *naVC = segue.destinationViewController;
        
        DynamicUpListTableViewController *controller = (DynamicUpListTableViewController*)naVC.topViewController;
        
        controller.aryUpList = sender;
        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        return _floatImageViewHeight + _floatLabelContentHeight + 122;
    }else{
        return _floatLabelCommentHeiht + 65;
    }
}
- (IBAction)buttonCancel:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    if(indexPath.row == 0){
    
        [_commentBar.textField resignFirstResponder];
        
//    }

}
//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 40;
//}

@end
