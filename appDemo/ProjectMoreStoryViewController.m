//
//  ProjectMoreStoryViewController.m
//  appDemo
//
//  Created by 刘雨辰 on 15/9/15.
//  Copyright (c) 2015年 lyc. All rights reserved.
//

#import "ProjectMoreStoryViewController.h"
#import "DataModel.h"
#import "JoinProjectTableViewController.h"
#import "ToolClass.h"
#import "LoginViewController.h"
#import "UMSocialData.h"
#import "UMSocialControllerService.h"
#import "UMSocial.h"
#import "UMSocialWechatHandler.h"

@interface ProjectMoreStoryViewController ()

@end

@implementation ProjectMoreStoryViewController

UIButton *_buttonCloseBlue;
DataModel *_dataModelProjectMoreStory;

NSString *_iOSSizeMoreStory;
CGSize  _iOSBoundsMoreStory;
UIView *_viewButtonsMoreStory;
UIView *_viewProjectMoreStory;
UIView *_viewShareShaowMoreStory;
NSInteger _iosBounds;

NSInteger _runTimesMoreStory;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dataModelProjectMoreStory = [[DataModel alloc] init];
    
    _iOSSizeMoreStory = _dataModelProjectMoreStory.userInfomation.iOSDeviceSize;
    
    _runTimesMoreStory  = [[NSUserDefaults standardUserDefaults] integerForKey:@"runtimes"];
    
    
    if([_iOSSizeMoreStory isEqualToString:@"iPhone6Plus"]){
        
        
        self.webViewTopNum.constant = -50;
    }else{
        self.webViewTopNum.constant = -30;
    }
    
    self.title = @"他们的故事";
 
    
  
    //上放导航栏
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:1]];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName : [UIColor colorWithRed:170.0/255 green:170.0/255 blue:170.0/255 alpha:1]}];
    
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    _iOSBoundsMoreStory = [UIScreen mainScreen].bounds.size;
    
    //登陆视图
    _viewProjectMoreStory = [[UIView alloc] initWithFrame:CGRectMake(0,0,_iOSBoundsMoreStory.width, _iOSBoundsMoreStory.height)];
    _viewProjectMoreStory.backgroundColor = [UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:0.95];
    [self.view addSubview:_viewProjectMoreStory];
    
    UIView *viewLoginBlock = [[UIView alloc]initWithFrame:CGRectMake(0,0,78,162)];
    viewLoginBlock.backgroundColor = [UIColor colorWithRed:38.0/255 green:55.0/255 blue:70.0/255 alpha:1];
    viewLoginBlock.clipsToBounds = YES;
    viewLoginBlock.layer.cornerRadius = 12;
    viewLoginBlock.center = _viewProjectMoreStory.center;
    [_viewProjectMoreStory addSubview:viewLoginBlock];

    
    UIButton *buttonLogin = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonLogin addTarget:self action:@selector(buttonMoreStoryLoginClick:) forControlEvents:UIControlEventTouchUpInside];
    buttonLogin.backgroundColor = [UIColor colorWithRed:119.0/255 green:134.0/255 blue:146.0/255 alpha:1];
    [buttonLogin setTitleColor:[UIColor colorWithRed:38.0/255 green:55.0/255 blue:70.0/255 alpha:1] forState:UIControlStateNormal];
    [buttonLogin setTitle:@"登录" forState:UIControlStateNormal];
    buttonLogin.clipsToBounds = YES;
    buttonLogin.layer.cornerRadius = 27;
    [buttonLogin setFrame:CGRectMake(12,84,54,54)];
    [viewLoginBlock addSubview:buttonLogin];
    
    UIImageView *viewClose = [[UIImageView alloc] initWithFrame:CGRectMake(31,36,16,16)];
    if([_iOSSizeMoreStory isEqualToString:@"iPhone6Plus"]){
        self.imageViewClose.image = [UIImage imageNamed:@"back_grew_3x"];
        viewClose.image = [UIImage imageNamed:@"close_grey_@3x"];
        self.imageViewShare.image = [UIImage imageNamed:@"share_grew@3x"];
    }else{
        viewClose.image = [UIImage imageNamed:@"close_grey_@2x"];
        self.imageViewClose.image = [UIImage imageNamed:@"back_grew_2x"];
        self.imageViewShare.image = [UIImage imageNamed:@"share_grew@2x"];
    }
    [viewLoginBlock addSubview:viewClose];
    
    
    UIButton *buttonCloseLogin = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonCloseLogin addTarget:self action:@selector(buttonMoreStoryLoginCloseClick:) forControlEvents:UIControlEventTouchUpInside];
    [buttonCloseLogin setFrame:CGRectMake(17,18,44,44)];
    //    buttonClose.layer.borderWidth = 1;
    [viewLoginBlock addSubview:buttonCloseLogin];
    _viewProjectMoreStory.hidden = YES;
    
    
    
    _viewButtonsMoreStory = [[UIView alloc] initWithFrame:CGRectMake(0,_iOSBoundsMoreStory.height-50, _iOSBoundsMoreStory.width, 50)];
    
    _viewButtonsMoreStory.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:_viewButtonsMoreStory];
    
    if(self.isProjectEnd == NO){
        _viewButtonsMoreStory.hidden = NO;
        self.viewWebDown.constant = 44;
    }else{
        _viewButtonsMoreStory.hidden = YES;
        self.viewWebDown.constant = 0;
    }

    
    
    UIButton *buttonJoinDid = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonJoinDid addTarget:self
                      action:@selector(buttonJoinWill)
            forControlEvents:UIControlEventTouchUpInside];
    
    [buttonJoinDid setBackgroundColor:[UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1]];

    [buttonJoinDid.titleLabel setFont:[UIFont boldSystemFontOfSize:14.7]];
    buttonJoinDid.frame = CGRectMake(0,0,_viewButtonsMoreStory.frame.size.width,50);
    [buttonJoinDid setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    buttonJoinDid.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    buttonJoinDid.contentHorizontalAlignment =UIControlContentHorizontalAlignmentCenter;
    
//    buttonJoinDid.layer.borderWidth  =1;
//    buttonJoinDid.layer.borderColor = [UIColor yellowColor].CGColor;
    
    [_viewButtonsMoreStory addSubview:buttonJoinDid];
    _viewButtonsMoreStory.backgroundColor = [UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:1];
    
//    //分享
//    UIButton *buttonShare = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [buttonShare addTarget:self action:@selector(buttonShareWillMoreStory) forControlEvents:UIControlEventTouchUpInside];
//    [buttonShare setBackgroundColor:[UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:1]];
//    buttonShare.frame = CGRectMake(_viewButtonsMoreStory.frame.size.width -70,0, 70,50);
//    [buttonShare setTitleColor:[UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1]  forState:UIControlStateNormal];
//    [_viewButtonsMoreStory addSubview:buttonShare];
//    
//    UIImageView *imageViewShare = [[UIImageView alloc] initWithFrame:CGRectMake(buttonShare.frame.size.width/2 - 22, 0, 44,44)];
//    if([_iOSSizeMoreStory isEqualToString:@"iPhone6Plus"]){
//                imageViewShare.image = [UIImage imageNamed:@"share_3x"];
//            }else{
//                imageViewShare.image = [UIImage imageNamed:@"share_2x"];
//            }
//    [buttonShare addSubview:imageViewShare];
//    

    
    if(self.isJoinProject == YES){
    [buttonJoinDid setTitle:@"支持更多" forState:UIControlStateNormal];
    }else{
    [buttonJoinDid setTitle:@"加入他们" forState:UIControlStateNormal];
    }

    
    [self.webView loadHTMLString:[NSString stringWithFormat:@"%@",self.strContents] baseURL:nil];
    
    [self viewCreateShareView];
}

//-(void)buttonLoginClick:(UIButton*)sender{
//    
//    [self performSegueWithIdentifier:@"loginSegue" sender:nil];
//}
//
//-(void)buttonLoginCloseClick:(UIButton*)sender{
//    
//    
//}
-(void)buttonMoreStoryLoginClick:(UIButton*)sender{
    
    [self performSegueWithIdentifier:@"loginSegue" sender:nil];
}
-(void)buttonMoreStoryLoginCloseClick:(UIButton*)sender{
    
    _viewProjectMoreStory.hidden = YES;
    _viewButtonsMoreStory.hidden = NO;
    
}

-(void)segmentedClick:(id)sender{
    UISegmentedControl *control = (UISegmentedControl*)sender;
    
    
    if(control.selectedSegmentIndex == 0){
        [self dismissViewControllerAnimated:YES completion:nil];
    }else if(control.selectedSegmentIndex == 1){
        
        [self performSegueWithIdentifier:@"JoinSegue" sender:self.aryChouMoreStory];
        
    }else if(control.selectedSegmentIndex == 2){
        
        
    }
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"JoinSegue"]){
        
        UINavigationController *naVC = segue.destinationViewController;
        
        JoinProjectTableViewController *controller = (JoinProjectTableViewController*)naVC.topViewController;
        
        controller.aryDataChou = sender;
        controller.projectID = self.strProjectID;
        controller.countDownJoinDate = self.countPayDate;
        controller.dataDate = self.dateEnd;
    }else if([segue.identifier isEqualToString:@"loginSegue"]){
    
        LoginViewController *controller = segue.destinationViewController;
        
        controller.delegate = self;
        
    }

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




-(void)buttonCancelWill{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)buttonJoinWill{
    if(_runTimesMoreStory != 1){
        
        _viewProjectMoreStory.hidden = NO;
//        self.scrollView.scrollEnabled = NO;
        _viewButtonsMoreStory.hidden = YES;
    }else{
    
    [self performSegueWithIdentifier:@"JoinSegue" sender:self.aryChouMoreStory];
        
    }
}

//-(void)buttonShareWillMoreStory{
//    
//
//}

-(void)LoginViewControllerClose:(LoginViewController *)controller{
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)buttonShare:(id)sender {
    
    //        [[ToolClass sharedInstance] showAlert:@"「分享」功能正在开发，稍后上线~"];
//    [UMSocialData defaultData].extConfig.wechatSessionData.title = self.strShareTitle;
//    [UMSocialSnsService presentSnsIconSheetView:self
//                                         appKey:@"5629c5de67e58e4ea8002490"
//                                      shareText:self.strShareText
//                                     shareImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.strShareImage]]]
//                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToWechatSession,UMShareToWechatTimeline,nil]
//                                       delegate:self];
//    [UMSocialData defaultData].extConfig.wechatSessionData.url = [NSString stringWithFormat:@"http://www.huoban.io/project_detail.html?p_id=%@",self.strProjectID];
    
    [UIView animateWithDuration:0.3f animations:^{
        _viewShareShaowMoreStory.hidden = NO;
        _viewShareShaowMoreStory.alpha = 1.0;
    } completion:^(BOOL finished) {
        
    }];
    
}

- (IBAction)buttonClose:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark 分享视图
-(void)viewCreateShareView{
    
    _iosBounds = _iOSBoundsMoreStory.width/2;
    
    _viewShareShaowMoreStory = [[UIView alloc] initWithFrame:CGRectMake(0,0 ,_iOSBoundsMoreStory.width,_iOSBoundsMoreStory.height)];
    
    _viewShareShaowMoreStory.backgroundColor = [UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:0.5];
    
    //    _viewShareShaow.layer.borderWidth = 1;
    //    _viewShareShaow.layer.borderColor = [UIColor redColor].CGColor;
    
    UIButton *buttonShareCancel = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    buttonShareCancel.frame = CGRectMake(0,0, _viewShareShaowMoreStory.frame.size.width,_viewShareShaowMoreStory.frame.size.height - 90);
    [buttonShareCancel addTarget:self action:@selector(buttonShareCancelMoreStory:) forControlEvents:UIControlEventTouchUpInside];
    
    //    buttonShareCancel.layer.borderColor = [UIColor redColor].CGColor;
    //    buttonShareCancel.layer.borderWidth = 1;
    
    [_viewShareShaowMoreStory addSubview:buttonShareCancel];
    
    UIView *viewShare = [[UIView alloc] initWithFrame:CGRectMake(0,_iOSBoundsMoreStory.height - 90, _iOSBoundsMoreStory.width, 90)];
    viewShare.backgroundColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
    [_viewShareShaowMoreStory addSubview:viewShare];
    
    UIImageView *imageWeChat = [[UIImageView alloc] initWithFrame:CGRectMake(_iosBounds/2 - 24, 12, 48, 48)];
    imageWeChat.clipsToBounds = YES;
    imageWeChat.layer.cornerRadius = 24;
    [viewShare addSubview:imageWeChat];
    UILabel *labelWeChat =[[UILabel alloc] initWithFrame:CGRectMake(_iosBounds/2  - 24, 66, 48, 12)];
    labelWeChat.text = @"微信好友";
    labelWeChat.textColor = [UIColor colorWithRed:199.0/255 green:199.0/255 blue:199.0/255 alpha:1];
    labelWeChat.font = [UIFont systemFontOfSize:12];
    labelWeChat.textAlignment = NSTextAlignmentCenter;
    [viewShare addSubview:labelWeChat];
    
    UIButton *buttonWeChat = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonWeChat addTarget:self action:@selector(buttonShareWeChatMoreStory:) forControlEvents:UIControlEventTouchUpInside];
    buttonWeChat.frame = CGRectMake(_iosBounds/2  - 24, 12, 50, 50);
    //    buttonWeChat.layer.borderWidth = 1;
    [viewShare addSubview:buttonWeChat];
    
    UIImageView *imageWeChatRound = [[UIImageView alloc] initWithFrame:CGRectMake(_iosBounds*2 - _iosBounds/2 - 24 , 12, 48, 48)];
    imageWeChatRound.clipsToBounds = YES;
    imageWeChatRound.layer.cornerRadius = 24;
    [viewShare addSubview:imageWeChatRound];
    
    UILabel *labelWeChatRound =[[UILabel alloc] initWithFrame:CGRectMake(_iosBounds*2 - _iosBounds/2 - 24 , 66, 48, 12)];
    labelWeChatRound.text = @"朋友圈";
    labelWeChatRound.textColor = [UIColor colorWithRed:199.0/255 green:199.0/255 blue:199.0/255 alpha:1];
    labelWeChatRound.font = [UIFont systemFontOfSize:12];
    labelWeChatRound.textAlignment = NSTextAlignmentCenter;
    [viewShare addSubview:labelWeChatRound];
    
    UIButton *buttonWeChatRound = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonWeChatRound addTarget:self action:@selector(buttonShareWeChatRoundMoreStory:) forControlEvents:UIControlEventTouchUpInside];
    buttonWeChatRound.frame = CGRectMake(_iosBounds*2 - _iosBounds/2 - 24 , 12, 50, 50);
    //    buttonWeChatRound.layer.borderWidth =1;
    [viewShare addSubview:buttonWeChatRound];
    
    
//    
//    UIImageView *imageWeiBo = [[UIImageView alloc] initWithFrame:CGRectMake(_iOSBoundsMoreStory.width - _iosBounds/2 - 24, 12, 48, 48)];
//    imageWeiBo.clipsToBounds = YES;
//    imageWeiBo.layer.cornerRadius = 24;
//    [viewShare addSubview:imageWeiBo];
//    
//    UILabel *labelWeiBo =[[UILabel alloc] initWithFrame:CGRectMake(_iOSBoundsMoreStory.width - _iosBounds/2 - 50, 66, 100, 12)];
//    labelWeiBo.text = @"COMMING SOON";
//    labelWeiBo.textColor = [UIColor colorWithRed:199.0/255 green:199.0/255 blue:199.0/255 alpha:1];
//    labelWeiBo.font = [UIFont systemFontOfSize:12];
//    labelWeiBo.textAlignment = NSTextAlignmentCenter;
//    [viewShare addSubview:labelWeiBo];
//    
//    UIButton *buttonWeiBo = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [buttonWeiBo addTarget:self action:@selector(buttonShareWeiBoMoreStory:) forControlEvents:UIControlEventTouchUpInside];
//    buttonWeiBo.frame = CGRectMake(_iOSBoundsMoreStory.width - _iosBounds/2 - 24, 12, 50, 50);
//    //    buttonWeiBo.layer.borderWidth = 1;
//    [viewShare addSubview:buttonWeiBo];
    
    
    if([_iOSSizeMoreStory isEqualToString:@"iPhone6Plus"]){
        imageWeChat.image = [UIImage imageNamed:@"ShareWeChatFriend@3x"];
        imageWeChatRound.image = [UIImage imageNamed:@"ShareWeChatPreson@3x"];
//        imageWeiBo.image = [UIImage imageNamed:@"Shareweibo@3x"];
    }else{
        imageWeChat.image = [UIImage imageNamed:@"ShareWeChatFriend@2x"];
        imageWeChatRound.image = [UIImage imageNamed:@"ShareWeChatPreson@2x"];
//        imageWeiBo.image = [UIImage imageNamed:@"Shareweibo@2x"];
    }
    
    
    
    [self.view addSubview:_viewShareShaowMoreStory];
    
    _viewShareShaowMoreStory.hidden = YES;
    _viewShareShaowMoreStory.alpha = 0.0;
    
}

-(void)buttonShareCancelMoreStory:(UIButton*)sender{
    
    [UIView animateWithDuration:0.3f animations:^{
        
        _viewShareShaowMoreStory.alpha = 0.0;
    } completion:^(BOOL finished) {
        _viewShareShaowMoreStory.hidden = YES;
    }];
}
-(void)buttonShareWeChatRoundMoreStory:(UIButton*)sender{
    
    [UMSocialData defaultData].extConfig.wechatSessionData.title = self.strShareTitle;
    [UMSocialData defaultData].extConfig.wechatSessionData.url = [NSString stringWithFormat:@"http://www.huoban.io/project_detail.html?p_id=%@",self.strProjectID];
    
    //使用UMShareToWechatSession,UMShareToWechatTimeline,UMShareToWechatFavorite分别代表微信好友、微信朋友圈、微信收藏
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatTimeline] content:self.strShareText image:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.strShareImage]]] location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
            NSLog(@"分享成功！");
        }
    }];
    
}
-(void)buttonShareWeChatMoreStory:(UIButton*)sender{
    
    [UMSocialData defaultData].extConfig.wechatSessionData.title = self.strShareTitle;
    [UMSocialData defaultData].extConfig.wechatSessionData.url = [NSString stringWithFormat:@"http://www.huoban.io/project_detail.html?p_id=%@",self.strProjectID];
    
    //使用UMShareToWechatSession,UMShareToWechatTimeline,UMShareToWechatFavorite分别代表微信好友、微信朋友圈、微信收藏
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatSession] content:self.strShareText image:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.strShareImage]]] location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
            NSLog(@"分享成功！");
        }
    }];
    
//    [UMSocialSnsService presentSnsIconSheetView:self
     //                                         appKey:@"5629c5de67e58e4ea8002490"
     //                                      shareText:self.strShareText
     //                                     shareImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.strShareImage]]]
     //                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToWechatSession,UMShareToWechatTimeline,nil]
     //                                       delegate:self];

}
-(void)buttonShareWeiBoMoreStory:(UIButton*)sender{
    
    [[ToolClass sharedInstance] showAlert:@"微博分享稍后上线~"];
 
    
}
@end
