//
//  CommentTableViewController.h
//  huoban
//
//  Created by Lyc on 15/12/23.
//  Copyright © 2015年 lyc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentSubmitBar.h"
#import "YcKeyBoardView.h"
#import "HttpClassSelf.h"
#import "DataModel.h"
#import "ToolClass.h"
#import "ImageSameWidth.h"
#import "MJRefresh.h"
#import "ProjectDetailsViewController.h"
#import "OtherUserInfoViewController.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "DynamicUpListTableViewController.h"
#import "CommentTableViewCell.h"
#import "TalkMessageTableViewCell.h"

@class ProjectCommentTableViewController;

@protocol ProjectCommentDelegate <NSObject>

-(void)projectCommentDelegateCancel:(ProjectCommentTableViewController*)controller;
-(void)ProjectCommentDelegateDelegate:(ProjectCommentTableViewController*)controller;
@end


@interface ProjectCommentTableViewController : UITableViewController<UIAlertViewDelegate,CommentSubmitBarDelegate>

@property(nonatomic,strong)NSString *strFeedID;

@property(nonatomic)id<ProjectCommentDelegate>delegate;



@property (weak, nonatomic) IBOutlet UIImageView *imageClose;

@property(nonatomic)BOOL isKeyBoardAppar;

- (IBAction)buttonCancel:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *buttonTEST;



@end
