//
//  MainTableViewCell.m
//  appDemo
//
//  Created by 刘雨辰 on 15/9/6.
//  Copyright (c) 2015年 lyc. All rights reserved.
//

#import "HomePageTableViewCell.h"
#import "MCFireworksButton.h"
#import "MCFireworksView.h"
#import "SDCollectionViewCell.h"
#import "SDCycleScrollView.h"
#import "DataModel.h"
#import "DataModelHomeSilder.h"
#import "ImageSameWidth.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "TYMProgressBarView.h"

@implementation HomePageTableViewCell

NSString *_iOSDevieceHomePage;
DataModel *_dataModelHomeCellPage;
CGSize _iOSLoginCellDeviceSize;


- (void)awakeFromNib{
    
    _dataModelHomeCellPage = [[DataModel alloc] init];
    
    self.labelProgress = [[UILabel alloc] init];
    self.labelProgress.font = [UIFont systemFontOfSize:9];
    self.labelProgress.textColor = [UIColor colorWithRed:238 green:238 blue:238 alpha:1];
    
    //选择视图
    _iOSLoginCellDeviceSize = [UIScreen mainScreen].bounds.size;
    
    
    _iOSDevieceHomePage = _dataModelHomeCellPage.userInfomation.iOSDeviceSize;

    self.imageViewContent.clipsToBounds = YES;
    self.imageViewContent.layer.cornerRadius = 8;
    
    self.viewCountry.clipsToBounds = YES;
    self.viewCountry.layer.cornerRadius = 8;
    
    self.viewType.clipsToBounds = YES;
    self.viewType.layer.cornerRadius = 8;
    
    self.viewDate.clipsToBounds = YES;
    self.viewDate.layer.cornerRadius = 8;
    
    self.viewPreson.clipsToBounds = YES;
    self.viewPreson.layer.cornerRadius = 8;

    self.imageViewContent.clipsToBounds = YES;
    self.imageViewContent.contentMode = UIViewContentModeScaleAspectFill;
    
    self.HomePageProgressBarView = [[TYMProgressBarView alloc] init];
    
    self.HomePageProgressBarView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    
//    if([_iOSDeviceSize isEqualToString:@"iPhone6Plus"]){
//        //轮播图高度:
//        _heightSliderCell = 270;
//        //card高度
//        _heightCardCell = 192;
//    }else if([_iOSDeviceSize isEqualToString:@"iPhone6"]){
//        //轮播图高度:
//        _heightSliderCell = 252;
//        _heightCardCell = 192;
//    }else if([_iOSDeviceSize isEqualToString:@"iPhone5"]){
//        //轮播图高度：
//        _heightSliderCell = 216;
//        //card高度:
//        _heightCardCell = 156;
//    }else if([_iOSDeviceSize isEqualToString:@"iPhone4"]){
//        //轮播图高度:
//        _heightSliderCell = 216;
//        _heightCardCell = 156;
//    }
    //进度条起始点0.13
    self.HomePageProgressBarView.barBorderWidth = 0.0f;
    self.HomePageProgressBarView.barInnerPadding = 0.0f;
    //    self.HomePageProgressBarView.barFillColor = [UIColor colorWithRed:255.0/255 green:51.0/255 blue:51.0/255 alpha:1];
    
    [self addSubview:self.HomePageProgressBarView];
    [self.HomePageProgressBarView addSubview:self.labelProgress];
    
    if([_iOSDevieceHomePage isEqualToString:@"iPhone6Plus"]){
        
        self.imageViewContentHeight.constant = 180;
        self.viewTypeHeight.constant = 48;
        self.viewCountryHeight.constant = 48;
        self.viewDateHeight.constant = 48;
        self.viewPresonHeight.constant = 48;
        self.viewCell.frame = CGRectMake(0,0, _iOSLoginCellDeviceSize.width, 192);

//        
        
        
    }else if([_iOSDevieceHomePage isEqualToString:@"iPhone6"]){
        
        self.imageViewContentHeight.constant = 160;
        self.viewTypeHeight.constant = 48;
        self.viewCountryHeight.constant = 48;
        self.viewDateHeight.constant = 48;
        self.viewPresonHeight.constant = 48;
//        self.HomePageProgressBarView.frame = CGRectMake(self.viewPreson.frame.origin.x, self.viewPreson.frame.origin.y - 22,self.viewCell.frame.size.height - 70,16);
        self.viewCell.frame = CGRectMake(0,0, _iOSLoginCellDeviceSize.width, 192);
    }else{

        self.imageViewContentHeight.constant = 144;
        self.viewTypeHeight.constant = 44;
        self.viewCountryHeight.constant = 44;
        self.viewDateHeight.constant = 44;
        self.viewPresonHeight.constant = 44;
//        self.HomePageProgressBarView.frame = CGRectMake(self.viewPreson.frame.origin.x, self.viewPreson.frame.origin.y - 22,self.viewCell.frame.size.height - 66,16);
        self.viewCell.frame = CGRectMake(0,0, _iOSLoginCellDeviceSize.width, 156);
        
    }
    

//    self.viewCell.layer.borderWidth = 1;
//    self.viewCell.layer.borderColor = [UIColor redColor].CGColor;
    
}





- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];


}


@end
