//
//  SuppotPresonViewController.m
//  huoban
//
//  Created by 刘雨辰 on 15/11/3.
//  Copyright © 2015年 lyc. All rights reserved.
//

#import "SuppotPresonViewController.h"
#import "DataModel.h"
#import "OtherUserInfoViewController.h"
#import "UIImageView+WebCache.h"
#import "JoinProjectTableViewController.h"
#import "UserSelfMessageViewController.h"




@interface SuppotPresonViewController ()

@end

@implementation SuppotPresonViewController
@synthesize dicChou,dicCreator,scrollViewHuoban,imageClose,imageViewShare,buttonShare,strProjectID,strShareText,strShareTitle,strShareImage;

DataModel *_dataModelHuoban;
NSString *_iOSHuoban;
NSInteger focusYYY;
NSMutableArray *_aryChouList;
NSMutableArray *_aryCreatorList;
UIView *_viewJoin;
CGSize _iOSAllPresonBound;
UIView *_viewProjectAllPreson;
UIView *_viewLoginAllPreson;
NSInteger _intLoginTimes;

NSInteger _intiOSBoundAllPreson;
UIView *_viewShareShaowAllPreson;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //上放导航栏
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:1]];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName : [UIColor colorWithRed:170.0/255 green:170.0/255 blue:170.0/255 alpha:1]}];
    
    
    //    导航栏左侧Item
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:19],NSForegroundColorAttributeName :[UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1]} forState:UIControlStateNormal];
    
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:170.0/255 green:170.0/255 blue:170.0/255 alpha:1];

//    self.scrollViewHuoban.backgroundColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];

    self.scrollViewHuoban.backgroundColor = [UIColor whiteColor];
    _dataModelHuoban = [[DataModel alloc] init];
    
    _iOSHuoban = _dataModelHuoban.userInfomation.iOSDeviceSize;
    
    _iOSAllPresonBound = [UIScreen mainScreen].bounds.size;

    if([_iOSHuoban isEqualToString:@"iPhone6Plus"]){
        
        self.imageClose.image = [UIImage imageNamed:@"back_grew_3x"];
        self.imageViewShare.image = [UIImage imageNamed:@"share_grew@3x"];
        
    }else{
        self.imageClose.image = [UIImage imageNamed:@"back_grew_2x"];
        self.imageViewShare.image = [UIImage imageNamed:@"share_grew@2x"];
    }
    
    
    _aryCreatorList = [[NSMutableArray alloc] init];
    _aryChouList = [[NSMutableArray alloc] init];
    
    //登陆视图
//    _viewProjectAllPreson = [[UIView alloc] initWithFrame:CGRectMake(0 ,self.scrollViewHuoban.contentOffset.y, _iOSAllPresonBound.width, _iOSAllPresonBound.height)];
//    _viewProjectAllPreson.backgroundColor = [UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:0.95];
//    [self.view addSubview:_viewProjectAllPreson];
    
//    UIView *viewLoginBlock = [[UIView alloc] initWithFrame:CGRectMake(0,0,78,162)];
//    viewLoginBlock.backgroundColor = [UIColor colorWithRed:38.0/255 green:55.0/255 blue:70.0/255 alpha:1];
//    viewLoginBlock.clipsToBounds = YES;
//    viewLoginBlock.layer.cornerRadius = 12;
//    viewLoginBlock.center = _viewProjectAllPreson.center;
//    [_viewProjectAllPreson addSubview:viewLoginBlock];
    
//    UIButton *buttonLogin = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [buttonLogin addTarget:self action:@selector(buttonAllPreosnDynamicClick:) forControlEvents:UIControlEventTouchUpInside];
//    buttonLogin.backgroundColor = [UIColor colorWithRed:119.0/255 green:134.0/255 blue:146.0/255 alpha:1];
//    [buttonLogin setTitleColor:[UIColor colorWithRed:38.0/255 green:55.0/255 blue:70.0/255 alpha:1]forState:UIControlStateNormal];
//    [buttonLogin setTitle:@"登陆" forState:UIControlStateNormal];
//    buttonLogin.clipsToBounds = YES;
//    buttonLogin.layer.cornerRadius = 27;
//    [buttonLogin setFrame:CGRectMake(12,84,54,54)];
//    [viewLoginBlock addSubview:buttonLogin];
//    
//    UIImageView *viewClose = [[UIImageView alloc] initWithFrame:CGRectMake(31,36,16,16)];
//
//    

//    [viewLoginBlock addSubview:viewClose];
//    
//    
//    UIButton *buttonClose = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [buttonClose addTarget:self action:@selector(buttonMoreDynmaicLoginCloseClick:) forControlEvents:UIControlEventTouchUpInside];
//    [buttonClose setFrame:CGRectMake(17,18,44,44)];
//    //    buttonClose.layer.borderWidth = 1;
//    [viewLoginBlock addSubview:buttonClose];
//    
//    _viewProjectAllPreson.hidden = YES;
    

    //低栏「加入他们」
    _viewJoin = [[UIView alloc] initWithFrame:CGRectMake(0,_iOSAllPresonBound.height - 50,_iOSAllPresonBound.width, 50)];
    [self.view addSubview:_viewJoin];
    
    UIButton *buttonJoin = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonJoin addTarget:self action:@selector(buttonJoinWillAllPreson) forControlEvents:UIControlEventTouchUpInside];
    
    [buttonJoin setBackgroundColor:[UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1]];

    if(self.isJoinProject == YES){
    [buttonJoin setTitle:@"支持更多" forState:UIControlStateNormal];
    }else{
    [buttonJoin setTitle:@"加入他们" forState:UIControlStateNormal];
    }
    
    buttonJoin.titleLabel.font = [UIFont boldSystemFontOfSize:14.7];
    
    buttonJoin.frame = CGRectMake(0, 0,_viewJoin.frame.size.width,50);
 
    [buttonJoin setTitleColor:[UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1]  forState:UIControlStateNormal];
    
    [_viewJoin addSubview:buttonJoin];
    
    if(self.isProjectEnd == NO){
        _viewJoin.hidden = NO;
    }else{
        _viewJoin.hidden = YES;
    }
    
    
    NSInteger focusXXX  = 18;
    focusYYY = -50;
    
    self.title = [NSString stringWithFormat:@"全部伙伴 %lu",[self.dicChou[@"users"] count]];
    
    //用户头像
    NSMutableArray *dicChouItem = self.dicChou[@"users"];
    
    NSLog(@"%lu",[self.dicChou[@"users"] count]);
    
        for(int i =0;i < [self.dicChou[@"users"] count];i++){
        
            if([_iOSHuoban isEqualToString:@"iPhone6Plus"] || [_iOSHuoban isEqualToString:@"iPhone6"]){
                if(i != 0 && i%5 == 0 ){
                    focusXXX = 18;
                    focusYYY += 98;
                }
            }else{
                if( i != 0 && i%4 == 0 ){
                    focusXXX = 18;
                    focusYYY += 98;
                }
            }
            [_aryChouList addObject:dicChouItem[i]];
            
            UIImageView *focusImageView = [[UIImageView alloc] initWithFrame:CGRectMake(focusXXX, focusYYY, 65,65)];
            
            //网络加载
//            focusImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:dicChouItem[i][@"image"]]]];
            [focusImageView sd_setImageWithURL:dicChouItem[i][@"image"] placeholderImage:nil];
            focusImageView.clipsToBounds = YES;
            focusImageView.layer.cornerRadius = 32.5;
            focusImageView.userInteractionEnabled = YES;
            focusImageView.contentMode = UIViewContentModeScaleAspectFill;
            [self.scrollViewHuoban addSubview:focusImageView];
            
            UITapGestureRecognizer *imageFocusTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buttonImagePresonClick:)];
            [focusImageView addGestureRecognizer:imageFocusTap];
            focusImageView.tag = i;
            
            UILabel *labelFocusName = [[UILabel alloc] initWithFrame:CGRectMake(focusXXX, focusYYY + 71,65,14)];
            labelFocusName.text = dicChouItem[i][@"name"];
            labelFocusName.font = [UIFont systemFontOfSize:12];
            labelFocusName.textColor = [UIColor colorWithRed:47.0/255 green:47.0/255 blue:47.0/255 alpha:1];
            labelFocusName.clipsToBounds = YES;
            labelFocusName.textAlignment = NSTextAlignmentCenter;
            [self.scrollViewHuoban addSubview:labelFocusName];
            
            if([_iOSHuoban isEqualToString:@"iPhone6Plus"] ){
                focusXXX  += 71;
            }else if([_iOSHuoban isEqualToString:@"iPhone6"]){
                focusXXX  += 71;
            }else{
                focusXXX  += 75;
            }
        }
    [self viewCreateShareView];
    [self viewLoginCreate];
}

-(void)buttonJoinWillAllPreson{
    
    
//    if(_runTimesMoreDynamic != 1){
//        
//        _viewProjectDetailsDynamic.hidden = NO;
//        self.tableViewMoreDynamic.scrollEnabled = NO;
//        _viewButtonsMoreDynamic.hidden = YES;
//        
//        
//    }else{
//        
//        [self performSegueWithIdentifier:@"JoinSegue" sender:self.aryChouMordDynameic];
//
//    }
    
        _intLoginTimes  = [[NSUserDefaults standardUserDefaults] integerForKey:@"runtimes"];
    
    if(_intLoginTimes == 0){
        _viewLoginAllPreson.hidden = NO;
        _viewJoin.hidden = YES;
    }else{
        [self performSegueWithIdentifier:@"JoinSegue" sender:self.aryChouAllPreson];
    }

    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    self.scrollViewHuoban.contentSize = CGSizeMake(self.scrollViewHuoban.frame.size.width,focusYYY + 130);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"OtherUserSegue"]){
        
        UINavigationController *naVC = segue.destinationViewController;
        
        OtherUserInfoViewController *controller = (OtherUserInfoViewController*)naVC.topViewController;
        
        NSLog(@"打印打印他人信息%@",sender);
        
        controller.dicOtherUser = sender;
        
    }else if([segue.identifier isEqualToString:@"JoinSegue"]){
        
        UINavigationController *naVC = segue.destinationViewController;
        
        JoinProjectTableViewController *controller = (JoinProjectTableViewController*)naVC.topViewController;
        
        controller.aryDataChou = sender;
        controller.projectID = self.strProjectID;

    }else if([segue.identifier isEqualToString:@"loginSegue"]){
        
        LoginViewController *controller = segue.destinationViewController;
        
        controller.delegate = self;
        
    }
    
}

- (IBAction)buttonCancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)buttonImagePresonClick:(UITapGestureRecognizer*)sender{
    
//    N%dSLog(@"点击了头像Image--弹出「他人信息」界面%lu",sender.view.tag);
    
//    [self performSegueWithIdentifier:@"OtherUserSegue" sender:_aryChouList[sender.view.tag]];
#warning segue
    
    UserSelfMessageViewController * userInfoVC = [[UserSelfMessageViewController alloc]initForOtherUserWithUserID:_aryChouList[sender.view.tag][@"_id"]];
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController pushViewController:userInfoVC animated:YES];

    
}

-(void)buttonCreatorImagePresonClick:(UITapGestureRecognizer*)sender{
    
    //    N%dSLog(@"点击了头像Image--弹出「他人信息」界面%lu",sender.view.tag);
    
//    [self performSegueWithIdentifier:@"OtherUserSegue" sender:_aryCreatorList[sender.view.tag]];
#warning segue
    UserSelfMessageViewController * userInfoVC = [[UserSelfMessageViewController alloc]initForOtherUserWithUserID:_aryCreatorList[sender.view.tag][@"_id"]];
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController pushViewController:userInfoVC animated:YES];

}

- (IBAction)buttonShare:(id)sender {
    
//    [UMSocialData defaultData].extConfig.wechatSessionData.title = self.strShareTitle;
//    [UMSocialSnsService presentSnsIconSheetView:self
//                                         appKey:@"5629c5de67e58e4ea8002490"
//                                      shareText:self.strShareText
//                                     shareImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.strShareImage]]]
//                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToWechatSession,UMShareToWechatTimeline,nil]
//                                       delegate:self];
//    [UMSocialData defaultData].extConfig.wechatSessionData.url = [NSString stringWithFormat:@"http://www.huoban.io/project_detail.html?p_id=%@",self.strProjectID];
    
    [UIView animateWithDuration:0.3f animations:^{
        _viewShareShaowAllPreson.hidden = NO;
        _viewShareShaowAllPreson.alpha = 1.0;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)viewLoginCreate{
    
    
    //登陆视图
    _viewLoginAllPreson = [[UIView alloc] initWithFrame:CGRectMake(0,0,_iOSAllPresonBound.width, _iOSAllPresonBound.height)];
    _viewLoginAllPreson.backgroundColor = [UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:0.95];
    [self.view addSubview:_viewLoginAllPreson];
    
    UIView *viewLoginBlock = [[UIView alloc]initWithFrame:CGRectMake(0,0,78,162)];
    viewLoginBlock.backgroundColor = [UIColor colorWithRed:38.0/255 green:55.0/255 blue:70.0/255 alpha:1];
    viewLoginBlock.clipsToBounds = YES;
    viewLoginBlock.layer.cornerRadius = 12;
    viewLoginBlock.center = _viewLoginAllPreson.center;
    [_viewLoginAllPreson addSubview:viewLoginBlock];
    
    
    UIButton *buttonLogin = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonLogin addTarget:self action:@selector(buttonAllPresonLoginClick:) forControlEvents:UIControlEventTouchUpInside];
    buttonLogin.backgroundColor = [UIColor colorWithRed:119.0/255 green:134.0/255 blue:146.0/255 alpha:1];
    [buttonLogin setTitleColor:[UIColor colorWithRed:38.0/255 green:55.0/255 blue:70.0/255 alpha:1] forState:UIControlStateNormal];
    [buttonLogin setTitle:@"登录" forState:UIControlStateNormal];
    buttonLogin.clipsToBounds = YES;
    buttonLogin.layer.cornerRadius = 27;
    [buttonLogin setFrame:CGRectMake(12,84,54,54)];
    [viewLoginBlock addSubview:buttonLogin];
    
    UIImageView *viewClose = [[UIImageView alloc] initWithFrame:CGRectMake(31,36,16,16)];
    if([_iOSHuoban isEqualToString:@"iPhone6Plus"]){
        viewClose.image = [UIImage imageNamed:@"close_grey_@3x"];
    }else{
        viewClose.image = [UIImage imageNamed:@"close_grey_@2x"];

    }
    [viewLoginBlock addSubview:viewClose];
    
    
    UIButton *buttonCloseLogin = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonCloseLogin addTarget:self action:@selector(buttonAllPresonLoginCloseClick:) forControlEvents:UIControlEventTouchUpInside];
    [buttonCloseLogin setFrame:CGRectMake(17,18,44,44)];
    //    buttonClose.layer.borderWidth = 1;
    [viewLoginBlock addSubview:buttonCloseLogin];
    _viewLoginAllPreson.hidden = YES;
}

-(void)buttonAllPresonLoginClick:(UIButton*)sender{
    
        [self performSegueWithIdentifier:@"loginSegue" sender:nil];
    
}
-(void)buttonAllPresonLoginCloseClick:(UIButton*)sender{
    
    _viewLoginAllPreson.hidden = YES;
    _viewProjectAllPreson.hidden = NO;
}


#pragma mark 分享视图
-(void)viewCreateShareView{
    
    _intiOSBoundAllPreson = _iOSAllPresonBound.width/2;
    
    _viewShareShaowAllPreson = [[UIView alloc] initWithFrame:CGRectMake(0,0 ,_iOSAllPresonBound.width,_iOSAllPresonBound.height)];
    
    _viewShareShaowAllPreson.backgroundColor = [UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:0.5];
    
    //    _viewShareShaow.layer.borderWidth = 1;
    //    _viewShareShaow.layer.borderColor = [UIColor redColor].CGColor;
    
    UIButton *buttonShareCancel = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    buttonShareCancel.frame = CGRectMake(0,0, _viewShareShaowAllPreson.frame.size.width,_viewShareShaowAllPreson.frame.size.height - 90);
    [buttonShareCancel addTarget:self action:@selector(buttonShareCancelAllPreson:) forControlEvents:UIControlEventTouchUpInside];
    
    //    buttonShareCancel.layer.borderColor = [UIColor redColor].CGColor;
    //    buttonShareCancel.layer.borderWidth = 1;
    
    [_viewShareShaowAllPreson addSubview:buttonShareCancel];
    
    UIView *viewShare = [[UIView alloc] initWithFrame:CGRectMake(0,_iOSAllPresonBound.height - 90, _iOSAllPresonBound.width, 90)];
    viewShare.backgroundColor = [UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1];
    [_viewShareShaowAllPreson addSubview:viewShare];
    
    UIImageView *imageWeChat = [[UIImageView alloc] initWithFrame:CGRectMake(_intiOSBoundAllPreson/2 - 24, 12, 48, 48)];
    imageWeChat.clipsToBounds = YES;
    imageWeChat.layer.cornerRadius = 24;
    [viewShare addSubview:imageWeChat];
    UILabel *labelWeChat =[[UILabel alloc] initWithFrame:CGRectMake(_intiOSBoundAllPreson/2  - 24, 66, 48, 12)];
    labelWeChat.text = @"微信好友";
    labelWeChat.textColor = [UIColor colorWithRed:199.0/255 green:199.0/255 blue:199.0/255 alpha:1];
    labelWeChat.font = [UIFont systemFontOfSize:12];
    labelWeChat.textAlignment = NSTextAlignmentCenter;
    [viewShare addSubview:labelWeChat];
    
    UIButton *buttonWeChat = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonWeChat addTarget:self action:@selector(buttonShareWeChatMoreDynamic:) forControlEvents:UIControlEventTouchUpInside];
    buttonWeChat.frame = CGRectMake(_intiOSBoundAllPreson/2  - 24, 12, 50, 50);
    //    buttonWeChat.layer.borderWidth = 1;
    [viewShare addSubview:buttonWeChat];
    
    UIImageView *imageWeChatRound = [[UIImageView alloc] initWithFrame:CGRectMake(_intiOSBoundAllPreson*2 - _intiOSBoundAllPreson/2 - 24 , 12, 48, 48)];
    imageWeChatRound.clipsToBounds = YES;
    imageWeChatRound.layer.cornerRadius = 24;
    [viewShare addSubview:imageWeChatRound];
    
    UILabel *labelWeChatRound =[[UILabel alloc] initWithFrame:CGRectMake(_intiOSBoundAllPreson*2 - _intiOSBoundAllPreson/2 - 24 , 66, 48, 12)];
    labelWeChatRound.text = @"朋友圈";
    labelWeChatRound.textColor = [UIColor colorWithRed:199.0/255 green:199.0/255 blue:199.0/255 alpha:1];
    labelWeChatRound.font = [UIFont systemFontOfSize:12];
    labelWeChatRound.textAlignment = NSTextAlignmentCenter;
    [viewShare addSubview:labelWeChatRound];
    
    UIButton *buttonWeChatRound = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonWeChatRound addTarget:self action:@selector(buttonShareWeChatRoundMoreDynamic:) forControlEvents:UIControlEventTouchUpInside];
    buttonWeChatRound.frame = CGRectMake(_intiOSBoundAllPreson*2 - _intiOSBoundAllPreson/2 - 24 , 12, 50, 50);
    //    buttonWeChatRound.layer.borderWidth =1;
    [viewShare addSubview:buttonWeChatRound];
    
    
//    
//    UIImageView *imageWeiBo = [[UIImageView alloc] initWithFrame:CGRectMake(_iOSAllPresonBound.width - _intiOSBoundAllPreson/2 - 24, 12, 48, 48)];
//    imageWeiBo.clipsToBounds = YES;
//    imageWeiBo.layer.cornerRadius = 24;
//    [viewShare addSubview:imageWeiBo];
//    
//    UILabel *labelWeiBo =[[UILabel alloc] initWithFrame:CGRectMake(_iOSAllPresonBound.width - _intiOSBoundAllPreson/2 - 50, 66, 100, 12)];
//    labelWeiBo.text = @"COMMING SOON";
//    labelWeiBo.textColor = [UIColor colorWithRed:199.0/255 green:199.0/255 blue:199.0/255 alpha:1];
//    labelWeiBo.font = [UIFont systemFontOfSize:12];
//    labelWeiBo.textAlignment = NSTextAlignmentCenter;
//    [viewShare addSubview:labelWeiBo];
//    
//    UIButton *buttonWeiBo = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [buttonWeiBo addTarget:self action:@selector(buttonShareWeiBoDynamic:) forControlEvents:UIControlEventTouchUpInside];
//    buttonWeiBo.frame = CGRectMake(_iOSAllPresonBound.width - _intiOSBoundAllPreson/2 - 24, 12, 50, 50);
//    //    buttonWeiBo.layer.borderWidth = 1;
//    [viewShare addSubview:buttonWeiBo];
    
    
    if([_iOSHuoban isEqualToString:@"iPhone6Plus"]){
        imageWeChat.image = [UIImage imageNamed:@"ShareWeChatFriend@3x"];
        imageWeChatRound.image = [UIImage imageNamed:@"ShareWeChatPreson@3x"];
//        imageWeiBo.image = [UIImage imageNamed:@"Shareweibo@3x"];
    }else{
        imageWeChat.image = [UIImage imageNamed:@"ShareWeChatFriend@2x"];
        imageWeChatRound.image = [UIImage imageNamed:@"ShareWeChatPreson@2x"];
//        imageWeiBo.image = [UIImage imageNamed:@"Shareweibo@2x"];
    }
    
    
    
    [self.view addSubview:_viewShareShaowAllPreson];
    
    _viewShareShaowAllPreson.hidden = YES;
    _viewShareShaowAllPreson.alpha = 0.0;
    
}
-(void)buttonShareCancelAllPreson:(UIButton*)sender{
    
    [UIView animateWithDuration:0.3f animations:^{
        
        _viewShareShaowAllPreson.alpha = 0.0;
    } completion:^(BOOL finished) {
        _viewShareShaowAllPreson.hidden = YES;
    }];
    
}
-(void)buttonShareWeChatMoreDynamic:(UIButton*)sender{
 
//       NSLog(@"%@----%@",self.strShareText,self.strProjectID);
    
    [UMSocialData defaultData].extConfig.wechatSessionData.title = self.strShareTitle;
    [UMSocialData defaultData].extConfig.wechatSessionData.url = [NSString stringWithFormat:@"http://www.huoban.io/project_detail.html?p_id=%@",self.strProjectID];
    
    //使用UMShareToWechatSession,UMShareToWechatTimeline,UMShareToWechatFavorite分别代表微信好友、微信朋友圈、微信收藏
 
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatSession] content:self.strShareText image:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.strShareImage]]] location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
            NSLog(@"分享成功！");
        }
    }];
    
}
-(void)buttonShareWeChatRoundMoreDynamic:(UIButton*)sender{
    
    
    [UMSocialData defaultData].extConfig.wechatSessionData.title = self.strShareTitle;
    [UMSocialData defaultData].extConfig.wechatSessionData.url = [NSString stringWithFormat:@"http://www.huoban.io/project_detail.html?p_id=%@",self.strProjectID];
    
    //使用UMShareToWechatSession,UMShareToWechatTimeline,UMShareToWechatFavorite分别代表微信好友、微信朋友圈、微信收藏
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatTimeline] content:self.strShareText image:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.strShareImage]]] location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
            NSLog(@"分享成功！");
        }
    }];
    
}
-(void)buttonShareWeiBoDynamic:(UIButton*)sender{
    
    [[ToolClass sharedInstance] showAlert:@"微博分享稍后上线~"];
    
}

@end
