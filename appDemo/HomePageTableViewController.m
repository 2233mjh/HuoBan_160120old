//
//  MainTableViewController.m
//  appDemo
//
//  Created by 刘雨辰 on 15/8/25.
//  Copyright (c) 2015年 lyc. All rights reserved.
//

#import "HomePageTableViewController.h"


#define kDEFAULT_DATE_TIME_FORMAT (@"yyyy-MM-dd")

@interface HomePageTableViewController ()

@end

@implementation HomePageTableViewController
bool _selected;
NSInteger _countCreateCell;
float _floatProgress;

//对象模型
HttpClassSelf *_httpClassHomePage;
NSMutableDictionary *_resHomePageDic;
ProjectModel *_projectModel;


//点赞数组
NSMutableArray *_aryButtonLove;
//首页data对象
NSMutableArray *_cellTopData;

//加载data对象
NSMutableArray *_cellDownData;

//所有celldata对象
NSMutableArray *_cellHomeSlide;

//Image数组
NSMutableArray *_aryImage;

NSMutableArray *_aryloveButton;

DataModel *_dataModelHomePage;

DataModelHomeCard *_dataModelHomeCard;

NSMutableArray *_aryCacheHomePageData;

//数组个数
int _arrayNumTop;

//数组个数
int _arrayNumDown;

//首页页数
int _pageNumTop;


//加载页数
int _pageNumDown;

//每页项目数
int _CountTop;

//每页项目数
int _CountDown;

BOOL isRegNib;

BOOL _isNil;

NSInteger _loveTime;

NSInteger _runtime;

//NSInteger _buttonLoveNum;

//轮播图高度
CGFloat _heightSliderCell;
//图片宽度
CGFloat _widthMainImage;
//Card高度
CGFloat _heightCardCell;

//发送http
NSString * _strKey = @"home";

NSString * _iOSDeviceSize;

UIView * _viewPopLogin;
CGSize _iOSLoginDeviceSize;
UIView * _viewTop;

NSDateFormatter *_dataForMatterHomePage;
NSTimeZone *_timeZoneHomePage;
NSDate *_dataNowHomePage;

bool isRefresh;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _viewTop = [[UIView alloc] initWithFrame:CGRectMake(0, 0,_iOSLoginDeviceSize.width,20)];
    _viewTop.backgroundColor = [UIColor colorWithRed:21.0/255 green:29.0/255 blue:40.0/255 alpha:1];
    
//    _viewTop.layer.borderColor = [UIColor yellowColor].CGColor;
//    _viewTop.layer.borderWidth = 1;
    
    [self.tableView addSubview:_viewTop];
    _viewTop.hidden = YES;
    
    
    _dataForMatterHomePage = [[NSDateFormatter alloc] init];
    [_dataForMatterHomePage setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    _timeZoneHomePage = [NSTimeZone systemTimeZone];
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if([_iOSDeviceSize isEqualToString:@"iPhone6Plus"]){
        
        if(scrollView.contentOffset.y < -20 || scrollView.contentOffset.y >270){
            _viewTop.hidden = NO;
            
            _viewTop.frame = CGRectMake(0,scrollView.contentOffset.y,_viewTop.frame.size.width, 20);
            
        }else{
            _viewTop.hidden = YES;
        }
        
    }else if([_iOSDeviceSize isEqualToString:@"iPhone6"]){
    
        if(scrollView.contentOffset.y < -20 || scrollView.contentOffset.y >252){
            _viewTop.hidden = NO;
            
            _viewTop.frame = CGRectMake(0,scrollView.contentOffset.y,_viewTop.frame.size.width, 20);
            
        }else{
            _viewTop.hidden = YES;
        }
    }else{
        
        if(scrollView.contentOffset.y < -20 || scrollView.contentOffset.y >216){
            _viewTop.hidden = NO;
            
            _viewTop.frame = CGRectMake(0,scrollView.contentOffset.y,_viewTop.frame.size.width, 20);
            
        }else{
            _viewTop.hidden = YES;
        }
    }
    
}

-(void)buttonLoginCloseClick:(UIButton*)sender{
    
    _viewPopLogin.hidden = YES;
    self.tableView.scrollEnabled = YES;
}

-(void)buttonLoginClick:(UIButton*)sender{
    
    NSLog(@"点击弹出登陆按钮按钮按钮");
    _viewPopLogin.hidden = YES;
    self.tableView.scrollEnabled = YES;
    [self performSegueWithIdentifier:@"loginSegue" sender:nil];
    
}
//集成刷新控件
- (void)setupRefresh
{
    
    // 1.下拉刷新(进入刷新状态就会调用self的headerRereshing)
    [self.tableView addHeaderWithTarget:self action:@selector(headerRereshing)];
    

    
    // 2.上拉加载更多(进入刷新状态就会调用self的footerRereshing)
    [self.tableView addFooterWithTarget:self action:@selector(footerRereshing)];
    

    
}

//画面加载前触发
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setupRefresh];
    
    _dataModelHomePage = [[DataModel alloc] init];
    
    _iOSDeviceSize =  _dataModelHomePage.userInfomation.iOSDeviceSize;
    
    //选择视图
    _iOSLoginDeviceSize = [UIScreen mainScreen].bounds.size;

    
    _countCreateCell = 0;

    _runtime  = [[NSUserDefaults standardUserDefaults] integerForKey:@"runtimes"];
    
    _viewTop.hidden = NO;

    if(_runtime != 1){
        
        DataModel *dataModelUnLogin = [[DataModel alloc] init];
        dataModelUnLogin.userInfomation.tokenID = @"UnLogin";
        dataModelUnLogin.userInfomation.UserID = @"UnLogin";
        [dataModelUnLogin saveUserInfomation];
        
    }
    
    [self loginViewCreate];
    
        _viewPopLogin.hidden = YES;
    
        _projectModel = [[ProjectModel alloc]init];
        
        //本地变量
        _cellDownData = [[NSMutableArray alloc]init];
        _httpClassHomePage = [[HttpClassSelf alloc] init];
        _cellTopData = [[NSMutableArray alloc] init];
        _cellHomeSlide = [[NSMutableArray alloc] init];
        
        isRegNib = NO;
        
        _loveTime = 0;
    
        if([_iOSDeviceSize isEqualToString:@"iPhone6Plus"]){
            //轮播图高度:
            _heightSliderCell = 270;
            //card高度
            _heightCardCell = 192;
        }else if([_iOSDeviceSize isEqualToString:@"iPhone6"]){
            //轮播图高度:
            _heightSliderCell = 252;
            _heightCardCell = 192;
        }else if([_iOSDeviceSize isEqualToString:@"iPhone5"]){
            //轮播图高度：
            _heightSliderCell = 216;
            //card高度:
            _heightCardCell = 156;
        }else if([_iOSDeviceSize isEqualToString:@"iPhone4"]){
            //轮播图高度:
            _heightSliderCell = 216;
            _heightCardCell = 156;
        }
    
        [self.tableView headerBeginRefreshing];
  
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    _viewPopLogin.hidden = YES;
    self.tableView.scrollEnabled = YES;
    
}
//下拉刷新触发方法
-(void)headerRereshing{

    _CountTop = 6;
    _pageNumTop = 0;
    _CountDown = 6;
    _pageNumDown = 1;
    
    DataModel *dataModelHeader = [[DataModel alloc] init];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"HomePageCellDataModel"] != nil ){

                    NSData *dataHomePage = [[NSUserDefaults standardUserDefaults] objectForKey:@"HomePageCellDataModel"];
                    _cellTopData = [NSKeyedUnarchiver unarchiveObjectWithData:dataHomePage];
                    NSLog(@"%@",_cellTopData);

                    [self.tableView reloadData];
                }
    //异步回调
    [_httpClassHomePage homePageSetKey:_strKey numPage:_pageNumTop numNum:_CountTop token:dataModelHeader.userInfomation.tokenID CallBackYES:^(MKNetworkOperation *operatioin){
        NSData *data = [operatioin responseData];
        NSMutableDictionary *resHomePageDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];

        
        if([resHomePageDic[@"status"] isEqualToString:@"success"]){
            //[@"data"]大数组；
            
            _dataNowHomePage = [NSDate date];
            
            NSMutableDictionary *dic  =  resHomePageDic[@"data"];
            
            NSLog(@"主页返回jSON%@",dic);
                [_cellTopData removeAllObjects];
            
            //[@"projects"]项目详情数组；刷新tableView
            for(NSMutableDictionary *dicCell in dic[@"projects"]){
                
//                _CountTop = (int)[_cellTopData count];
//                _cellTopData[_CountTop] = dicCell;
                [_cellTopData addObject:dicCell];
            }
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"HomePageCellDataModel"];
            NSData *dataSaveHomePageCell = [NSKeyedArchiver archivedDataWithRootObject:_cellTopData];
            [[NSUserDefaults standardUserDefaults] setObject:dataSaveHomePageCell forKey:@"HomePageCellDataModel"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [_cellHomeSlide removeAllObjects];
            //@["slide"]轮播图数组
            for(NSDictionary *dicDymic in dic[@"slide"]){
                DataModelHomeSilder *dataModel = [[DataModelHomeSilder alloc] initWithResJSON:dicDymic];
                [_cellHomeSlide addObject:dataModel];
            }

            [self.tableView reloadData];
            [self.tableView headerEndRefreshing];
        }else{
            [self.tableView headerEndRefreshing];
            [[ToolClass sharedInstance]showAlert:@"刷新失败，服务器报错"];
        }
    }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
            [[ToolClass sharedInstance]showAlert:@"刷新失败,请检查网络"];
        [self.tableView headerEndRefreshing];
    }];
}
//下拉刷新
-(void)footerRereshing{
    
    DataModel *dataModelFooter = [[DataModel alloc] init];
    
   _isNil = NO;
    [_httpClassHomePage homePageSetKey:_strKey numPage:_pageNumDown numNum:_CountDown token:dataModelFooter.userInfomation.tokenID CallBackYES:^(MKNetworkOperation *operatioin){
        NSData *data = [operatioin responseData];
        NSMutableDictionary *resHomePageDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        if([resHomePageDic[@"status"] isEqualToString:@"success"]){
            
                _dataNowHomePage = [NSDate date];
            
                NSMutableDictionary *dic  =  resHomePageDic[@"data"];
            if(dic != nil){

             NSLog(@"%lu",(unsigned long)[dic count]);
       
                if(![dic count] == 0){
                    for(NSMutableDictionary *dicCell in dic[@"projects"]){
                        _CountTop = (int)[_cellTopData count];
                        _cellTopData[_CountTop] = dicCell;
                        NSLog(@"%i",(int)[_cellTopData count]);
                        
                        _isNil = YES;
                    }

                    _pageNumDown++;
                    
            }
            [self.tableView reloadData];
            }
            [self.tableView footerEndRefreshing];
       
        }else{
            [self.tableView footerEndRefreshing];
            [[ToolClass sharedInstance]showAlert:@"加载失败，服务器报错"];
        }
    }CallBackNO:^(MKNetworkOperation *errorOp,NSError *err){
        [[ToolClass sharedInstance]showAlert:@"加载失败,请检查网络"];
        [self.tableView footerEndRefreshing];
    }];
}

//低内存
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//表视图返回行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //data数组 + 1 因为第一行是轮播图
//    NSLog(@"Cell数量%i",(int)[_cellTopData count]);
    if([_cellTopData count] == 0){
        return 0;
    }else{
        return ([_cellTopData count] + 1);        
    }

}


//获取cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        
        
        if (_countCreateCell == 0) {
            UINib *nib = [UINib nibWithNibName:@"HomeDynamicImage" bundle:nil];
            [tableView registerNib:nib forCellReuseIdentifier:@"HomeDymamicImageCell"];
            _countCreateCell = 1;
        }
        
        // 3. 从TableView中获取标识符为paperCell的Cell
        HomeDynamicImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeDymamicImageCell"];
        
        cell.delegate = self;
        
        NSLog(@"%lu",(unsigned long)[_cellHomeSlide count]);
        [cell cleanData];
        for(DataModelHomeSilder *data in _cellHomeSlide){
            NSLog(@"打印%@",data);
            [cell setDataSilder:data];
        }

        return cell;
        
    }else{
      
        HomePageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomePageTableViewCell"];
        
        if(cell == nil){
            
            cell = [[HomePageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HomePageTableViewCell"];
            
        }
        
        
        NSMutableDictionary * dicData = _cellTopData[indexPath.row - 1];
        
        NSLog(@"%@",dicData);
        //发起人
        NSMutableArray *aryCreator = dicData[@"creator"];
        NSMutableDictionary * dicCreator = aryCreator[0];
        NSLog(@"%@",dicCreator);
        cell.labelBy.text = [NSString stringWithFormat:@"by %@",dicCreator[@"name"]];
        [cell.labelBy sizeToFit];
        
        cell.labelTitle.text = dicData[@"title"];
        [cell.labelTitle sizeToFit];
        
        cell.labelContents.text = dicData[@"desc"];
        [cell.labelContents sizeToFit];
        
        if([dicData[@"isFocus"] longValue] == 0 && [dicData[@"isorder"] longValue] == 0){
            
            cell.imageViewJoined.image = nil;
            cell.imageViewLove.image = nil;
            
        }else if([dicData[@"isFocus"] longValue] == 1 && [dicData[@"isorder"] longValue] != 1){

            cell.imageViewJoined.image = nil;
            if([_iOSDeviceSize isEqualToString:@"iPhone6Plus"]){
            cell.imageViewLove.image = [UIImage imageNamed:@"tamenLikeYES@3x"];
            }else{
            cell.imageViewLove.image = [UIImage imageNamed:@"tamenLikeYES@2x"];
            }

 
            
        }else if([dicData[@"isorder"] longValue] == 1){
            
            if([_iOSDeviceSize isEqualToString:@"iPhone6Plus"]){
            cell.imageViewJoined.image = [UIImage imageNamed:@"ProjectJoined@3x"];
            }else{
            cell.imageViewJoined.image = [UIImage imageNamed:@"ProjectJoined@2x"];
            }
            cell.imageViewLove.image = nil;

        }
        
        cell.labelType.text = [NSString stringWithFormat:@"%@",dicData[@"type"]];
        cell.labelCountry.text = [NSString stringWithFormat:@"%@",dicData[@"address"]];
        cell.labelPreson.text = [NSString stringWithFormat:@"%lu个火伴",[dicData[@"fact_person"] longValue]];
        
        
        NSLog(@"开始%@-----结束%@",dicData[@"create_date"],dicData[@"end_date"]);
        
        NSDate *dataProjectCreate = [_dataForMatterHomePage dateFromString:dicData[@"create_date"]];
        
        NSInteger interval = [_timeZoneHomePage secondsFromGMTForDate:dataProjectCreate];
        
        NSDate *dateCreate = [dataProjectCreate dateByAddingTimeInterval: interval];
        
        NSDate *dataProjectEnd = [_dataForMatterHomePage dateFromString:dicData[@"end_date"]];
        
        interval = [_timeZoneHomePage secondsFromGMTForDate:dataProjectEnd];
        
        NSDate *dateEnd = [dataProjectEnd dateByAddingTimeInterval: interval];
        
        cell.labelDate.text = [NSString stringWithFormat:@"剩余%i天",(int)[dateEnd timeIntervalSinceDate:dateCreate]/86400];
        

        [cell.labelType sizeToFit];
        [cell.labelCountry sizeToFit];
        [cell.labelDate sizeToFit];
        [cell.labelPreson sizeToFit];

        cell.viewTypeWidth.constant = cell.labelType.frame.size.width + 12;
        cell.viewCountryWidth.constant = cell.labelCountry.frame.size.width + 12;
        cell.viewDateWidth.constant = cell.labelDate.frame.size.width + 12;
        cell.viewPresonWidth.constant = cell.labelPreson.frame.size.width + 12;
        
        [cell.viewType sizeToFit];
        [cell.viewCountry sizeToFit];
        [cell.viewDate sizeToFit];
        [cell.viewPreson sizeToFit];


        [cell.imageViewContent sd_setImageWithURL:dicData[@"images"][0] placeholderImage:nil];
        cell.imageViewContentWidth.constant = cell.viewPreson.frame.origin.x - 16;
        
        NSLog(@"进度条%f",cell.viewCell.frame.size.height - 66);
        if([_iOSDeviceSize isEqualToString:@"iPhone6Plus"] || [_iOSDeviceSize isEqualToString:@"iPhone6"]){
            
            cell.HomePageProgressBarView.frame = CGRectMake(cell.viewPreson.frame.origin.x,122,cell.viewCell.frame.size.width -  cell.viewPreson.frame.origin.x - 6,16);
        }else{
            
            cell.HomePageProgressBarView.frame = CGRectMake(cell.viewPreson.frame.origin.x,90,cell.viewCell.frame.size.width -  cell.viewPreson.frame.origin.x - 6,16);
        }
        
        _floatProgress = [dicData[@"fact_money"] floatValue]/[dicData[@"wanted_money"] floatValue];
        
        if(_floatProgress * 100 >= 0 && _floatProgress * 100 < 10){
            
            cell.HomePageProgressBarView.progress = 0.16;
            
            //百分比文字位置
            cell.labelProgress.frame = CGRectMake(6,2.5,50,15);
            
            //百分比文字内容
            cell.labelProgress.text = [NSString stringWithFormat:@"%@%%",[[NSString stringWithFormat:@"%f",round(_floatProgress * 100)] substringToIndex:1]];

            [cell.labelProgress sizeToFit];
//            //进度条位置

            cell.HomePageProgressBarView.barFillColor = [UIColor colorWithRed:255.0/255 green:51.0/255 blue:51.0/255 alpha:1];
            
            [cell.labelProgress setTextColor:[UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1]];

        }else if(_floatProgress * 100 >= 10 && _floatProgress * 100 < 21){

            cell.HomePageProgressBarView.progress = 0.18;
            //百分比文字位置
            cell.labelProgress.frame = CGRectMake(6,2.5,50,15);
            
            cell.labelProgress.text = [NSString stringWithFormat:@"%@%%",[[NSString stringWithFormat:@"%f",round(_floatProgress * 100)] substringToIndex:2]];
            
            [cell.labelProgress sizeToFit];
//            //进度条位置
            
            //进度条位置
            cell.HomePageProgressBarView.barFillColor = [UIColor colorWithRed:255.0/255 green:51.0/255 blue:51.0/255 alpha:1];
            
            [cell.labelProgress setTextColor:[UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1]];
            
        }else if(_floatProgress * 100 >= 21 && _floatProgress * 100 < 90){

             //进度条进度
            cell.HomePageProgressBarView.progress = _floatProgress ;
            
            cell.labelProgress.text = [NSString stringWithFormat:@"%@%%",[[NSString stringWithFormat:@"%f",round(_floatProgress * 100)] substringToIndex:2]];
            
//            //百分比文字位置
            cell.labelProgress.frame = CGRectMake(cell.HomePageProgressBarView.frame.size.width * _floatProgress - 26,2.5,50,15);
//            
            [cell.labelProgress sizeToFit];
            
            //            //进度条位置
            cell.HomePageProgressBarView.barFillColor = [UIColor colorWithRed:255.0/255 green:51.0/255 blue:51.0/255 alpha:1];
            
            [cell.labelProgress setTextColor:[UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1]];
            
        }else if(_floatProgress * 100 >= 90 && _floatProgress * 100 <100){
            
//            
//            NSLog(@"%f",_floatPercentageHome * 100);
//            
//            //百分比文字
//            self.progressLabel.text = [NSString stringWithFormat:@"%@%%",[[NSString stringWithFormat:@"%f",round(_floatPercentageHome * 100)] substringToIndex:2]];
            //进度条进度
            cell.HomePageProgressBarView.progress = _floatProgress;
//            
//            //百分比文字位置
            cell.labelProgress.frame = CGRectMake(cell.HomePageProgressBarView.frame.size.width * _floatProgress - 30,2.5,50,15);
//
            
            cell.labelProgress.text = [NSString stringWithFormat:@"%@%%",[[NSString stringWithFormat:@"%f",round(_floatProgress * 100)] substringToIndex:2]];
            
            [cell.labelProgress sizeToFit];
            
            //            //进度条位置
            cell.HomePageProgressBarView.barFillColor = [UIColor colorWithRed:255.0/255 green:51.0/255 blue:51.0/255 alpha:1];
            
            [cell.labelProgress setTextColor:[UIColor colorWithRed:238.0/255 green:238.0/255 blue:238.0/255 alpha:1]];
            
        }else if(_floatProgress * 100 >= 100){
            
        cell.HomePageProgressBarView.progress = 1;
            
        cell.labelProgress.text = [NSString stringWithFormat:@"%@%%",[[NSString stringWithFormat:@"%f",round(_floatProgress * 100)] substringToIndex:3]];
            
        //百分比文字位置
        [cell.labelProgress sizeToFit];
        cell.labelProgress.frame = CGRectMake(cell.HomePageProgressBarView.frame.size.width - cell.labelProgress.frame.size.width - 6 ,2.5,cell.labelProgress.frame.size.width,cell.labelProgress.frame.size.height);
            
        cell.HomePageProgressBarView.barFillColor = [UIColor colorWithRed:79.0/255 green:194.0/255 blue:177.0/255 alpha:1];

        [cell.labelProgress setTextColor:[UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:1]];
            
        }
        
//        if(indexPath.row == [_cellTopData count]){
//    
//            [self.tableView reloadData];
//        }
        return cell;
    }

}


-(void)HomeDynamicImageCellSegue:(DataModelHomeSilder *)sender{
    
    if([sender.imageType isEqualToString:@"project"]){
        
        [self performSegueWithIdentifier:@"Projectdetails" sender:sender.projectID];
    }else if([sender.imageType isEqualToString:@"article"]){

        [self performSegueWithIdentifier:@"articleSegue" sender:sender.strContents];
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
        NSMutableDictionary *dic = _cellTopData[(indexPath.row - 1)];
    
    [self performSegueWithIdentifier:@"Projectdetails" sender:dic[@"_id"]];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"Projectdetails"]){

        ProjectDetailsViewController *controller = (ProjectDetailsViewController*)segue.destinationViewController;
        
        NSLog(@"ProjectID%@",sender);
        
        controller.strProjectID = sender;

    }else if([segue.identifier isEqualToString:@"articleSegue"]){
        
        UINavigationController *naVC = segue.destinationViewController;
        
        ArticleHTMLViewController *controller = (ArticleHTMLViewController*)naVC.topViewController;
        
        NSLog(@"HTML内容%@",sender);
        
        controller.strContents = sender;
    }else if([segue.identifier isEqualToString:@"loginSegue"]){
        
        LoginViewController *controller = segue.destinationViewController;
        
        controller.delegate = self;
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        
        return _heightSliderCell;
        
    }else{
//        if([_cellTopData count] != 0){
//            NSLog(@"%ld",(long)indexPath.row);
//            HomePageTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
//            cell.imageViewContentWidth.constant = cell.viewPreson.frame.origin.x - 16;
//            if([_iOSDeviceSize isEqualToString:@"iPhone6Plus"] || [_iOSDeviceSize isEqualToString:@"iPhone6"]){
//            cell.HomePageProgressBarView.frame = CGRectMake(cell.viewPreson.frame.origin.x,122,cell.viewCell.frame.size.width -  cell.viewPreson.frame.origin.x - 6,16);
//            }else{
//            cell.HomePageProgressBarView.frame = CGRectMake(cell.viewPreson.frame.origin.x,90,cell.viewCell.frame.size.width -  cell.viewPreson.frame.origin.x - 6,16);
//            }
//        }
        return _heightCardCell;
        
    }
    
}

-(void)LoginViewControllerClose:(LoginViewController *)controller{
    [controller dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)loginViewCreate{

    //    _viewPayList = [[UIView alloc] initWithFrame:];
    _viewPopLogin = [[UIView alloc] initWithFrame:CGRectMake(0 ,self.tableView.contentOffset.y, _iOSLoginDeviceSize.width, _iOSLoginDeviceSize.height - 44)];
    _viewPopLogin.backgroundColor = [UIColor colorWithRed:21.0/255 green:30.0/255 blue:40.0/255 alpha:0.95];
    [self.view addSubview:_viewPopLogin];
    
    UIView *viewLoginBlock = [[UIView alloc] initWithFrame:CGRectMake(0,0,78,162)];
    viewLoginBlock.backgroundColor = [UIColor colorWithRed:38.0/255 green:55.0/255 blue:70.0/255 alpha:1];
    viewLoginBlock.clipsToBounds = YES;
    viewLoginBlock.layer.cornerRadius = 12;
    viewLoginBlock.center = _viewPopLogin.center;
    [_viewPopLogin addSubview:viewLoginBlock];
    
    UIButton *buttonLogin = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonLogin addTarget:self action:@selector(buttonLoginClick:) forControlEvents:UIControlEventTouchUpInside];
    buttonLogin.backgroundColor = [UIColor colorWithRed:119.0/255 green:134.0/255 blue:146.0/255 alpha:1];
    [buttonLogin setTitleColor:[UIColor colorWithRed:38.0/255 green:55.0/255 blue:70.0/255 alpha:1]forState:UIControlStateNormal];
    [buttonLogin setTitle:@"登录" forState:UIControlStateNormal];
    buttonLogin.clipsToBounds = YES;
    buttonLogin.layer.cornerRadius = 27;
    [buttonLogin setFrame:CGRectMake(12,84,54,54)];
    [viewLoginBlock addSubview:buttonLogin];
    
    
    UIImageView *viewClose = [[UIImageView alloc] initWithFrame:CGRectMake(31,24,16,16)];
    if([_iOSDeviceSize isEqualToString:@"iPhone6Plus"]){
        viewClose.image = [UIImage imageNamed:@"close_grey_@3x"];
    }else{
        viewClose.image = [UIImage imageNamed:@"close_grey_@2x"];
    }
    [viewLoginBlock addSubview:viewClose];
    
    UIButton *buttonClose = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonClose addTarget:self action:@selector(buttonLoginCloseClick:) forControlEvents:UIControlEventTouchUpInside];
    [buttonClose setFrame:CGRectMake(17,28,44,44)];
    //    buttonClose.layer.borderWidth = 1;
    [viewLoginBlock addSubview:buttonClose];

}

//-(void)projectDeatilsCancelFromHomePage:(ProjectDetailsViewController *)controller{
//    
//    isRefresh = NO;
//    
//    [controller dismissViewControllerAnimated:YES completion:nil];
//    
//}
@end
