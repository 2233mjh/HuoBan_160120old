//
//  HttpClassSelf.m
//  appDemo
//
//  Created by 刘雨辰 on 15/9/10.
//  Copyright (c) 2015年 lyc. All rights reserved.


#import "HttpClassSelf.h"
#import "MKNetworkKit.h"
#import "CommonCrypto/CommonDigest.h"
#import "CommonBase64.h"

#define HTTPAddHuoBan  @"api.huoban.io:8877"
//服务器测试地址
//#define HTTPAddHuoBan  @"123.57.33.133:8888"    

@implementation HttpClassSelf

#pragma mark POST

//登陆
-(void)loginSetMobile:(NSString *)mobile pwd:(NSString *)pwd umengid:(NSString *)umengid mobibuild:(NSString *)mobibuild mobitype:(NSString *)mobitype CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    //设置请求路径
    NSString *path = [[NSString alloc]initWithFormat:@"/auth/login"];
    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan customHeaderFields:nil ];
    
    NSString *pwdMD5 =  [self MD5ByAStr:pwd];
    
    NSMutableDictionary *postBody = [[NSMutableDictionary alloc]init ];
    [postBody setValue:mobile forKey:@"mobile"];
    [postBody setValue:pwdMD5 forKey:@"pwd"];
//    [postBody setValue:umengid forKey:@"umengid"];
    [postBody setValue:@"" forKey:@"umengid"];
    [postBody setValue:mobibuild forKey:@"mobibuild"];
    [postBody setValue:mobitype forKey:@"mobitype"];
    
    NSLog(@"%@",umengid);
    
    //请求地址，参数
    MKNetworkOperation *op = [engine operationWithPath:path params:postBody httpMethod:@"POST"];
    
    //请求闭包
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];

    //发起网络请求
    [engine enqueueOperation:op];

}

//回复评论
-(void)answerComment:(NSString *)strComment token:(NSString *)strToken answerID:(NSString*)strAnswerID message:(NSString *)strMessage CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    //设置请求路径
    NSString *path = [[NSString alloc]initWithFormat:@"/comment/%@/reply/%@",strComment,strToken];
    
    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan customHeaderFields:nil ];
    
    NSMutableDictionary *postBody = [[NSMutableDictionary alloc]init ];
    [postBody setValue:strMessage forKey:@"message"];

    //请求地址，参数
    MKNetworkOperation *op = [engine operationWithPath:path params:postBody httpMethod:@"POST"];
    
    //请求闭包
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    //发起网络请求
    [engine enqueueOperation:op];
    
    
}


//保存个人信息
-(void)UpdateUserSettingSetUser:(NSString*)steUser setToken:(NSString*)strToken setImage:(NSString*)steImage setName:(NSString*)strName setSex:(NSString*)strSex setAddress:(NSString*)strAddress setSign:(NSString*)strSign setDesc:(NSString*)strDesc payName:(NSString *)strPayName payMobile:(NSString *)strPayMobile payPostCode:(NSString *)strPostCode CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"/user/profile/user-%@/%@",steUser,strToken];
    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan customHeaderFields:nil ];
    
    NSMutableDictionary *postBody = [[NSMutableDictionary alloc]init];
    
    [postBody setValue:steUser forKey:@"user"];
    [postBody setValue:strToken forKey:@"token"];
    [postBody setValue:steImage forKey:@"image"];
    [postBody setValue:strName forKey:@"name"];
    [postBody setValue:strSex forKey:@"sex"];
    [postBody setValue:strAddress forKey:@"address"];
    [postBody setValue:strSign forKey:@"sign"];
    [postBody setValue:strDesc forKey:@"desc"];
    [postBody setValue:strPayName forKey:@"postname"];
    [postBody setValue:strPayMobile forKey:@"postmobile"];
    [postBody setValue:strAddress forKey:@"postaddress"];
    [postBody setValue:strPostCode forKey:@"postcode"];
    
//    NSLog(@"%@--%@--%@--%@--%@--%@--%@--%@--%@---%@---%@",steUser,strToken,steImage,strName,strSex,strAddress,strSign,strDesc,strPayName,strPayMobile,strAddress);
    
    MKNetworkOperation *op = [engine operationWithPath:path params:postBody httpMethod:@"POST"];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
}


-(void)UpdateUserSettingSetUser:(NSString*)steUser setToken:(NSString*)strToken setImage:(NSString*)steImage setName:(NSString*)strName setSex:(NSString*)strSex setAddress:(NSString*)strAddress setSign:(NSString*)strSign setDesc:(NSString*)strDesc payName:(NSString *)strPayName payMobile:(NSString *)strPayMobile payPostCode:(NSString *)strPostCode profession:(NSString *)profession city:(NSString *)city CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"/user/profile/user-%@/%@",steUser,strToken];
    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan customHeaderFields:nil ];
    
    NSMutableDictionary *postBody = [[NSMutableDictionary alloc]init];
    
    NSString * _strSex;
    _strSex = [strSex isEqualToString:@"男"]?@"1":@"0";
    
    [postBody setValue:steUser forKey:@"user"];
    [postBody setValue:strToken forKey:@"token"];
    [postBody setValue:steImage forKey:@"image"];
    [postBody setValue:strName forKey:@"name"];
    
    [postBody setValue:_strSex forKey:@"sex"];
    
    [postBody setValue:strAddress forKey:@"address"];
    [postBody setValue:strSign forKey:@"sign"];
    [postBody setValue:strDesc forKey:@"desc"];
    [postBody setValue:strPayName forKey:@"postname"];
    [postBody setValue:strPayMobile forKey:@"postmobile"];
    [postBody setValue:strAddress forKey:@"postaddress"];
    [postBody setValue:strPostCode forKey:@"postcode"];
    [postBody setValue:profession forKey:@"profession"];
    [postBody setValue:city forKey:@"city"];
    
    NSLog(@"%@",postBody);
    //    NSLog(@"%@--%@--%@--%@--%@--%@--%@--%@--%@---%@---%@",steUser,strToken,steImage,strName,strSex,strAddress,strSign,strDesc,strPayName,strPayMobile,strAddress);
    
    MKNetworkOperation *op = [engine operationWithPath:path params:postBody httpMethod:@"POST"];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
}


//注册
-(void)RegisterSetMobile:(NSString*)mobile pwd:(NSString*)pwd umengid:(NSString*)umengid mobibuild:(NSString*)mobibuild mobitype:(NSString*)mobitype CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    //设置请求路径
    NSString *path = [[NSString alloc]initWithFormat:@"/auth/register"];
    
    NSString *pwdMD5 =  [self MD5ByAStr:pwd];
    
    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan customHeaderFields:nil ];
    
    NSMutableDictionary *postBody = [[NSMutableDictionary alloc]init ];
    [postBody setValue:mobile forKey:@"mobile"];
    [postBody setValue:pwdMD5 forKey:@"pwd"];
    [postBody setValue:umengid forKey:@"umengid"];
    [postBody setValue:mobibuild forKey:@"mobibuild"];
    [postBody setValue:mobitype forKey:@"mobitype"];
    
    //请求地址，参数
    MKNetworkOperation *op = [engine operationWithPath:path params:postBody httpMethod:@"POST"];

    [op addCompletionHandler:BlockYES errorHandler:BlockNO];

    //发起网络请求
    [engine enqueueOperation:op];
}


//忘记密码
-(void)forgotPWDSetMobile:(NSString*)mobile pwd:(NSString*)pwd umengid:(NSString*)umengid mobibuild:(NSString*)mobibuild mobitype:(NSString*)mobitype CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    //设置请求路径
    NSString *path = [[NSString alloc]initWithFormat:@"/auth/forgetpwd"];
    
    NSString *pwdMD5 =  [self MD5ByAStr:pwd];

    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan customHeaderFields:nil ];
    
    NSMutableDictionary *postBody = [[NSMutableDictionary alloc]init ];
    [postBody setValue:mobile forKey:@"mobile"];
    [postBody setValue:pwdMD5 forKey:@"pwd"];
    [postBody setValue:umengid forKey:@"umengid"];
    [postBody setValue:mobibuild forKey:@"mobibuild"];
    [postBody setValue:mobitype forKey:@"mobitype"];
    
    //请求地址，参数
    MKNetworkOperation *op = [engine operationWithPath:path params:postBody httpMethod:@"POST"];
    
    //请求闭包
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    //发起网络请求
    [engine enqueueOperation:op];
    
    
    
}

//发布项目动态
-(void)sendProjectselfDynamicSetProjectID:(NSString*)strProjectID strToken:(NSString*)strToken message:(NSString*)message image:(NSString*)strImage CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    //设置请求路径
    NSString *path = [[NSString alloc]initWithFormat:@"/project/feed/p-%@/new/%@",strProjectID,strToken];
    
    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan customHeaderFields:nil ];
    
    NSMutableDictionary *postBody = [[NSMutableDictionary alloc]init ];
    [postBody setValue:message forKey:@"message"];
    [postBody setValue:strImage forKey:@"image"];
    
    //请求地址，参数
    MKNetworkOperation *op = [engine operationWithPath:path params:postBody httpMethod:@"POST"];
    
    //请求闭包
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    //发起网络请求
    [engine enqueueOperation:op];

    
}
//评论项目动态
-(void)commentProjectselDynamicSetFeed:(NSString*)strFeed strToken:(NSString*)strToken message:(NSString*)message CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    //设置请求路径
    NSString *path = [[NSString alloc]initWithFormat:@"/project/feed/%@/comment/%@",strFeed,strToken];
    
    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan customHeaderFields:nil ];
    
    NSMutableDictionary *postBody = [[NSMutableDictionary alloc]init ];
    [postBody setValue:message forKey:@"message"];
    
    
    //请求地址，参数
    MKNetworkOperation *op = [engine operationWithPath:path params:postBody httpMethod:@"POST"];
    
    //请求闭包
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    //发起网络请求
    [engine enqueueOperation:op];
    
    
}

#pragma mark GET
//主页项目列表
-(void)homePageSetKey:(NSString*)strkey numPage:(int)numPage numNum:(int)numNum token:(NSString*)strtoken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    //设置请求路径
    NSString *path = [[NSString alloc]initWithFormat:@"/project/list/%@/page-%i/num-%i/%@",strkey,numPage,numNum,strtoken];
    
    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan customHeaderFields:nil ];
    
    //请求地址，参数
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    //请求闭包
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    //发起网络请求
    [engine enqueueOperation:op];
    
}

//聊天列表;
-(void)projectTalkSetFeed:(NSString *)strFeed numPage:(int)numPage numNum:(int)numNum token:(NSString *)strToken CallBackyes:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"/comments/list/f-%@/page-%i/num-%i/%@",strFeed,numPage,numNum,strToken];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
}

//项目详情
-(void)projectDetailsSetProjectID:(NSString*)strProjectID token:(NSString*)strtoken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    //设置请求路径
    NSString *path = [[NSString alloc]initWithFormat:@"/project/p-%@/%@",strProjectID,strtoken];
    
    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan customHeaderFields:nil ];
    
    //请求地址，参数
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    //请求闭包
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    //发起网络请求
    [engine enqueueOperation:op];
    
}
-(void)oneProjectDynamicSetProjectID:(NSString *)strProjectID numPage:(int)numPage numNum:(int)numNum token:(NSString *)strToken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    
    NSString *path = [[NSString alloc] initWithFormat:@"/feed/p-%@/page-%i/num-%i/%@",strProjectID,numPage,numNum,strToken];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
    
}

//显示一个用户的所有动态
-(void)projectDynmaicUserSelfSetUser:(NSString*)strUser numPage:(int)numPage numNum:(int)numNum token:(NSString*)strToken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    NSString *path = [[NSString alloc] initWithFormat:@"/feed/u-%@/page-%i/num-%i/%@",strUser,numPage,numNum,strToken];
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan  customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
    
}

//全部动态接口
-(void)projectDynamicOurSetUser:(NSString*)strUser numPage:(int)numPage numNum:(int)numNum token:(NSString*)strToken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"/feed/u-%@-we/page-%i/num-%i/%@",strUser,numPage,numNum,strToken];

    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
}

//  查看个人所有动态接口
-(void)personDynamic:(NSString *)strUserID numPage:(int)numPage numNum:(int)numNum token:(NSString *)strToken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"/feed/u-%@/page-%i/num-%i/%@",strUserID,numPage,numNum,strToken];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
    
}

-(void)projectDetailsDynamic:(NSString *)strProjectID numPage:(int)numPage numNum:(int)numNum token:(NSString *)strToken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"/feed/p-%@-creator/page-%i/num-%i/%@",strProjectID,numPage,numNum,strToken];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];

}


//项目点赞(取消)
-(void)loveProjectselfSetFeed:(NSString*)strFeed up:(int)upInt token:(NSString*)strtoken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    //设置请求路径
    NSString *path = [[NSString alloc]initWithFormat:@"/project/feed/%@/up/%i/%@",strFeed,upInt,strtoken];
    
    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan  customHeaderFields:nil ];
    
    //请求地址，参数
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    //请求闭包
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    //发起网络请求
    [engine enqueueOperation:op];
}

//关注一个项目(取消)(返回状态)
-(void)focusProjectselfSetProjectID:(NSString*)strProjectID up:(int)upInt token:(NSString*)strtoken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    //设置请求路径
    NSString *path = [[NSString alloc]initWithFormat:@"/user/focus/%@/%i/%@",strProjectID,upInt,strtoken];
    
    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan  customHeaderFields:nil ];
    
    //请求地址，参数
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    //请求闭包
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    //发起网络请求
    [engine enqueueOperation:op];
}

//查看用户关注的项目(自己，别人)
-(void)focusProjectselfSetUser:(NSString*)strUser page:(int)numPage num:(int)numNum token:(NSString*)strtoken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    //设置请求路径
    NSString *path = [[NSString alloc]initWithFormat:@"/user/profile/focus/%@/page-%i/num-%i/%@",strUser,numPage,numNum,strtoken];
    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan customHeaderFields:nil ];
    //请求地址，参数
    MKNetworkOperation *op = [engine operationWithPath:path];
    //请求闭包
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    //发起网络请求
    [engine enqueueOperation:op];
}

//查看用户基本信息
-(void)CheckUserSelfSetUser:(NSString*)strUser token:(NSString*)strtoken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    //设置请求路径
    NSString *path = [[NSString alloc]initWithFormat:@"/user/profile/userv2-%@/%@",strUser,strtoken];
    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan customHeaderFields:nil ];
    //请求地址，参数
    MKNetworkOperation *op = [engine operationWithPath:path];
    //请求闭包
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    //发起网络请求
    [engine enqueueOperation:op];
}

//查看用户发起的项目
-(void)CheckUserCreateProjectSetUser:(NSString*)strUser page:(int)numPage num:(int)numNum token:(NSString*)strtoken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    //设置请求路径
    NSString *path = [[NSString alloc]initWithFormat:@"/user/profile/create/%@/page-%i/num-%i/%@",strUser,numPage,numNum,strtoken];
    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    //请求地址，参数
    MKNetworkOperation *op = [engine operationWithPath:path];
    //请求闭包
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    //发起网络请求
    [engine enqueueOperation:op];
    
}
//查看用户支持的项目
-(void)CheckUserSuppotProjectSetUser:(NSString*)strUser page:(int)numPage num:(int)numNum token:(NSString*)strtoken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    //设置请求路径
    NSString *path = [[NSString alloc]initWithFormat:@"/user/profile/pay/%@/page-%i/num-%i/%@",strUser,numPage,numNum,strtoken];
    //设置主机名
    MKNetworkEngine *engine = [[MKNetworkEngine alloc]initWithHostName:HTTPAddHuoBan customHeaderFields:nil ];
    //请求地址，参数
    MKNetworkOperation *op = [engine operationWithPath:path];
    //请求闭包
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    //发起网络请求
    [engine enqueueOperation:op];
}

-(void)qiniuSDK:(NSString*)qiniu CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"/qiniu/1w3EcXsD"];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
    
}

-(void)pingSDk:(NSString *)pingsdk projectID:(NSString *)strProjectID paytype:(NSString *)strPaytype chou:(NSString *)strChouID token:(NSString *)strToken CallBackYES:(CallBackYES)BlockYES CallBackNo:(CallBackNO)BlockNO{

    NSString *path = [[NSString alloc] initWithFormat:@"/pay/p-%@/%@/%@/%@",strProjectID,strPaytype,strChouID,strToken];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
}


-(void)getTalkListSetFeed:(NSString *)strFeed page:(int)pageInt num:(int)numInt token:(NSString *)strToken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"comments/list/f-%@/page-%i/num-%i/%@",strFeed,pageInt,numInt,strToken];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
}

-(void)GetCheckIDSetTelephoneNumForCreate:(NSString *)strTelephone getKey:(NSString *)strKey CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"/sendsms/%@/%@",strKey,strTelephone];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
}

-(void)GetCheckIDSetTelephoneNumForFixPWD:(NSString *)strTelephone getKey:(NSString *)strKey CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"/sendsms/%@/%@",strKey,strTelephone];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
}

-(void)CheckIDReviewForCreate:(NSString *)strTelephone getKey:(NSString *)strKey getCode:(NSString *)strCode CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"/sendsms/verify/%@/%@/%@",strKey,strTelephone,strCode];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
}

-(void)CheckIDReviewForFixPWD:(NSString *)strTelephone getKey:(NSString *)strKey getCode:(NSString *)strCode CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"/sendsms/verify/%@/%@/%@",strKey,strTelephone,strCode];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
}


-(void)ChoseMyProjectIDByUserID:(NSString *)strUserID token:(NSString *)strToken page:(int)pageInt num:(int)numInt CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"/user/profile/postfeed/%@/page-%i/num-%i/%@",strUserID,pageInt,numInt,strToken];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];

    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
}
//查看关注和加入项目接口
-(void)UpAndJoinProjectList:(NSString *)strUserID token:(NSString *)strToken page:(int)pageInt num:(int)numInt CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    
    NSString *path = [[NSString alloc] initWithFormat:@"/v2/user/profile/postfeed/%@/page-%i/num-%i/%@",strUserID,pageInt,numInt,strToken];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
}

-(void)CHeckPayList:(NSString *)strUserID token:(NSString *)strToken page:(int)pageInt num:(int)numInt CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
  
    NSString *path = [[NSString alloc] initWithFormat:@"user/orders/%@/page-%i/num-%i/%@",strUserID,pageInt,numInt,strToken];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];

}

-(void)deleteProjectDynamic:(NSString *)strFeedID token:(NSString *)strToken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"/project/feed/%@/%@",strFeedID,strToken];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path params:nil httpMethod:@"DELETE"];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
    
}


//单个动态信息
-(void)getOneDynamic:(NSString *)strFeedID token:(NSString *)strToken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"/project/feed/%@/%@",strFeedID,strToken];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path params:nil httpMethod:@"GET"];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
    
}

-(void)getUserTimeLineByUserID:(NSString *)strUserID page:(int)pageInt num:(int)numInt token:(NSString *)strToken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    
    NSString *path = [[NSString alloc] initWithFormat:@"/user/profile/timeline/%@/page-%i/num-%i/%@",strUserID,pageInt,numInt,strToken];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path params:nil httpMethod:@"GET"];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
    
}


//我V@
-(void)getUserSelfInfoByUserID:(NSString *)strUserID page:(int)pageInt num:(int)numInt token:(NSString *)strToken CallBackYES:(CallBackYES)BlockYES CallBackNO:(CallBackNO)BlockNO{
    ///v2/profile/u-{user}/page-{page}/num-{num}/{token}
    NSString *path = [[NSString alloc] initWithFormat:@"/v2/profile/u-%@/page-%i/num-%i/%@",strUserID,pageInt,numInt,strToken];
    
    MKNetworkEngine *engine = [[MKNetworkEngine alloc] initWithHostName:HTTPAddHuoBan customHeaderFields:nil];
    
    MKNetworkOperation *op = [engine operationWithPath:path params:nil httpMethod:@"GET"];
    
    [op addCompletionHandler:BlockYES errorHandler:BlockNO];
    
    [engine enqueueOperation:op];
    
    //http://123.57.33.133:8888/v2/profile/u-5620a7b7b5a2a7a8fe41b7eb/page-0/num-10/5620a7b7b5a2a7a8fe41b7eb
    //http://123.57.33.133:8888/v2/profile/567b9019acaad970fc37c754/page-0/num-0/567b9019acaad970fc37c754
}

//md5加密
-(NSString *)MD5ByAStr:(NSString *)aSourceStr {
    const char* cStr = [aSourceStr UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, strlen(cStr), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH];
    for (NSInteger i=0; i<CC_MD5_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02x", result[i]];
    }
    
    return ret;
}

@end
