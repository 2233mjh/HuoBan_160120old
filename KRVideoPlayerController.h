//
//  KRVideoPlayerController.h
//  KRKit
//
//  Created by aidenluo on 5/23/15.
//  Copyright (c) 2015 36kr. All rights reserved.
//

@import MediaPlayer;
@class KRVideoPlayerController;

@protocol KRVideoPlayerDelegate <NSObject>

-(void)videoFullConteoller:(KRVideoPlayerController*)controller;
-(void)videoDismissController:(KRVideoPlayerController*)controller;
-(void)videoDefullController:(KRVideoPlayerController*)controller;
@end

@interface KRVideoPlayerController : MPMoviePlayerController

@property (nonatomic, copy)void(^dimissCompleteBlock)(void);
@property (nonatomic, assign) CGRect frame;

- (instancetype)initWithFrame:(CGRect)frame;
- (void)showInWindow;
- (void)dismiss;

@property(nonatomic,strong)id<KRVideoPlayerDelegate>delegate;

@end